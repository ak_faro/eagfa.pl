<?
return [
	'msg_no_neworders'=>"Nie ma zamówień z którymi można powiązać ten projekt",
	'pin'=>"przyłącz",
	'unpin'=>"odłącz",

	'title_import'=>"Projekt zewnętrzny",
	'title_coupons'=>"Kupony rabatowe",
	'title_discounts'=>"Rabaty",

	'list_projects'=>"Lista załączonych projektów zalamo.com",
	'config_buy'=>'Skonfiguruj i zamów nowy produkt',
	'config2_buy'=>'Dodaj pozycje do koszyka i zamów',
	'login_attach'=>"Zaloguj się aby dołączyć projekt do któregoś ze swoich zamówień",
	'login_attach2'=>"Zaloguj się aby dołączyć odbitki do któregoś ze swoich zamówień",
	'show_list'=>"pokaż listę",
	'hide_list'=>"ukryj listę",
	'pos_orders'=>"Pozycje w zamówieniu",

	'msg_pined'=>"Projekt został przypisany do zamówienia",
	'msg_notpined'=>"Projekt nie został przypisany do zamówienia",
	'msg_unpined'=>"Projekt został odłączony od zamówienia",
	'msg_notunpined'=>"Projekt nie został odłączony od zamówienia",
	'msg_pined2'=>"Odbitki zostały przypisane do zamówienia",
	'msg_notpined2'=>"Odbitki nie zostały przypisane do zamówienia",
	'msg_unpined2'=>"Odbitki zostały odłączone od zamówienia",
	'msg_notunpined2'=>"Odbitki nie zostały odłączone od zamówienia",
	'msg_show_allnew'=>"Pokaż pozostałe zamówienia",
	'msg_no_orders'=>"Nie masz jeszcze żadnych zamówień",
	'msg_ftpcreated'=>"Konto FTP zostało utworzone",
	'err_ftpcreated'=>"Nie udało się utworzyć konta FTP",
];