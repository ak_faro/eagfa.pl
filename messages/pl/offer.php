<?

return [
	'main_subtitle'=>"Wybierz produkt",
	'basket_title'=>"Moje zamówienie",
	'summary_title'=>"Podsumowanie zamówienia",

	'msg_empty_category'=>"Brak produktów w tej kategorii",
	'msg_basket_updated'=>"Koszyk został zaktualizowany",
	'msg_basket_error'=>"Nie udało się zaktualizować koszyka",
	'msg_del_confirm'=>"Czy na pewno usunąć pozycję?",
	'msg_clear_confirm'=>"Czy na pewno wyczyścić koszyk?",

	'err_empty_basket'=>"Koszyk jest pusty",
	'err_no_address'=>"Proszę uzupełnić adres dostawy",
	'err_no_invoice'=>"Proszę uzupełnić dane do faktury",
	'err_tomuch_coupons'=>"Suma wybranych kuponów rabatowych przekracza kwotę zamówienia. Zamów dodatkowe produkty lub zmień wybór kuponów.",

	'label_item_name'=>"Nazwa pozycji",
	'label_item_price'=>"Cena za szt.",
	'label_item_amount'=>"Ilość sztuk",
	'label_item_sum'=>"Cena pozycji",
	'label_item_discount'=>"Rabat %",
	'label_item_cfg'=>"Konfiguracja produktu",
	
	'label_order_sum'=>"Podsumowanie zamówienia",
	'label_price'=>"Cena:",
	'label_brutto'=>"brutto",
	'label_total'=>"Cena brutto:",
	'label_shipcost'=>"Cena wysyłki:",
	'label_finaltotal'=>"Końcowa cena brutto:",
	'label_coupons'=>"Kupony rabatowe",
	'label_discount'=>"Kwota rabatu",
	
	'btn_gobasket'=>"przejdź do koszyka",
	'btn_continuebuy'=>"kontynuuj zakupy",
	'btn_addbasket'=>"do koszyka",
	'btn_savebasket'=>"zapamiętaj",
	'btn_clearbasket'=>"wyczyść koszyk",
	'btn_checkout'=>"kontynuuj",
	'btn_finalize'=>"zamawiam",

	'btn_seeother'=>"zobacz inne produkty",
	'btn_nextstep'=>"przejdź dalej",
	'btn_prevstep'=>"wróć",
	'btn_backhome'=>"strona główna",
	'btn_mybasket'=>"moje zamówienie",

];