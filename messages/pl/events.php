<?
return [
	'model_id'=>"ID",
	'model_created'=>"Data utworzenia",
	'model_when'=>"Data rozpoczęcia",
	'model_stop'=>"Data zakończenia",
	'model_remind'=>"Przypomnienie",
	'model_title'=>"Tytuł",
	'model_description'=>"Opis",
	'model_owner'=>"Twórca",
	'model_target'=>"Dotyczy osoby",

	'msg_delconfirm'=>"Czy na pewno usunąć zdarzenie?",
	'msg_deleted'=>"Zdarzenie zostało usunięte",
	'msg_notdeleted'=>"Nie udało się skasować zdarzenia",

	'err_no_model'=>"Nie ma takiego zdarzenia",
	'err_not_permit'=>"Brak uprawnień do tego zdarzenia",

	'label_time'=>"Czas",
	'label_target_profile'=>"Zobacz profil osoby",
	'label_target_msg'=>"Wyślij wiadomość do osoby",
	'label_close_remind'=>"Zatwierdź przypomnienie",
	'find_string'=>"Wyszukaj zdarzenie",
	'msg_saved'=>"Zdarzenie zostało zapisane",
	'msg_notsaved'=>"Nie udało się zapisać zdarzenia",
	'btn_add'=>"dodaj zdarzenie",
	'label_unspecifed'=>"nieprzypisane",

	'panel_title_list'=>"Lista zdarzeń",
	'panel_title_new'=>"Dodaj zdarzenie",
	'panel_title_edit'=>"Edytuj zdarzenie",
];