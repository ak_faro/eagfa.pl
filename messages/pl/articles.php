<?
return [
	'model_id'=>"ID",
	'model_title' => "Tytuł artykułu",
	'model_intro' => "Wstęp",
	'model_full' => "Pełna treść",
	'model_group' => "Kategoria",
	'model_style' => "Style CSS",
	'model_style2' => "Czas wyświetlania (ms)",
	'model_created'=>"Data utworzenia",
	'model_image'=>"Obraz główny",
	
	'all_news'=>"wszystkie aktualności",
	'list_all'=>"Lista wszystkich treści",

	'group_all'=>"Wszystkie",
	'btn_add'=>"Dodaj treść",
	'btn_addimg'=>"Dodaj/edytuj obraz",
	'find_string'=>"Wyszukaj tytuł",

	'msg_saved'=>"Treść została zapisana",
	'msg_notsaved'=>"Treść nie została zapisana",
	'msg_delconfirm'=>"Czy na pewno usunąc tą treść?",
	'msg_deleted'=>"Treść została usunięta",

	'panel_title_list'=>"Lista treści",
	'panel_title_edit'=>"Edycja treści",
	'panel_title_new'=>"Nowa treść",
	'panel_title_copy'=>"Nowa treść z kopii",
];
