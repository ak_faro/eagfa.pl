<?
return [
	'model_id'=>"ID",
	'model_text'=>"Treść opinii",
	'model_user'=>"Autor opinii",
	'model_created'=>"Utworzono",
	'model_status'=>"Status",

	'status_new'=>"nowy",
	'status_ok'=>"zaakceptowany",

	'label_yourtext'=>"Twoja opinia",
	'label_signature'=>"Twój podpis",
	'label_sent'=>"Twoja opinia została zapisana",

	'btn_add_comm'=>"Dodaj swoją opinię",
	'btn_all_comm'=>"Zobacz wszystkie opinie",

	'list_items'=>"Lista komentarzy",

	'msg_delconfirm'=>"Czy na pewno usunąć komentarz?",
	'msg_deleted'=>"Komentarz został usunięty",
	'msg_published'=>"Komentarz został opublikowany",
	'msg_saved'=>"Komentarz został zakutalizowany",
	'msg_notsaved'=>"Komentarz nie został zakutalizowany",

	'err_no_text'=>"Proszę wpisać swoją opinię",
	'err_no_user'=>"Proszę się podpisać",
	'err_publish'=>"Komentarz nie został opublikowany",

	'panel_title_all'=>"Opinie naszych klientów",
	'panel_title_list'=>"Lista komentarzy",
	'panel_title_edit'=>"Edycja komentarza",
];