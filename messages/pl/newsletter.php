<?php
return [
	'model_email'=>"E-mail",
	'model_status'=>"Status",
	'model_created'=>"Utworzone",

	'status_new'=>"Nowy",
	'status_active'=>"Potwierdzony",

	'msg_subscribe'=>"E-mail został dodany do listy wysyłkowej.",
	'msg_unsubscribe'=>"E-mail został usunięty z listy wysyłkowej.",
	'msg_confirm'=>"Potwierdzono dodanie e-mail do listy wysyłkowej.",
	
	'err_subscribed'=>"Ten e-mail jest już dodany.",
	'err_subscribe'=>"Zapis nie powiódł się.",
	'err_nodata'=>"Nieprawidłowo wypełniony formularz.",
	'err_email'=>"Proszę wprowadzić poprawny adres e-mail.",
	'err_empty_model'=>"Nie ma takiej pozycji w liście wysyłkowej.",

];