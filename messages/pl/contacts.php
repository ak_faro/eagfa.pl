<?
return [
	'model_id'=>"ID",
	'model_created'=>"Data utworzenia",
	'model_firstname'=>"Imię",
	'model_lastname'=>"Nazwisko",
	'model_email'=>"E-mail",
	'model_phone'=>"Telefon",
	'model_notes'=>"Notatki",
	'model_owner'=>"Właściciel",
	'model_status'=>"Status",

	'name'=>"Nazwisko i Imię",
	'find_string'=>"Wyszukaj imię, nazwisko, email, telefon",
	'panel_title_list'=>"Lista kontaktów",
	'panel_title_show'=>"Podgląd kontaktu",
	'panel_title_new'=>"Dodaj kontakt",

	'label_msg_title'=>"Temat wiadomości",
	'label_msg_text'=>"Treść wiadomości",
	'label_status'=>"Pokaż ze statusem: ",
	'status_active'=>"Aktywny",
	'status_blocked'=>"Nieaktywny",
	'status_all'=>"Wszyscy",
	
	'btn_add'=>"Dodaj kontakt",

	'msg_saved'=>"Dane kontaktu zostały zapisane",
	'msg_notsaved'=>"Dane kontaktu nie zostały zapisane.",
	'msg_delconfirm'=>"Czy na pewno usunąć kontakt?",
	'msg_deleted'=>"Kontakt został usunięty",
	'msg_notdeleted'=>"Kontakt nie został usunięty",
	'msg_no_rows'=>"Lista kontaktów jest pusta",
	'msg_delnote'=>"Czy na pewno usunąć notatkę?",

	'err_not_permit'=>"Brak uprawnień do tego kontaktu",
	'err_no_note'=>"Wpisz treść notatki.",
	'err_no_model'=>"Kontakt nie istnieje w bazie.",

];
