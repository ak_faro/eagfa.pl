<?
return [
	'model_id'=>"ID",
	'model_name' => "Nazwa kategorii",
	'model_foto' => "Ikona kategorii",
	'model_sort' => "Kolejność wyświetlania",
	'model_subtitle' => "Podtytuł",
	'model_intro' => "Opis kategorii",
	'model_type'=>"Rodzaj kategorii",

	'order_first'=>"Jako pierwsza",
	'list_all'=>"Lista wszystkich kategorii",
	'list_type'=>"Lista kategorii: ",
	'type_all'=>"wszystkie",

	'panel_title_list'=>"Lista kategorii",
	'panel_title_new'=>"Nowa kategoria",
	'panel_title_edit'=>"Edycja kategorii",

	'msg_saved'=>"Kategoria została zapisana",
	'msg_notsaved'=>"Kategoria nie została zapisana",
	'msg_delconfirm'=>"Czy napewno usunąć kategorię?",
	'msg_deleted'=>"Kategoria została usunięta",

	'err_empty_model'=>"Nie ma takiej kategorii",
	'err_readonly'=>"Tej kategorii nie można usunąć",

];
