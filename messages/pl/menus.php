<?
return [
	'model_id'=>"ID",
	'model_name'=>"Nazwa",
	'model_link'=> "Odnośnik",
	'model_style'=>"Styl CSS",
	'model_class'=>"Klasa CSS",

	'ancestor'=>"Element nadrzędny",
	'order'=>"Kolejność po",
	'order_first'=>"Jako pierwsze",
	'order_last'=>"Jako ostatnie",
	'change_parent'=>"Zmień menu nadrzędne",
	'main_node'=>"[ jako nadrzędne ]",
	'childs_title'=>"Pozycje potomne",

	'btn_add'=>"utwórz",

	'panel_title_list'=>"Lista menu",
	'panel_title_edit'=>"Edytuj menu",
	'panel_title_new'=>"Nowe menu",

	'list_main'=>"Lista menu",
	'list_items'=>"Lista pozycji w menu:",

	'msg_saved'=>"Manu zapisane",
	'msg_notsaved'=>"Menu nie zostało zapisane",
	'msg_no_rows'=>"Lista pozycji menu jest pusta",
	'msg_delconfirm'=>"Czy na pewno usunąć?",
	'msg_deleted'=>"Menu zostało usunięte",
];