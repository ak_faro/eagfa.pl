<?
return [
	'model_id'=>"ID",
	'model_user'=>"Beneficjent",
	'model_created'=>"Data utworzenia",
	'model_amount'=>"Kwota rabatu",
	'model_status'=>"Status",

	'status_used'=>"Wykorzystany",
	'status_active'=>"Aktywny",
	'label_reason'=>"Powód",
	'panel_title_list'=>"Lista wystawionych kuponów",

	'msg_no_coupons'=>"Brak przydzielonych kuponów",
	'msg_no_rows'=>"Brak wystawionych kuponów",
];
