<?
namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Categories;
use app\models\Articles;

class articlesWidget extends Widget
{
	public $view="articles";
	public $catid;
	public $divid;
	public $width;
	public $limit=2;
	public $order="created desc";
	public $wrapcss=[];
	public $title=false;
	public $titlecss=[];
	public $more=true;

	private $articles;
	private $category;
	private $count=0;

	public function init()
	{
		parent::init();
		if (!$this->width) $this->width=Yii::$app->params['publicWidth'];
		$this->category=Categories::findOne($this->catid);
		if ($this->category) {
			if ($this->limit>0) {
				$this->articles=$this->category->getChildrens()->limit($this->limit)->orderBy($this->order)->all();
			} else {
				$this->articles=$this->category->getChildrens()->orderBy($this->order)->all();
			}
			$this->count=$this->category->getChildrens()->count();
		}
		if (!$this->divid) $this->divid="articles_".$this->catid;
	}

	public function run()
	{
		$param=[
			'width'=>$this->width,
			'articles'=>$this->articles,
			'category'=>$this->category,
			'wrapcss'=>$this->getCss($this->wrapcss),
			'titlecss'=>$this->getCss($this->titlecss),
			'divid'=>$this->divid,
			'limit'=>$this->limit,
			'more'=>$this->more,
			'count'=>$this->count
		];
		return $this->render($this->view.'.twig',$param);
	}

	private function getCss($data)
	{
		$out="";
		foreach($data as $k=>$v) {
			$out.=$k.":".$v.";";
		}
		return $out;
	}

} //end class