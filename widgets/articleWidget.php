<?
namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Articles;

class articleWidget extends Widget
{
	public $articleid;
	public $width;
	public $style=[];
	public $article;

	public function init()
	{
		parent::init();
		if (!$this->width) $this->width=Yii::$app->params['publicWidth'];
		$this->article=Articles::findOne($this->articleid);
		if (strlen($this->article->style)>0) $this->style=$this->article->styleArray;
	}

	public function run()
	{
		$param=[
			'width'=>$this->width,
			'article'=>$this->article,
			'style'=>$this->getCss($this->style),
		];
		return $this->render('article.twig',$param);
	}

	private function getCss($data)
	{
		$out="";
		foreach($data as $k=>$v) {
			$out.=$k.":".$v.";";
		}
		return $out;
	}

} //end class