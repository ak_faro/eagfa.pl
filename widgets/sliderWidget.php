<?
namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Articles;
use app\models\Categories;

class sliderWidget extends Widget
{
	public $catid;
	public $limit=0;
	public $order="created desc";
	public $timeout=5000;

	private $articles;
	private $category;

	public function init()
	{
		parent::init();
		$this->category=Categories::findOne($this->catid);
		if ($this->category) $this->articles=$this->category->getChildrens()->limit($this->limit)->orderBy($this->order)->all();
	}

	public function run()
	{
		$images=[];
		$timeouts=[];
		foreach($this->articles as $article) {
			$images[]=$article->image;//."?".rand(10000,99999);
			$timeouts[]=$article->style>0?$article->style:$this->timeout;
		}
		$param=[
			'articles'=>$this->articles,
			'maxslide'=>sizeof($this->articles)-1,
			'limit'=>$this->limit,
			'timeouts'=>$timeouts,
			'images'=>$images,
		];
		return $this->render('slider.twig',$param);
	}

} //end class