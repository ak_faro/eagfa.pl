<?
namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Menus;

class menuWidget extends Widget
{
	public $menuid;
	public $wrapcss;
	public $itemcss="menu-item";
	public $titlecss="menu-title";
	public $children=true; //pobrac podmenu?
	public $title=false;
	public $menu=null;
	public $items=[];

	public $colwidth=250; //szerokość kolumny w px
	public $cols=0; //ile kolumn

	public function init()
	{
		parent::init();
		if (isset($this->menuid)) {
			$this->menu=Menus::findOne($this->menuid);
			if ($this->menu!=null) {
				$this->items=$this->menu->children(1)->all();
			}
		}
		if ($this->cols==0) $this->cols=2;
	}

	public function run()
	{
		$param=[
			'menu'=>$this->menu,
			'items'=>$this->items,
			'itemcss'=>$this->itemcss,
			'titlecss'=>$this->titlecss,
			'wrapcss'=>$this->wrapcss,
			'childrens'=>$this->children,
			'title'=>$this->title,
			'colwidth'=>$this->colwidth,
			'cols'=>$this->cols,
		];
		return $this->render('menu.twig',$param);
	}


} //end class