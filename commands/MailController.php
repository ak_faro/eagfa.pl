<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\components\Emailer;
use \yii\db\Query;

class MailController extends Controller
{
	public function actionIndex()
	{
		$rows=(new Query)->select("*")->from('mailing')->all();
		foreach($rows as $row){
			if ($row['recipient']) {
				$result=Emailer::queue([
					'email'=>$row['recipient'],
					'sender_id'=>$row['creator'],
					'subject'=>$row['subject'],
					'body'=>$row['body'],
					'reply'=>Yii::$app->params['adminEmail'],
				]);
				if ($result) Yii::$app->db->createCommand()->delete('mailing', 'id = '.$row['id'])->execute();
			}
		}
	}

//end class
}