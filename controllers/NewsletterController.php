<?php
namespace app\controllers;

use Yii;
use app\models\Newsletter;
use yii\web\NotFoundHttpException;

class NewsletterController extends \app\components\BaseController
{

	public function actionSubscribe()
	{
		$out=['status'=>false];
		$data=$this->getJson();
		if (isset($data->email)) {
			$count=Newsletter::find()->where(['email'=>$data->email])->count();
			if ($count>0) {
				$out['msg']=Yii::t('newsletter','err_subscribed');
			} else {
				$model=new Newsletter();
				$model->email=$data->email;
				$model->status=0;
				$out['status']=$model->save();
				if (sizeof($model->errors)) $out['msg']=array_reduce($model->errors, 'array_merge', array());
			}
		} else {
			$out['msg']=Yii::t('newsletter','err_nodata');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionConfirm($hash="")
	{
		$model=Newsletter::findOne(['hash'=>$hash]);
		if ($model) {
			$model->status=1;
			$model->save();
			return $this->render('confirm.twig');
		} else {
			throw new NotFoundHttpException(Yii::t('newsletter','err_empty_model'));
		}
	}

	public function actionUnsubscribe($hash="")
	{
		$model=Newsletter::findOne(['hash'=>$hash]);
		if ($model) {
			$model->delete();
			return $this->render('unsubscribe.twig');
		} else {
			throw new NotFoundHttpException(Yii::t('newsletter','err_empty_model'));
		}
	}

//end class
}
