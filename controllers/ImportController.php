<?php
namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\Products;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ImportController extends \app\components\BaseController
{


	public function actionZalamo($type="",$hash="")
	{
		//Yii::info("type=".$type."&hash=".$hash);
		switch($type) {
			case 'project':
				if (strlen($hash)>20) {
					$import=$this->getImported('zalamo-proj',$hash);
					if ($import) {

						$params=[
							'panelTitle'=>Yii::t('panel','title_import'),
							'subtitle'=>"Projekt zalamo.com",
							'panelIcon'=>"import_export",
							'project'=>$import,
						];

						if (isset($import->grupa)) {
							$model=Products::findOne($import->grupa);
							if ($model) {
								$params['model']=$model;
								$params['urlparam']=['category'=>$model->category->name,'product'=>$model->name];
								if (isset($model->export->zalamo)&&isset($model->export->zalamo->second)&&isset($import->uniq)) {
									$params['urlparam'][$model->export->zalamo->second]=$import->uniq;
								}
								$params['urlparam']['src']="zalamo-proj";
								$params['urlparam']['hash']=$import->hash;
							}
						}

						if (Yii::$app->user->isGuest) {
							return $this->render('projectguest.twig',$params);
						} else {
							$orders=ArrayHelper::toArray(
								Yii::$app->user->identity->getOrders([0,1])->orderBy('id desc')->all(),
								['app\models\Orders'=>['id','status','statusLabel','statusData','brutto','created','items','projects']]
							);

							$params['orders']=$orders;

							return $this->render('projectuser.twig',$params);
						}
					} else {
						throw new NotFoundHttpException("Nieprawidłowe dane z serwisu zalamo.com");
					}
				break;
				}
			case 'print': //dane z odbitek
				//http://eagfa.localhost/import/zalamo/print/8543a988339b5e7e25c46dea8df379ca
				if (strlen($hash)>20) {
					$import=$this->getImported('zalamo-pic',$hash);
					if ($import&&isset($import->data)) {
						$items=Orders::printParse($import);
						if (sizeof($items)>0) {
							$model=Products::find()->where("export like '%type\":\"print%'")->one();

							if ($model && isset($model->export->zalamo)){
								$z=$model->export->zalamo;
								if (isset($z->type)&&$z->type=="print"&&isset($z->format)&&isset($z->paper)&&isset($z->fill)&&isset($z->papers)&&isset($z->fills)) {
									if (Yii::$app->request->get('redir',false)) {
										$session = Yii::$app->session;
										$basket=$session->has('basket')?$session->get('basket'):[];

										foreach($items as $item){
											$get=$model->zalamoPrintValues($item,false);
											$basket[]=[
												'item'=>$model->id,
												'amount'=>$item['amount'],
												'data'=>$model->getPrepParams(true,$get,true),
												'link'=>[
													'src'=>"zalamo-pic",
													'hash'=>$hash,
												],
											];
										}
										$session->set('basket',$basket);
										$this->redirect(['offer/basket']);
									} else {
										$params=[
											'panelTitle'=>Yii::t('panel','title_import'),
											'subtitle'=>"Odbitki zalamo.com",
											'panelIcon'=>"import_export",
											'items'=>$items,
											'urlparam'=>['type'=>"print",'hash'=>$hash,'redir'=>"true"],
											'model'=>$model,
										];

										if (Yii::$app->user->isGuest) {
											return $this->render('printguest.twig',$params);
										} else {
											$orders=ArrayHelper::toArray(
												Yii::$app->user->identity->getOrders([0,1])->orderBy('id desc')->all(),
												['app\models\Orders'=>['id','status','statusLabel','statusData','brutto','created','items','projects']]
											);
											$params['orders']=$orders;
											$params['project']=$import;

											return $this->render('printuser.twig',$params);
										}
									}
								} else {
									//nieprawidlowa konfiguracja
								}
							} else {
								//brak produktu
							}
						} else {
							throw new NotFoundHttpException("Nie udało się przetworzyć danych z serwisu zalamo.com");
						}
					} else {
						throw new NotFoundHttpException("Nieprawidłowe dane z serwisu zalamo.com");
					}
				break;
				}
			default:
				throw new NotFoundHttpException(Yii::t('orders','err_bad_import'));
		}
	}


//end class
}