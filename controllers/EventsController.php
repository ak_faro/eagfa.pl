<?php
namespace app\controllers;

use Yii;
use app\models\Events;
use app\models\Users;

class EventsController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator','merchant'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'calendar'=>[
				'class'=>"\app\components\IndexAction",
				'beforeFind'=>"prepCalendar",
				'map'=>[
					'app\models\Events'=>['id','title','description','start','end'=>'prepEnd','remind','targetLabel','targetLink','target_type','target_id','target'],
					'app\models\Contacts'=>['email','phone','name'],
					'app\models\Users'=>['email','phone','name']
				],
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
				'rule'=>"manageEvent",
				'field'=>"owner_id"
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('events','msg_saved'),
				'notsavedMsg'=>Yii::t('events','msg_notsaved'),
			],
			'list'=>[
				'class'=>"\app\components\ListAction",
				'map'=>[
					'app\models\Events'=>['id','title','startDate','remindDate','targetLabel','description','target_type','target_id','targetLink'],
				],
				'order'=>"created asc",
				'where'=>['and',
					['=','owner_id',Yii::$app->user->id],
					['>','remind',-1],
					'date_sub(start,interval remind minute)<=now()'
				]
			],
		];
	}

	public function prepCalendar($item)
	{
		$all=Yii::$app->user->can('administrator')&&(Yii::$app->request->get('all',false)||Yii::$app->request->get('owner',0));
		$getParams=[
			'start'=>[[],"start >= :start or end>=:start"],
			'end'=>[[],"start <= :end"],
		];
		//if (Yii::$app->user->can('administrator')) {
		//	$getParams['owner']=[[],'owner_id=:owner'];
		//}
		$item->getParams=$getParams;
		$item->where=$all?[]:['=','owner_id',Yii::$app->user->id];
		$item->pageSize=-1;
	}

	public function actionIndex()
	{
		$params=[
			'panelTitle'=>Yii::t('events','panel_title_list'),
			'panelIcon'=>"event",
			'target'=>['type'=>0,'id'=>0,'label'=>Yii::t('events','label_unspecifed')]
		];
		return $this->render("index.twig",$params);
	}

	public function actionClose($id=null)
	{
		$out=['saved'=>false];
		$request=(array)$this->getJson();
		if (isset($request['id'])) {
			$model=$this->loadModel($request['id']);
			if ($model) {
				if (Yii::$app->user->can('manageEvent',$model->owner_id)) {
					$model->remind=-1;
					$out['saved']=$model->save(false);
				} else {
					$out['msg']=Yii::t('events',"err_not_permit");
				}
			} else {
				$out['msg']=Yii::t('events',"err_no_model");
			}
		} else {
			$out['msg']=Yii::t('site',"err_bad_data");
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

//end class
}