<?php
namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\web\NotFoundHttpException;

class FilesController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions'=>['thumb','pic','crop'],
						'allow' => true,
						'roles' => ['?','@'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		/*$auth = Yii::$app->authManager;
		$rule = new \app\rbac\manageRule;

		$mc = $auth->createPermission('manageCustomer');
		$mc->description = 'Uprawnienie do zarządzania klientem';
		$mc->ruleName = $rule->name;
		var_dump($auth->add($mc));*/
	}

	public function actionPopup() {
		$params=[
			'baseurl'=>"/images",
			'listurl'=>"list-images",
			'layout'=>"simply"
		];
		return $this->render('manager.twig',$params);
	}

	public function actionManager($dir="") {
		$dirs=['images'=>"list-images",'pictures'=>'list-icons'];
		$params=[
			'panelTitle'=>Yii::t('files','panel_title'),
			'panelIcon'=>"folder_shared",
			'layout'=>"admin",
		];
		if (array_key_exists($dir,$dirs)) {
			$params['baseurl']="/".$dir;
			$params['listurl']=$dirs[$dir];
		} else {
			$params['baseurl']="/images";
			$params['listurl']=$dirs['images'];
		}
		return $this->render('manager.twig',$params);
	}

	public function actionDelete($base="img")
	{
		$out=[];
		$path=$this->prepDir();

		if ($path!==false) {
			$request=(array)$this->getJson();
			if (isset($request['items']) and is_array($request['items']) and sizeof($request['items'])>0) {
				$dir=$this->getBase($base).implode('/',$path);
				$out['count']=0;
				foreach ($request['items'] as $item) {
					if ($this->badFile($item)) continue;
					$p=$dir."/".$item;
					if (is_dir($p)) {
						$out['count']++;
						if (PHP_OS == 'WINNT') { exec("rd /s /q ".str_replace('/','\\',$p)); } else { exec("rm -rf ".$p); }
					} elseif (is_file($p)){
						if (@unlink($p)) $out['count']++;
					}
				}
			} else {
				$out['error']=Yii::t('files','err_no_items');
			}
		} else {
			$out['error']=Yii::t('files','err_dir_denied');
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionRename($base="img")
	{
		$out=[];
		$source=$this->prepDir();
		$target=$this->prepDir('target');
		if ($source!==false and $target!==false) {
			if (sizeof($target)==1 and $target[0]=="") $target=$source;
			$request=(array)$this->getJson();
			if (isset($request['items']) and is_array($request['items']) and sizeof($request['items'])>0) {
				$dir1=$this->getBase($base).implode('/',$source);
				$dir2=$this->getBase($base).implode('/',$source);
				$out['count']=0;
				foreach ($request['items'] as $k=>$item) {
					if ($this->badFile($item)) continue;
					$newitem=(isset($request['new'][$k]) and !$this->badFile($request['new'][$k]))?$request['new'][$k]:$item;
					if (file_exists($dir1."/".$item) && $dir1."/".$item!=$dir2."/".$newitem) {
						if (rename($dir1."/".$item,$dir2."/".$newitem)) $out['count']++;
					}
				}
			} else {
				$out['error']=Yii::t('files','err_no_items');
			}
		} else {
			$out['error']=Yii::t('files','err_dir_denied');
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionCreate($base="img")
	{
		$path=$this->prepDir();
		if ($path!==false) {
			$request=(array)$this->getJson();
			if (isset($request['item']) and !$this->badFile($request['item'])) {
				$dir=$this->getBase($base).implode('/',$path);
				if (is_dir($dir)) {
					if (isset($request['type']) and $request['type']=="dir") {
						if (@mkdir($dir."/".$request['item'])) $out['count']=1;
							else $out['error']=Yii::t('files','err_not_created');
					} else {
						if (touch($dir."/".$request['item'])) $out['count']=1;
							else $out['error']=Yii::t('files','err_not_created');
					}
				} else {
					$out['error']=Yii::t('files','err_bad_dir');
				}
			} else {
				$out['error']=Yii::t('files','err_no_items');
			}
		} else {
			$out['error']=Yii::t('files','err_dir_denied');
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionUpload($base="img")
	{
		$out=[];
		$path=$this->prepDir('dir','post');
		if ($path!==false) {
			$file=UploadedFile::getInstanceByName('file');
			if ($file!==null) {
				$dir=$this->getBase($base).implode('/',$path);
				if($file->saveAs($dir."/".$file->baseName.'.'.$file->extension)) $out['count']=1;
					else $out['count']=0;
			} else {
				$out['error']=Yii::t('files','err_no_file');
			}
		} else {
			$out['error']=Yii::t('files','err_bad_dir');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionThumb($file)
	{
		$this->createThumb(urldecode($file),92,68,"img");
	}

	public function actionThumb2($file)
	{
		$this->createThumb(urldecode($file),92,68,"ico");
	}

	public function actionPic($file)
	{
		$this->createThumb(urldecode($file),135,100,"ico");
	}

	public function actionCrop($file)
	{
		$this->createThumb(urldecode($file),156,156,"ico");
	}

	public function createThumb($file,$w=92,$h=68,$folder="img")
	{
		$path=$this->getBase($folder)."/".urldecode($file);
		if (!preg_match("/\.\.\//",$file) and is_file($path)){ 
			try {
				Image::thumbnail($path,$w,$h)->show('png');
				Yii::$app->response->format=\yii\web\Response::FORMAT_RAW;
			} catch (\Exception $e) {
				throw new NotFoundHttpException(Yii::t('site','err_bad_data'));
			}
		} else {
			throw new NotFoundHttpException(Yii::t('site','err_bad_data'));
		}
	}

	public function actionListImages()
	{
		return $this->listFiles(Yii::getAlias('@images'),'/images');
	}

	public function actionListIcons()
	{
		return $this->listFiles(Yii::getAlias('@icons'),'/pictures');
	}

	private function listFiles($base,$urlbase="/")
	{
		$out=[
			'base'=>$urlbase,
		];
		$path=$this->prepDir();

		if ($path!==false) {
			$dir=$base.implode('/',$path);
			if (is_dir($dir)) {
				$out['items']=[];
				foreach(new \DirectoryIterator($dir) as $item){
					if($item->isDot()) continue;
					$f=$this->fileType($item->getExtension());
					$item=[
						'dir'=>$item->isDir()?1:0,
						'name'=>$item->getFileName(),
						'size'=>$item->getSize(),
						'type'=>$f['type'],
						'icon'=>$f['icon'],
					];
					$out['items'][]=$item;
				}
			} else {
				$out['error']=Yii::t('files','err_bad_dir');
			}
		} else {
			$out['error']=Yii::t('files','err_dir_denied');
		}
		$out['path']=$path;
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	private function prepDir($v="dir",$type='json')
	{
		$request=$type=='json'?(array)$this->getJson():$_POST;
		if (!isset($request[$v]) or $request[$v]=="/" or !is_string($request[$v])) return [""];
		$dir=explode("/",$request[$v]);
		if ($dir[0]!="") return false;
		if (sizeof(array_keys($dir,''))>1) return false;
		if (array_search("..",$dir)) return false;

		$allowedDirs=[''];
		if (!in_array($dir[0],$allowedDirs)) return false;
		return $dir;
	}

	private function fileType($ext)
	{
		switch(strtolower($ext)) {
			case 'jpg': case 'jpeg': case 'gif': case 'png': $icon=$type="image"; break; //case 'ico':
			case 'wav': case 'ogg': case 'mp3': $icon=$type="audio"; break;
			case 'mp4': case 'avi': case '3gp': case 'mpg': case 'mov': $icon=$type="video"; break;
			case 'doc': case 'docx': case 'xls': case 'xlsx': case 'txt': case 'odf': case 'rtf': $icon=$type="document"; break;
			case 'zip': case 'rar': case 'gz': case '7z': $icon=$type="archive"; break;
			case 'pdf': $icon="pdf"; $type="document"; break;
			default: $icon=$type="file";
		}
		return compact('icon','type');
	}

	private function getBase($key)
	{
		//exeption jak nie ma key
		return Yii::getAlias(Yii::$app->params['folders'][$key]);
	}

	private function badFile($item)
	{
		return (in_array($item,['','.','..',';','|']) or sizeof(explode("/",$item))>1)?true:false;
	}

//end class
}
