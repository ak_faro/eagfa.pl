<?php
namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use app\models\Products;
use app\models\Categories;
use app\models\Orders;
use app\models\Coupons;


class OfferController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['buying','finalize'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['buying','finalize'],
						'allow' => false,
						'roles' => ['?'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['?','@'],
					],
				],
			],
		];
	}

	/*public function actionMigrate()
	{
		$models=Products::find()->all();
		foreach($models as $model) {
			$params=[];
			foreach($model->params as $p) {
				if (isset($p->visible)) $p->cond[]=$p->visible;
				if(!isset($p->cond)) $p->cond=[];
				if (property_exists($p,'visible')) unset($p->visible);
				$params[]=$p;
			}
			
			$model->params=$params;
			$model->save();
		}
	}*/

	public function actionIndex()
	{
		$params=[
			'categories'=>Categories::getCategories(0),
			'catlist'=>ArrayHelper::toArray(Categories::getCategories(0),['app\models\Categories'=>['id','name','foto'=>function($item){ return $item->getFoto(); },'intro']]),
		];
		return $this->render('index.twig',$params);
	}

	public function actionCategory($category)
	{
		$model=Categories::findOne(['name'=>$category,'type'=>0]);

		if ($model==null) throw new NotFoundHttpException(Yii::t('categories','err_empty_model'),404);

		$params=[
			'model'=>$model,
			'categories'=>$model::getCategories(0),
			'childs'=>ArrayHelper::toArray($model->childrens,['app\models\Products'=>['id','name','foto'=>function($item){ return $item->getFoto(); },'intro']]),
		];

		return $this->render('category.twig',$params);
	}

	public function actionProduct($category,$product)
	{
		$model=Products::find()
			->joinWith('category')
			->where(['products.name'=>$product,'categories.name'=>$category])
			->one();

		if ($model==null) throw new NotFoundHttpException(Yii::t('products','err_empty_model'),404);

		//dodatkowe parametry do ustawienia w podukcie
		$get=Yii::$app->request->get();
		if (isset($get['category'])) unset($get['category']);
		if (isset($get['product'])) unset($get['product']);
		if (isset($get['src'])) unset($get['src']);
		if (isset($get['hash'])) unset($get['hash']);

		$params=[
			'model'=>$model,
			'categories'=>Categories::getCategories(0),
			'order'=>[
				'index'=>null,
				'amount'=>1,
				'data'=>$model->getPrepParams(false,$get),
			],
		];
		if (Yii::$app->request->get('hash','') && Yii::$app->request->get('src','')) {
			$params['link']=[
				'hash'=>Yii::$app->request->get('hash'),
				'src'=>Yii::$app->request->get('src')
			];
		} else {
			$params['link']=null;
		}

		return $this->render('product.twig',$params);
	}

	public function actionBasket()
	{
		$items=$this->prepBasket();
		return $this->render('basket.twig',[
			'items'=>$items,
			'categories'=>Categories::getCategories(0),
		]);
	}

	//zmiana pozycji w koszyku
	public function actionChange($pos=null)
	{
		$session = Yii::$app->session;
		if ($pos!==null && $session->has('basket') && isset($session['basket'][$pos])) {
			$row=$session['basket'][$pos];
			$model=Products::findOne($row['item']);
			if ($model!=null && isset($row['data'])){
				$data=[];
				foreach($model->getPrepParams(false) as $k=>$p) {
					$data[$k]=$p;
					if (isset($row['data'][$k])) {
						$data[$k]->data=$row['data'][$k];
						$data[$k]->data->price=$model->findPrice($p,$row['data'][$k],$row); //aktualizacja ceny
					}
				}
				$params=[
					'model'=>$model,
					'categories'=>Categories::getCategories(0),
					'order'=>[
						'index'=>$pos,
						'amount'=>$row['amount'],
						'data'=>$data,
					],
					'link'=>isset($row['link'])?$row['link']:null,
				];
				return $this->render('product.twig',$params);
			}
		}
		throw new NotFoundHttpException(Yii::t('site','err_empty_model'),404);
	}

	public function actionStore()
	{
		$out=[
			'saved'=>false
		];
		$r=$this->getJson();
		if (isset($r->mode)) {
			$session = Yii::$app->session;

			switch ($r->mode) {
				case 'amount':
					if ($session->has('basket') && sizeof($session['basket'])>0
						&& isset($r->amount) && is_array($r->amount) && sizeof($r->amount)>0 && sizeof($session['basket'])==sizeof($r->amount)) {
						$basket=$session->get('basket');
						for($i=0;$i<sizeof($basket);$i++) {
							if ($r->amount[$i]>0) $basket[$i]['amount']=$r->amount[$i];
								else unset($basket[$i]);
						}
						$session->set('basket',array_values($basket));
						$out['saved']=true;
					}
					break;
				case 'update':
					if ($session->has('basket')) $basket=$session->get('basket');
					if (property_exists ($r,'index') && isset($r->item) && isset($r->data)) {
						$model=Products::findOne($r->item);
						if ($model) {
							$row=[
								'item'=>$model->id,
								'amount'=>(isset($r->amount)?$r->amount:1),
								'data'=>$model->updatePrices($r->data), //aktualizacja cen
								'link'=>(isset($r->link)?(array)$r->link:null),
							];
							if ($r->index===null) {
								$basket[]=$row;
								end($basket);
								$out['index']=key($basket);
							} else {
								$basket[$r->index]=$row;
								$out['index']=$r->index;
							}
							$session->set('basket',$basket);
							$out['saved']=true;
						}
					} else {
						$out['msg']='bad input';
					}
					break;
				case 'del':
					if (isset($r->index) && isset($session['basket'][$r->index])) {
						$basket=$session['basket'];
						unset($basket[$r->index]);
						$session->set('basket',array_values($basket));
						$out['saved']=true;
					}
					break;
				case 'clear':
					$session->set('basket',[]);
					$out['saved']=true;
					break;
			}
			$session->close();
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionBuying()
	{
		$items=$this->prepBasket();

		return $this->render('buying.twig',[
			'items'=>$items,
			'categories'=>Categories::getCategories(0),
			'locations'=>Orders::getLocations(),
			'shiptypes'=>Orders::getShiptypes(),
			'paytypes'=>Orders::getPayments(),
			'invoicetypes'=>Orders::getinvoicetypes(),
			'coupons'=>ArrayHelper::toArray(Yii::$app->user->identity->getCoupons(1)->all(),['app\models\Coupons'=>['id','amount','reason']])
		]);
	}

	public function actionFinalize()
	{
		$out=['saved'=>false];
		$request=(array)$this->getJson();
		$session = Yii::$app->session;
		if ($session->has('basket') && sizeof($session['basket'])>0 && $request) {
			$items=[]; $total=0;
			$basket=$session->get('basket');
			$projects=[];

			foreach($basket as $k=>$pos) {
				$model=Products::findOne($pos['item']);
				if ($model) {
					$data=[];
					$items[$k]['price']=$model->price;
					foreach($pos['data'] as $i=>$v){
						if (isset($v->name)&&isset($model->params[$i])&&isset($model->params[$i]->name)) {
							$data[$i]=[
								'name'=>$model->params[$i]->name,
								'value'=>$model->findNotes($model->params[$i],$v->name),
								'price'=>$model->findPrice($model->params[$i],$v,$basket[$k]),
							];
							$items[$k]['price']+=$data[$i]['price'];
						} else {
							$data[$i]=null;
						}
					}
					$items[$k]['config']=array_values($data);
					$items[$k]['amount']=$pos['amount'];
					$items[$k]['amountprice']=$model->amountPrice($pos['data'],$pos['amount']);
					$items[$k]['price']+=$items[$k]['amountprice'];
					$items[$k]['id']=$model->id;
					$items[$k]['name']=$model->name;
					$items[$k]['discount']=$model->discount;
					if ($items[$k]['price']>0 && $items[$k]['discount']>0) {
						$items[$k]['price2']=$items[$k]['price']-round($items[$k]['price']*($items[$k]['discount']/100),2);
					}
					$items[$k]['category']=$model->category->name;
					$total+=(isset($items[$k]['price2'])?$items[$k]['price2']:$items[$k]['price'])*$items[$k]['amount'];

					//projects
					if (isset($pos['link'])) {
						$pos['link']=ArrayHelper::toArray($pos['link']);
						if (is_array($pos['link'])&& isset($pos['link']['src']) && isset($pos['link']['hash'])) {
							$data=$this->getImported($pos['link']['src'],$pos['link']['hash']);
							if ($data) $projects[$k]=[$data];
						}
					}
				}
			}

			if (sizeof($items)>0 && $total>0) {
				$model=new Orders();
				$model->items=$items;
				$model->shipping=(isset($request['ship'])&&isset($request['ship']->type))?$request['ship']->type:null;
				$model->invoice=(isset($request['invoice'])&&isset($request['invoice']->type))?$request['invoice']->type:0;
				$model->payment=isset($request['payment'])?$request['payment']:null;
				$params=[];
				if ($model->shipping==1) {
					$params['collect']=(isset($request['ship']->collect)?intval($request['ship']->collect):0);
					$params['shipCost']=0;
				} else {
					$params['address']=isset($request['ship']->address)?$request['ship']->address:[];
					$params['shipCost']=$model->shipCost;
				}
				if ($model->invoice==1) {
					$params['invoice']=isset($request['invoice']->address)?$request['invoice']->address:[];
				}

				Yii::$app->user->identity->paramsfromOrder($params);

				if (isset($request['notice'])) $params['notice']=trim(strip_tags($request['notice']));
				if (sizeof($projects)>0) $params['projects']=$projects;

				$model->user_id=Yii::$app->user->id;
				$model->user_group=Yii::$app->user->identity->group;
				$model->user_name=Yii::$app->user->identity->name;
				$model->brutto=$total+$model->shipCost;
				$coupons=isset($request['coupons'])?$request['coupons']:null;
				if ($coupons) {
					$discount=Coupons::prepDiscount(Yii::$app->user->id,$coupons);
					if (sizeof($discount[1])>0) {
						$params['couponDiscount']=$discount[0];
						$params['coupons']=$discount[1];
					}
				}
				$model->params=$params;

				if (!isset($discount)||(isset($discount)&&$model->brutto>=$discount[0])) {
					if (isset($discount)) $model->brutto-=$discount[0];
					if ($model->save()) {
						$session->set('basket',[]);
						$out['saved']=true;
						$out['id']=$model->id;
						Yii::$app->session->setFlash('orderCreated',true);
					} else {
						$out['msg']=array_reduce($model->errors, 'array_merge', array()); ;
					}
				} else {
					$out['msg']=Yii::t('orders','err_tomuch_coupons');
				}
			} else {
				$out['msg']=Yii::t('orders','err_no_items');
			}
		} else {
			$out['msg']=Yii::t('orders','err_empty_basket');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	private function prepBasket($throw=true)
	{
		$items=[];
		$session = Yii::$app->session;
		if ($session->has('basket') && sizeof($session['basket'])>0) {
			$basket=$session->get('basket');

			foreach($basket as $k=>$pos) {
				$model=Products::findOne($pos['item']);
				if ($model) {
					$items[$k]=$pos;
					$items[$k]['name']=$model->name;
					$items[$k]['aprices']=$model->amountPrices($pos['data']);
					$items[$k]['aprice']=$model->amountPrice($pos['data'],$pos['amount']);
					$items[$k]['price']=$model->price+$model->paramSum($pos['data'],$basket[$k]);
					$items[$k]['category']=$model->category->name;
					$items[$k]['discount']=$model->discount;
					if (($items[$k]['price']+$items[$k]['aprice'])>0 &&$items[$k]['discount']>0) {
						$items[$k]['price2']=$items[$k]['price']-($items[$k]['price']*($items[$k]['discount']/100));
						$items[$k]['aprice2']=$items[$k]['aprice']-($items[$k]['aprice']*($items[$k]['discount']/100));
					}
					if (isset($items[$k]['data'])) unset($items[$k]['data']);
					$items[$k]['config']=$model->getConfiguration($pos['data']);
				} else {
					unset($basket[$k]);
				}
			}
			$session->set('basket',$basket);
		} else {
			if ($throw) throw new NotFoundHttpException(Yii::t('offer','err_empty_basket'));
		}
		return $items;
	}


//end class
}
