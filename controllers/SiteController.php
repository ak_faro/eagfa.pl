<?php
namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Products;
use app\models\Articles;
use app\components\Emailer;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use app\components\Freshmail;

class SiteController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login','remind','register','activation'],
						'allow' => false,
						'roles' => ['@'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['?','@'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$params=[
			'popup'=>false,
		];
		if (Yii::$app->params['popup']['visible']||Yii::$app->request->get('popup',false)) {
			$params['popup']=true;
			$params['article']=Articles::findOne(Yii::$app->params['popup']['article']);
		}
		return $this->render('index.twig',$params);
	}

	public function actionTest($m="")
	{
		//		$vemail = new \yii\validators\EmailValidator();
		//		var_dump($vemail->validate($m));
		/*$all=\app\models\Orders::find()->where('id>3000')->all();
		foreach($all as $o){
			$o->user_name=$o->user->name;
			$o->user_group=$o->user->group;
			var_dump($o->id,$o->user_name,$o->user_group,$o->save(false));
		}*/

	}

	public function actionRemind()
	{
		if (Yii::$app->request->isPost) {
			$out=['status'=>false];
			$data=$this->getJson();
			if (isset($data->email)&&strlen($data->email)>=5) {
				$model=Users::findOne(['email'=>$data->email]);
				if ($model&&$model->id>1) {
					if (strlen($model->hash)<1) {
						$model->scenario="remind";
						$model->hash=$model->calcHash();
						$model->save();
					}
					Emailer::queue([
						'email'=>$model->email,
						'sender_id'=>Yii::$app->user->isGuest?-1:Yii::$app->user->id,
						'schema_id'=>17,
						'tags'=>[
							'[link]'=>Url::to(['site/reset','pass'=>$model->hash], true),
						]
					]);
					$out['status']=true;
				} else {
					$out['msg']=Yii::t('users','err_no_model');
				}
			} else {
				$out['msg']=Yii::t('site','err_pass_noemail');
			}
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $out;
		} else {
			return $this->render('remindpass.twig');
		}
	}

	public function actionReset($pass="")
	{
		if (Yii::$app->request->isPost) {
			$out=['status'=>false];
			$data=$this->getJson();
			if (isset($data->hash)) {
				$model=Users::findOne(['hash'=>$data->hash]);
				if ($model) {
					$model->scenario="resetpass";
					$model->password=isset($data->password)?$data->password:"";
					$model->password2=isset($data->password2)?$data->password2:"";
					if ($model->validate()) {
						$model->hash="";
						$model->password=$model->calcPassword($model->password,$model->salt);
						$out['status']=$model->save(false);
					}
					if (sizeof($model->errors)) $out['msg']=array_reduce($model->errors, 'array_merge', array());
				} else {
					$out['msg']=Yii::t('users','err_no_model');
				}
			} else {
				//out err
				$out['msg']=Yii::t('site','err_bad_data');
			}
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $out;
		} else {
			if (strlen($pass)>30) {
				$model=Users::findOne(['hash'=>$pass]);
				if ($model) {
					return $this->render('resetpass.twig',['hash'=>$pass]);
				}
			}
			throw new NotFoundHttpException(Yii::t('users','err_no_model'));
		}
	}

	public function actionCookie()
	{
		Yii::$app->session->set('cookie',true);
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return ['hide'=>true];
	}

	public function actionContact()
	{
		return $this->render('contact.twig');
	}

	public function actionCatalog()
	{
		return $this->render('catalog.twig');
	}

	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) return $this->goHome();

		$model = new Users(['scenario' => 'login']);
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			$url=Yii::$app->user->returnUrl;
			if ($url!="/") {
				Yii::$app->user->setReturnUrl(Yii::$app->homeUrl);
			} else {
				//do paneli
				if (Yii::$app->user->can('administrator')) {
					$url=['dashboard/index'];
				}
				if (Yii::$app->user->can('merchant')) {
					$url=['merchant/index'];
				}
				if (Yii::$app->user->can('customer')) {
					$url=['panel/index'];
				}
			}
			return $this->redirect($url);
		} else {
			if (Yii::$app->request->get('return',null)) {
				Yii::$app->user->setReturnUrl(Yii::$app->request->get('return'));
			}
			return $this->render('login.twig',['model'=>$model]);
		}
	}

	public function actionLogout()
	{
		if (Yii::$app->user->isGuest) return $this->goHome();
		Yii::$app->user->logout(false);
		return $this->goHome();
	}

	public function actionRegister() {
		$session = Yii::$app->session;
		$code=Yii::$app->request->get('code',null);
		if ($code) $session->set('regcode',$code);

		$model=new Users();
		$model->scenario="register";
		$model->load(Yii::$app->request->post());

		if (Yii::$app->request->isPost&&$model->validate()) {
			$model->group=null;
			$model->refer=null;
			if ($session->has('regcode')) {
				$check=Users::roleByCode($session->get('regcode',0));
				if ($check[0]>0) {
					if ($check[1]=="merchant") $model->group=$check[0];
					if ($check[1]=="customer") {
						$model->refer=$check[0];
						$model->group=$check[2];
					}
				}
			}
			if ($model->save(false)) {
				$auth = Yii::$app->authManager;
				$aresult=$auth->assign($auth->getRole('customer'),$model->getId());

				Yii::info(['mode'=>"register",'userid'=>$model->getId(),'result'=>$aresult],"authassign");

				$session->set('regcode',null);

				if ($model->group>0) {
					Emailer::queue([
						'email'=>$model->merchant->email,
						'schema_id'=>35,
						'tags'=>[
							'[name]'=>$model->name,
							'[phone]'=>$model->phone,
							'[email]'=>$model->email,
							'[created]'=>date($model::$db_save_datetime),
						],
					]);
				}

				//$model->login(); //autologowanie?
				//return $this->redirect(["dashboard/index"]);
				$article=Articles::findOne(18);
				return $this->render('@app/views/articles/show.twig',['model'=>$article]); exit;
			}
		}
		return $this->render('register.twig',['model'=>$model]);
	}

	public function actionMessage()
	{
		$out= ['status'=>false];
		$r=$this->getJson();

		if (isset($r->name)&&isset($r->email)&&isset($r->phone)&&isset($r->text)) {
			if (strlen($r->name)>=4&&strlen($r->phone)>=9&&strlen($r->email)>=5) {
				if(strlen($r->text)>5) {
					$out['status']=Emailer::queue([
						'email'=>Yii::$app->params['adminEmail'],
						'schema_id'=>7,
						'reply'=>$r->email,
						'tags'=>[
							'[message]'=>$r->text,
							'[name]'=>$r->name,
							'[phone]'=>$r->phone,
							'[email]'=>$r->email
						]
					]);
				} else {
					$out['msg']=Yii::t('site','err_form_empty');
				}
			} else {
				$out['msg']=Yii::t('site','err_form_anonymous');
			}
		} else {
			$out['msg']=Yii::t('site','err_form_fill');
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionError()
	{
		$param=[
			'exception' => Yii::$app->errorHandler->exception
		];
		return $this->render('error.twig',$param);
	}


//end class
}
