<?php
namespace app\controllers;

use Yii;
use app\models\Products;
use app\models\Orders;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class ApiController extends \app\components\BaseController
{

	public function actionStructure()
	{
		$out=[];
		$models=Products::find()->where('length(export)>1')->all();
		foreach($models as $model) {
			if (isset($model->export->zalamo)&&isset($model->export->zalamo->type)){
				$z=$model->export->zalamo;
				if ($z->type=="print") continue;

				$out[$model->group]['id']="cat_".$model->category->id;
				$out[$model->group]['label']=$model->category->name;
				//$out[$model->group]['id']=$model->category->id;
				$out[$model->group]['thumb']=Url::to($model->category->getFoto(),true);

				$line=[
					'label'=>$model->name,
					'id'=>$model->id,
					'thumb'=>Url::to($model->getFoto(),true),
				];

				if (isset($z->second)) {
					foreach($model->params as $param) {
						if ($param->name==$z->second) {
							if (isset($param->options)) {
								$line['children']=ArrayHelper::getColumn($param->options,'name');
								$thumb=ArrayHelper::map($param->options,'name','data');
							}
							break;
						}
					}
				}
				if (isset($line['children'])) {
					if (isset($z->amount)) {
						foreach($model->params as $param) {
							if ($param->name==$z->amount) {
								$min=(isset($param->text)&&isset($param->text->min))?$param->text->min:1;
								$max=(isset($param->text)&&isset($param->text->max))?$param->text->max:0;
								break;
							}
						}
					}

					$s=Yii::$app->params['zalamo']['sizes'];
					foreach ($line['children'] as $k=>$v) {
						$line['children'][$k]=[
							'id'=>$model->id."_".$v,
							'label'=>$v,
							'type'=>isset($z->type)?$z->type:'album',
							'width'=>isset($s[$v][0])?$s[$v][0]:0,
							'height'=>isset($s[$v][1])?$s[$v][1]:0,
							'margin'=>isset($s[$v][2])?$s[$v][2]:Yii::$app->params['zalamo']['margin'],
							'bleed'=>isset($s[$v][3])?$s[$v][3]:Yii::$app->params['zalamo']['bleed'],
							'min'=>$min,
						];
						if ($max>0) $line['children'][$k]['max']=$max;
						if (isset($thumb[$v])) $line['children'][$k]['thumb']=Url::to("/pictures/".$thumb[$v],true);
					}
				}
			}
			$out[$model->group]['children'][]=$line;
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return array_values($out);
	}

	public function actionZalamo($key="")
	{
		if ($key=="abc1231") {
			$out=[];
			$models=Orders::find()->where(['status'=>[0,1]])->andWhere('params like "%zalamo%"')->all();
			foreach ($models as $model) {
				if (isset($model->params->projects)&&$model->params->projects!=null) {
					foreach($model->params->projects as $k=>$projects) {
						foreach ($projects as $p=>$project) {
							if (isset($project->provider) && $project->provider=="zalamo"&&isset($project->hash)) {
								$out[]=[
									'source'=>$model->projectUrl(ArrayHelper::toArray($project)),
									'user'=>$model->user->ftpname,
									'order'=>$model->id,
									'item'=>$k.(sizeof($projects)>1?"_".$p:""), //item pos in order
									'mode'=>isset($project->mode)?$project->mode:"",
								];
							}
						}
					}
				}
			}
			//print_r($out);
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return $out;
		} else {
			//notfound?
		}
	}


//end class
}
