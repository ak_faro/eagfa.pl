<?php
namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use app\components\Emailer;
use app\models\Users;
use app\models\Orders;
use app\models\Articles;
use app\models\Events;

class MerchantController extends \app\components\BaseController
{

	public $model="Users";

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['merchant'],
					],
				],
			],
		];
	}

	public function actions()
	{
		$group_id=Yii::$app->user->id;
		return [
			'clients'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('users','panel_title2_list'),
				'panelIcon'=>"account_box",
				'model'=>'app\models\Users',
				'view'=>"clients.twig",
				'map'=>[
					'app\models\Users'=>['id','firstname','lastname','name','email','status','vip','statusLabel','ordersCount','ordersSum','lastDate','lastDays',
												//'removable'=>function($item){ return (bool)$item->removable; },
												'created'=>function($item) { return $item::displayDate($item->created); },
											  ],
				],
				'params'=>[
					'listTitle'=>Yii::t('users','list2_all'),
					'paramTitle'=>Yii::t('users','list_status2_z'),
					'group'=>Users::findOne($group_id),
					'message'=>Articles::findOne(Yii::$app->params['schema']['invite']),
					'statuses'=>Users::getStatuses(),
					'priorities'=>Users::getPriorities(),
				],
				'where'=>[
					'=','group',$group_id
				],
				'join'=>[
					['left outer join','orders_stat','orders_stat.user_id='.Users::tableName().'.id'],
				],
				'sort'=>['lastname','ilosc','brutto','days'],
				'getParams'=>[
					'vip'=>[[],'users.vip=:vip'],
					'status'=>[[],'users.status=:status'],
					'string'=>[[],"email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)"],
				],
				'beforeFind'=>"clientsPrepare"
			],
			'orders'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('orders','panel_title_list'),
				'model'=>'app\models\Orders',
				'view'=>"orders.twig",
				'map'=>[
					'app\models\Orders'=>['id','status','statusLabel','statusData','brutto','user','user_name','created'],
					'app\models\Users'=>['id','name','email','vip']
				],
				'params'=>[
					'listTitle'=>Yii::t('orders',(Yii::$app->request->get('status',false)!==false?'list_status':'list_all')),
					'statuses'=>Orders::getStatuses(),
				],
				//'join'=>[
				//	['inner join','users','orders.user_id='.Users::tableName().'.id'],
				//],
				'where'=>[
				//	'=','users.group',$group_id
					'=','user_group',$group_id
				],
				'getParams'=>[
					'status'=>[[],'orders.status=:status'], //table,where_params
					'start'=>[[],"orders.created>=:start"],
					'end'=>[[],"orders.created<=:end"],
				],
				'summary'=>['brutto'],
				'order'=>'orders.id desc'
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('users','msg_saved'),
				'notsavedMsg'=>Yii::t('users','msg_notsaved'),
				'scenario'=>"user",
				'modelId'=>Yii::$app->user->id,
			],
			'savepass'=>[
				'class'=>"\app\components\PassAction",
			],
			'invite'=>[
				'class'=>"\app\components\InviteAction",
			],
		];
	}

	public function actionIndex()
	{
		$params=[
			'panelTitle'=>"Dashboard",
			'panelIcon'=>"dashboard",
			'target'=>['type'=>0,'id'=>0,'label'=>Yii::t('events','label_unspecifed')],
			//'events'=>ArrayHelper::toArray(Events::find()->where(['owner_id'=>Yii::$app->user->id])->all(),[
			//	'app\models\Events'=>['id','title','description','start','end'=>'prepEnd','remind','targetLabel','target_type','target_id'],
			//])
		];
		$params['orders']=Orders::find()->orderBy('id desc')->limit(10)
			->where(['=','user_group',Yii::$app->user->id])->all();
			//->join('inner join','users','orders.user_id=users.id')
			//->where(['=','users.group',Yii::$app->user->id])->all();
		return $this->render('index.twig',$params);
	}


	public function actionSettings()
	{
		$params=[
			'panelTitle'=>"Ustawienia",
			'panelIcon'=>"settings",
			'user'=>ArrayHelper::toArray(
				Yii::$app->user->identity,
				['app\models\Users'=>['id','firstname','lastname','phone','email','name','params']]
			),
		];
		return $this->render('settings.twig',$params);
	}

	public function actionExport($mode="")
	{
		switch($mode){
			case 'emails':
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Disposition: attachment; filename=kontakty.csv');

				$output = fopen('php://output', 'w');
				fputcsv($output, array('E-mail', 'Imię', 'Nazwisko'));
				foreach (Yii::$app->user->identity->getCustomers()->select(['email','firstname','lastname'])->asArray()->all() as $row) {
					fputcsv($output, $row);
				}
				break;
			default:
				throw new NotFoundHttpException(Yii::t('site','err_bad_data'));
		}
	}

	public function clientsPrepare(&$c)
	{
		$buyer=Yii::$app->request->get('buyer',null);
		if ($buyer==="0") {
			$c->getParams['buyer']=[[],'orders_stat.ilosc is null',true];
		}
		if ($buyer==1) {
			$c->getParams['buyer']=[[],'orders_stat.ilosc>0',true];
		}
		if ($buyer==2) {
			$c->getParams['buyer']=[[],'orders_stat.brutto>0 and orders_stat.days>90 and orders_stat.days<=120',true];
		}
	}
//end class
}
