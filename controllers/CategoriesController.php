<?php
namespace app\controllers;

use Yii;
use app\models\Categories;

class CategoriesController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('categories','panel_title_list'),
				'panelIcon'=>"menu",
				'map'=>[
					'app\models\Categories'=>['id','name','typeLabel','childs']
				],
				'params'=>[
					'listTitle'=>Yii::t('categories',(Yii::$app->request->get('status',false)!==false?'list_status':'list_type')),
					'types'=>Categories::getTypes(),
				],
				'getParams'=>[
					'type'=>[[],'categories.type=:type'], //table,where_params
				],
			],
			'create'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('menus','panel_title_new'),
				'panelIcon'=>"menu",
				'map'=>[
					'app\models\Categories'=>['name','foto','subtitle','sort','type','intro']
				],
				'params'=>[
					'types'=>Categories::getTypes(),
				],
			],
			'edit'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('menus','panel_title_edit'),
				'panelIcon'=>"menu",
				'map'=>[
					'app\models\Categories'=>['id','name','foto','subtitle','sort','type','siblings','intro']
				],
				'params'=>[
					'types'=>Categories::getTypes(),
				],
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('categories','msg_saved'),
				'notsavedMsg'=>Yii::t('categories','msg_notsaved'),
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
			]
		];
	}

//end class
}
