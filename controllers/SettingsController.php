<?php
namespace app\controllers;

use Yii;
use \app\components\Settings;

class SettingsController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actionIndex()
	{
		$params=[
			'panelTitle'=>"Ustawienia serwisu",
			'panelIcon'=>"dashboard",
			'vars'=>Settings::read(),
		];
		return $this->render('index.twig',$params);
	}

	public function actionSave()
	{
		$out=['saved'=>false];
		$r=(array)$this->getJson();
		if (isset($r['data'])) {
			foreach($r['data'] as $v) {
				if (isset($v->table) && isset($v->name) && isset($v->value)
					 && isset(Yii::$app->params[$v->table][$v->name]) && Yii::$app->params[$v->table][$v->name]!=$v->value) {
					Settings::save($v->name,$v->value,$v->table);
				} elseif (isset($v->name) && isset($v->value)
					 && isset(Yii::$app->params[$v->name]) && Yii::$app->params[$v->name]!=$v->value) {
					Settings::save($v->name,$v->value);
				}
			}
			$out['saved']=true;
			$out['msg']="Ustawienia zaktualizowane";
		} else {
			$out['msg']=Yii::t('site','err_bad_data');
		}

		//public static function{

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}


//end class
}
