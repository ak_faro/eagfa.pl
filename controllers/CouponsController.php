<?php
namespace app\controllers;

use Yii;
use app\models\Coupons;

class CouponsController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('coupons','panel_title_list'),
				'panelIcon'=>"local_play",
				'map'=>[
					'app\models\Coupons'=>['id','created','amount','status','statusLabel','params','user'],
					'app\models\Users'=>['id','name'],
				],
				'order'=>Coupons::tableName().'.id desc'
			]
		];
	}


//end class
}
