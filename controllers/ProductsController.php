<?php
namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Products;
use app\models\Categories;

class ProductsController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		$map=[
			'app\models\Products'=>['id','name','price','status','statusLabel','params','foto','images','gallery','amount','group','category','intro','export','sort'],
			'app\models\Categories'=>['id','name']
		];
		$params=[
			'newOption'=>Products::getOption(),
			'newParam'=>Products::getSchema(),
			'newImage'=>Products::getImageSchema(),
			'newGallery'=>Products::getGallerySchema(),
			'newCond'=>Products::getImageCond(),
			'paramTypes'=>Products::getParamTypes(),
			'paramSizes'=>Products::getParamSizes(),
			'categories'=>Categories::getList(0),
		];
		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('products','panel_title_list'),
				'panelIcon'=>"shopping_cart",
				'map'=>[
					'app\models\Products'=>['id','name','status','statusLabel','group','category'],
					'app\models\Categories'=>['id','name']
				],
				'getParams'=>[
					'group'=>[[],'products.group=:group'],
				],
				'params'=>[
					'groups'=>ArrayHelper::map(Categories::getList(0),'id','name'),
					'listTitle'=>Yii::t('products','list_all')
				],
			],
			'create'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('products','panel_title_new'),
				'panelIcon'=>"shopping_cart",
				'map'=>[
					'app\models\Products'=>['name','price','status','params','foto','images','gallery','amount','group','intro','export','sort']
				],
				'params'=>$params
			],
			'edit'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('products','panel_title_edit'),
				'panelIcon'=>"shopping_cart",
				'map'=>$map,
				'params'=>$params
			],
			'copy'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('products','panel_title_copy'),
				'panelIcon'=>"shopping_cart",
				'map'=>$map,
				'params'=>$params
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('products','msg_saved'),
				'notsavedMsg'=>Yii::t('products','msg_notsaved'),
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
			]
		];
	}




//end class
}