<?php
namespace app\controllers;

use Yii;
use app\models\Menus;

class MenusController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		
		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('menus','panel_title_list'),
				'panelIcon'=>"menu",
				'map'=>[
					'app\models\Menus'=>['id','name']
				],
				'params'=>[
					'listTitle'=>Yii::t('menus',(Yii::$app->request->get('id',false)?'list_items':'list_main')),
				]
			],
			'create'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('menus','panel_title_new'),
				'panelIcon'=>"menu",
				'map'=>[
					'app\models\Menus'=>['name','link','style','class','ancestor']
				]
			],
			'edit'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('menus','panel_title_edit'),
				'panelIcon'=>"menu",
				'map'=>[
					'app\models\Menus'=>['id','name','link','linkItems','style','class','ancestor','order','siblings']
				]
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('menus','msg_saved'),
				'notsavedMsg'=>Yii::t('menus','msg_notsaved'),
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
			]
		];
	}

	public function actionOrdering()
	{
		$out=[];
		$request=(array)$this->getJson();
		if (isset($request['parent'])) {
			$p=Menus::findOne($request['parent']);
			$out['items']=($p!==null?$p->getSiblings($p):[]);
		} else {
			$out['error']=Yii::t('site','err_bad_post');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;

	}

	/*
	public function actionDelete(){
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out=[];
		$request=(array)$this->getJson();
		if (isset ($request['id'])) {
			$model=Menus::findOne($request['id']);
			if ($model) {
				$model->delete();
			}
		}
		return $out;
	}*/

//end class
}
