<?php
namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\Products;
use app\models\Categories;
use app\models\Articles;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

class PanelController extends \app\components\BaseController
{

	public $pageSize=15;

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['customer'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'savepass'=>[
				'class'=>"\app\components\PassAction",
			],
			'invite'=>[
				'class'=>"\app\components\InviteAction",
			],
		];
	}

	public function actionIndex($json=false)
	{
		$query=Yii::$app->user->identity->getOrders()->orderBy('id desc');
		$count=$query->count();
		$pagination = new Pagination(['totalCount' => $count,'pageSize'=>$this->pageSize]);
		$models = $query->offset($pagination->offset)->limit($pagination->limit)->all();

		$params=[
			'panelTitle'=>"Panel użytkownika",
			'panelIcon'=>"dashboard",
			'orders'=>ArrayHelper::toArray(
				$models,
				['app\models\Orders'=>['id','status','statusLabel','statusData','brutto','created','items','upload'=>function($m){
					return $m->canUpload();
				}]]
			),
			'user'=>ArrayHelper::toArray(
				Yii::$app->user->identity,
				['app\models\Users'=>['id','firstname','lastname','phone','params']]
			),
			'pagination'=>$pagination,
			'pager'=>[
				'page'=>$pagination->page+1,
				'pageSize'=>$pagination->pageSize,
				'pageCount'=>$pagination->pageCount,
				'totalCount'=>$pagination->totalCount,
			],
			'categories'=>Categories::getCategories(0),
			'coupons'=>ArrayHelper::toArray(Yii::$app->user->identity->coupons,
				['app\models\Coupons'=>['id','created','amount','status','statusLabel','reason']]
			),
			'followers'=>ArrayHelper::toArray(
				Yii::$app->user->identity->followers,
				['app\models\Users'=>['name','created','isRefunded',
											 'ordersSum'=>function($item){ return $item->isRefunded?0:$item->ordersSum; },
											 'ordersCount'=>function($item){ return $item->isRefunded?0:$item->ordersCount; },
											 'lastDate'=>function($item){ return $item->isRefunded?0:$item->lastDate; },
											 'lastDays'=>function($item){ return $item->isRefunded?0:$item->lastDays; }
											]]
			),
			'message'=>Articles::findOne(Yii::$app->params['schema']['invite']),
		];

		if ($json) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return [
				'pagination'=>$params['pager'],
				'items'=>$models,
			];
		} else {
			return $this->render('index.twig',$params);
		}
	}

	public function actionOrder($id=null)
	{
		$model=Orders::findOne($id);
		if ($model&&$model->user_id==Yii::$app->user->id) {
			$params=[
				'panelTitle'=>Yii::t('orders','panel_title_show'),
				'panelIcon'=>"shopping_basket",
				'model'=>$model,
				'files'=>Yii::$app->request->get('files',false),
				'newOrder'=>Yii::$app->session->hasFlash('orderCreated')?Yii::$app->session->getFlash('orderCreated'):false,
			];
			return $this->render('order.twig',$params);
		} else {
			throw new NotFoundHttpException(Yii::t('orders','err_empty_model'));
		}
	}

	public function actionUpload()
	{
		$out=[];
		$id=Yii::$app->request->post('id',null);

		$model=Orders::findOne($id);
		if ($model && $model->user_id==Yii::$app->user->id) {
			if ($model->canUpload()) {
				$file=UploadedFile::getInstanceByName('file');
				if ($file!==null) {
					$dir=$model->updir;
					if (!is_dir($dir)) mkdir($dir,0700,true);
					if($file->saveAs($dir."/".$file->baseName.'.'.$file->extension)) $out['count']=1;
						else $out['count']=0;
					$out['files']=$model->files;
				} else {
					$out['error']=Yii::t('files','err_no_file');
				}
			} else {
				$out['error']=Yii::t('orders','err_no_upload');
			}
		} else {
			$out['error']=Yii::t('orders','err_empty_model');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionFiledel()
	{
		$out=[];
		$request=$this->getJson();
		if (isset($request->id)&&isset($request->name)) {
			$model=Orders::findOne($request->id);
			if ($model) {
				$dir=$model->updir."/";
				$p=explode("/",$request->name);
				$p=array_pop($p);
				if (is_dir($dir.$p)) {
					$out['delete']=true;
					if (PHP_OS == 'WINNT') { exec("rd /s /q ".str_replace('/','\\',$dir.$p)); } else { exec("rm -rf ".$dir.$p); }
				} elseif (is_file($dir.$p)){
					if (unlink($dir.$p)) $out['delete']=true;
				} else {
					$out['error']="Brak takiego elementu";
				}
				$out['files']=$model->files;
			} else {
				$out['error']=Yii::t('orders','err_empty_model');
			}
		} else {
			$out['error']=Yii::t('site','err_bad_data');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionSave()
	{
		$out=['saved'=>false];
		$request=$this->getJson();
		$model=Yii::$app->user->identity;
		$params=is_object($model->params)?$model->params:new \stdClass();

		if (isset($request->firstname)) {
			$model->firstname=$request->firstname;
			$out['saved']=true;
		}
		if (isset($request->lastname)) {
			$model->lastname=$request->lastname;
			$out['saved']=true;
		}
		if (isset($request->phone)) {
			$model->phone=$request->phone;
			$out['saved']=true;
		}
		if (isset($request->params->shipping)) {
			$params->shipping=$request->params->shipping;
			$out['saved']=true;
		}
		if (isset($request->params->invoice)) {
			$params->invoice=$request->params->invoice;
			$out['saved']=true;
		}

		if ($out['saved']==true) {
			$model->params=$params;
			$model->scenario="user";
			$out['saved']=$model->save();
			if (sizeof($model->errors)) $out['errors']=$model->errors;
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionUpdateorder()
	{
		$out=['saved'=>false];
		$r=$this->getJson();
		if (isset($r->id) && isset($r->mode)) {
			$model=Orders::findOne($r->id);
			if ($model && $model->user_id==Yii::$app->user->id) {
				switch ($r->mode) {
					case 'get':
						if (isset($r->hash)&&isset($r->provider)) {
							$data=$this->getImported($r->provider,$r->hash);
							if ($data) $r->project=$data;
						} else {
							break;
						}
					case 'add':
						if (isset($r->project)&&isset($r->project->hash)&&isset($r->pos)&&$r->pos>=0&&$r->pos<=sizeof($model->items)) {
							$param=$model->params;
							if (isset($param->projects) && is_array($param->projects) && sizeof($param->projects)>0) {
								if (isset($param->projects[$r->pos])) {
									$i=array_search($r->project->hash,ArrayHelper::getColumn($param->projects[$r->pos],'hash'));
									if ($i===false) $param->projects[$r->pos][]=$r->project;
								} else {
									$param->projects[$r->pos][]=$r->project;
								}
							} else {
								$param->projects=[];
								$param->projects[$r->pos]=[$r->project];
							}
							$model->params=$param;
							$out['saved']=$model->save();
							if ($r->mode=="get") $out['project']=$r->project;
						}
						break;
					case 'del':
						if (isset($r->hash)&&isset($r->pos)&&$r->pos>=0&&$r->pos<=sizeof($model->items)) {
							$param=$model->params;
							if (isset($param->projects) && is_array($param->projects) && sizeof($param->projects)>0) {
								if (isset($param->projects[$r->pos])) {
									$i=array_search($r->hash,ArrayHelper::getColumn($param->projects[$r->pos],'hash'));
									if ($i!==false) {
										unset($param->projects[$r->pos][$i]);
										if (sizeof($param->projects[$r->pos])==0) unset($param->projects[$r->pos]);
										$model->params=$param;
										$out['saved']=$model->save();
									}
								}
							}
						}
						break;
				}
			}
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionUpdateftp()
	{
		$user=Yii::$app->user->identity;
		$ftp=[
			'user'=>$user->ftpname,
		];

		$r=$this->getJson();
		if (isset($r->password)) {
			$ftp['pass']=$r->password;
		} else {
			$ftp['pass']=substr(str_shuffle("abcdefghijklmnprstuwxyzABCDEFGHKLMNPRSTUWXYZ123456789"),0,5);
		}

		$link="http://".Yii::$app->params['ftphost']."/ws.php?k=VhR9tXPufT2f8SdK&a=add&u=".$ftp['user']."&p=".$ftp['pass'];
		$data=@file_get_contents($link);
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if ($data=="OK") {
			$params=$user->params;
			if (!is_object($params)) $params=new \stdClass();
			$params->ftp=$ftp;
			$user->params=$params;
			$user->save(false);
			return ['ftp'=>$ftp];
		} else {
			return null;
		}
	}

//end class
}
