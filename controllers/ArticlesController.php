<?php
namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Articles;
use app\models\Categories;
use yii\web\NotFoundHttpException;

class ArticlesController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['show','category'],
						'allow' => true,
						'roles' => ['?','@'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		$params=[
			'categories'=>Categories::getList(1),
		];

		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('articles','panel_title_list'),
				'map'=>[
					'app\models\Articles'=>['id','title','created','category','removable'=>function($item){ return (bool)$item->removable; }],
					'app\models\Categories'=>['id','name'],
				],
				'params'=>[
					'listTitle'=>Yii::t('articles','list_all'),
					'groups'=>ArrayHelper::map(Categories::getList(1),'id','name'),
				],
				'getParams'=>[
					'group'=>[[],'articles.group=:group'],
					'string'=>[[],"articles.title like :string"],
				],
			],
			'create'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('articles','panel_title_new'),
				'panelIcon'=>"mode_edit",
				'map'=>[
					'app\models\Articles'=>['title','intro','full','style','image','pagebreak'],
				],
				'params'=>$params
			],
			'edit'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('articles','panel_title_edit'),
				'panelIcon'=>"mode_edit",
				'map'=>[
					'app\models\Articles'=>['id','title','intro','full','style','image','group','pagebreak'],
					'app\models\Categories'=>['id','name'],
				],
				'params'=>$params
			],
			'copy'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('articles','panel_title_copy'),
				'panelIcon'=>"mode_edit",
				'map'=>[
					'app\models\Articles'=>['id','title','intro','full','style','image','group','pagebreak'],
					'app\models\Categories'=>['id','name'],
				],
				'params'=>$params
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('articles','msg_saved'),
				'notsavedMsg'=>Yii::t('articles','msg_notsaved'),
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
				'readonly'=>'removable',
			]
		];
	}

	public function actionShow($id=null)
	{
		$params=[
			'model'=>$this->loadModel($id),
			'part'=>Yii::$app->request->get('part',null),
		];
		return $this->render('show.twig',$params);
	}

	public function actionCategory($id=null)
	{
		$model=Categories::findOne($id);
		if (!$model) throw new NotFoundHttpException(Yii::t('site','err_empty_model'));
		$params=[
			'model'=>$model
		];
		return $this->render('category.twig',$params);
	}

	public function actionPopup() {
		$params=[
			'layout'=>'simple',
			//'assets'=>['angular'=>['angularFileUpload']],
		];
		return $this->render('manager.twig',$params);
	}

//end class
}
