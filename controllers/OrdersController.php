<?php
namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\Users;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class OrdersController extends \app\components\BaseController
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['show'],
						'allow' => true,
						'roles' => ['merchant'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('orders','panel_title_list'),
				'map'=>[
					'app\models\Orders'=>['id','status','statusLabel','statusData','brutto','user','user_name','user_group','created'],
					'app\models\Users'=>['id','name','email','vip']
				],
				'params'=>[
					'listTitle'=>Yii::t('orders',(Yii::$app->request->get('status',false)!==false?'list_status':'list_all')),
					'group'=>Users::findOne(Yii::$app->request->get('group',null)),
					'statuses'=>Orders::getStatuses(),
				],
				//'join'=>[
				//	['left outer join',Users::tableName(),Orders::tableName().'.user_id='.Users::tableName().'.id'],
				//],
				'getParams'=>[
					'status'=>[[],'orders.status=:status'], //table,where_params
					'start'=>[[],"orders.created>=:start"],
					'end'=>[[],"orders.created<=:end"],
					//'group'=>[[],'users.group=:group'],
					'group'=>[[],'orders.user_group=:group'],
					'id'=>[[],'orders.id=:id'],
				],
				'summary'=>['brutto'],
				'sort'=>['created'],
				'order'=>[Orders::tableName().'.id'=>SORT_DESC],
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
			]
		];
	}

	public function actionShow($id=null)
	{
		$model=$this->loadModel($id);
		//if (Yii::$app->user->can('administrator')||$model->user->group==Yii::$app->user->id) {
		if (Yii::$app->user->can('administrator')||$model->user_group==Yii::$app->user->id) {
			$params=[
				'panelTitle'=>Yii::t('orders','panel_title_show'),
				'model'=>$model,
				'statuses'=>Orders::getStatuses(),
			];
			return $this->render('show.twig',$params);
		} else {
			throw new ForbiddenHttpException(Yii::t('orders','err_not_permit'));
		}
	}

	public function actionStatus()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$out=['saved'=>false];
		$request=(array)$this->getJson();
		if (isset ($request['id']) && isset($request['status'])) {
			$model=Orders::findOne($request['id']);
			if ($model) {
				$model->status=$request['status'];
				$out['saved']=$model->save();
				if ($out['saved'] && in_array($model->status,[-1,1,2,3])) {
					$model->deleteFiles();
					$out['delete']=true;
				}
				if ($out['saved'] && isset($request['mode']) && $request['mode']) $model->statusInform();
			}
		}
		return $out;
	}

	public function actionGet($id="",$name="")
	{
		//xSendFile
		$model=Orders::findOne($id);
		if ($model && is_file($model->updir.'/'.$name)) {
			return \Yii::$app->response->sendFile($model->updir.'/'.$name);
		} else {
			throw new NotFoundHttpException("Nie ma takiego pliku");
		}
	}

	public function actionMsg($id) {
		$model=Orders::findOne($id);
		if ($model) var_dump($model->email("admin",true));
	}

	public function actionPdf($id) {
		$model=Orders::findOne($id);
		if ($model){
			$pdf = Yii::$app->pdf;
			$article=\app\models\Articles::findOne(36);

			$pdf->content=str_replace(array_keys($model->tags),array_values($model->tags),$article->full);

			$pdf->destination=$pdf::DEST_DOWNLOAD;//DEST_STRING
			$pdf->filename="zamówienie_".$model->id.".pdf";
			Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
			return $pdf->render();
		}
	}

	public function actionDelfiles($id=null) {
		$out=['delete'=>false];
		$model=Orders::findOne($id);
		if ($model) {
			$model->deleteFiles();
			$out['delete']=true;
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}


//end class
}
