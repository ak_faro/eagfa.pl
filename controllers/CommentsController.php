<?php
namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Comments;

class CommentsController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['list','add','all'],
						'allow' => true,
						'roles' => ['?','@'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('comments','panel_title_list'),
				'panelIcon'=>"comment",
				'map'=>[
					'app\models\Comments'=>['id','text','author'=>'user','created','status','statusLabel']
				],
				'params'=>[
					'listTitle'=>Yii::t('comments','list_items'),
				],
				'order'=>'id desc',
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
			],
			'edit'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('comments','panel_title_edit'),
				'panelIcon'=>"mode_edit",
				'map'=>[
					'app\models\Comments'=>['id','text','user'],
				],
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('comments','msg_saved'),
				'notsavedMsg'=>Yii::t('comments','msg_notsaved'),
			],
		];
	}

	public function actionList()
	{
		$limit=5;
		$out=[
			'items'=>ArrayHelper::toArray(Comments::find()->where(['status'=>1])->limit($limit)->all(),['app\models\Comments'=>['text','sig'=>'user']])
		];
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionAll()
	{
		$params=[
			'models'=>Comments::find()->where(['status'=>1])->orderBy('id desc')->all(),
			'panelTitle'=>Yii::t('comments','panel_title_all'),
		];
		return $this->render('all.twig',$params);
	}

	public function actionAdd()
	{
		$out=['saved'=>false];
		$request=$this->getJson();
		if (isset($request->text)&&strlen($request->text)>2) {
			if (isset($request->user)&&strlen($request->user)>2) {
				$model=new Comments();
				$model->text=trim(strip_tags($request->text));
				$model->user=trim(strip_tags($request->user));
				$model->user_id=Yii::$app->user->isGuest?0:Yii::$app->user->id;
				$out['saved']=$model->save();
			} else {
				$out['msg']=Yii::t('comments','err_no_user');
			}
		} else {
			$out['msg']=Yii::t('comments','err_no_text');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionPublish()
	{
		$out=['status'=>false];
		$r=$this->getJson();
		if (isset($r->id)) {
			$model=Comments::findOne($r->id);
			if ($model) {
				$model->status=1;
				$out['status']=$model->save();
			}
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

//end class
}
