<?php
namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Contacts;
use app\models\Events;
use app\models\Articles;
use app\components\Freshmail;

use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

class ContactsController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['freshmail'],
						'allow' => false,
						'roles' => ['merchant'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator','merchant'],
					],
				],
			],
		];
	}

	public function actions()
	{
		$all=Yii::$app->user->can('administrator')&&(Yii::$app->request->get('all',false)||Yii::$app->request->get('owner',0));
		$where=[];
		if (!$all) $where[]='owner_id='.Yii::$app->user->id;
		if (!isset($_GET['json'])) $where[]='status=1';
		if (sizeof($where)>0) array_unshift($where,"and");

		$getParams=[
			'status'=>[[],"status=:status"],
			'string'=>[[],"email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)"],
		];
		$params=[
			'message'=>Articles::findOne(Yii::$app->params['schema']['invite']),
			'statuses'=>Contacts::getStatuses(),
		];
		if (Yii::$app->user->can('administrator')) {
			$getParams['owner']=[[],'owner_id=:owner'];
			$params['owner']=Users::findOne(Yii::$app->request->get('owner',null));
		}

		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('contacts','panel_title_list'),
				'panelIcon'=>"contacts",
				'map'=>[
					'app\models\Contacts'=>['id','firstname','lastname','created','phone','email','owner','notes'],
					'app\models\Users'=>['id','name']
				],
				'sort'=>['lastname','created'],
				'getParams'=>$getParams,
				'params'=>$params,
				'where'=>$where,
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
				'rule'=>"manageContact",
				'field'=>"owner_id"
			],
			'create'=>[
				'class'=>"\app\components\UpdateAction",
				'panelTitle'=>Yii::t('contacts','panel_title_new'),
				'panelIcon'=>"mode_edit",
				'view'=>"create.twig",
				'map'=>[
					'app\models\Contacts'=>['firstname','lastname','phone','email','status'],
				],
				'params'=>[
					'statuses'=>Contacts::getStatuses(),
				]
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('contacts','msg_saved'),
				'notsavedMsg'=>Yii::t('contacts','msg_notsaved'),
				'onlyInsert'=>true,
			],
			'message'=>[
				'class'=>"\app\components\MessageAction",
				'model'=>"Contacts"
			],
			'invite'=>[
				'class'=>"\app\components\InviteAction",
			],
		];
	}

	public function actionShow($id=null)
	{
		$model=$this->loadModel($id);
		if (Yii::$app->user->can('administrator')||Yii::$app->user->can('manageContact',$model->owner_id)) {
			$params=[
				'back'=>Yii::$app->session->getFlash('back'),
				'panelTitle'=>Yii::t('contacts','panel_title_show'),
				'model'=>$model,
				'target'=>[
					'type'=>Events::$targetTypeContact,
					'id'=>$model->id,
					'label'=>Events::contactTargetLabel($model->name)
				],
				'events'=>Events::forContact($model->id),
				'user'=>ArrayHelper::toArray($model,[
					'app\models\Contacts'=>['id','firstname','lastname','phone','email','params','notes','owner','created','status'=>function($item){ return (string)$item->status;},'statusLabel'],
					'app\models\Users'=>['id','name']
				]),
			];
			return $this->render('show.twig',$params);
		} else {
			throw new ForbiddenHttpException(Yii::t('contacts','err_not_permit'));
		}
	}

	public function actionSave2()
	{
		$out=['saved'=>false];
		$request=(array)$this->getJson();
		if (isset($request['id']) && isset($request['mode']) && in_array($request['mode'],['data','notes'])) {
			$model=Contacts::findOne($request['id']);
			if ($model) {
				if (Yii::$app->user->can('administrator')||Yii::$app->user->can('manageContact',$model->owner_id)) {
					$params=is_object($model->params)?$model->params:new \stdClass();
					//$notes=is_object($model->notes)?$model->notes:new \stdClass();
					switch($request['mode']) {
						case 'data':
							if (isset($request['user']->firstname)) { 
								$model->firstname=$request['user']->firstname;
								$out['saved']=true;
							}
							if (isset($request['user']->lastname)) {
								$model->lastname=$request['user']->lastname;
								$out['saved']=true;
							}
							if (isset($request['user']->phone)) {
								$model->phone=$request['user']->phone;
								$out['saved']=true;
							}
							if (isset($request['user']->email)) {
								$model->email=$request['user']->email;
								$out['saved']=true;
							}
							if (isset($request['user']->status)) {
								$model->status=$request['user']->status;
								$out['saved']=true;
							}
							break;
						case 'notes':
							if (isset($request['user']->notes)) {
								$notes=$request['user']->notes;
								$out['saved']=true;
							}
							break;
					}
					if ($out['saved']==true) {
						$model->params=$params;
						if ($request['mode']=="notes") $model->notes=$notes;
						$out['saved']=$model->save();
						$out['errors']=$model->errors;
					}
				} else {
					$out['msg']=Yii::t('contacts',"err_not_permit");
				}
			} else {
				$out['msg']=Yii::t('contacts',"err_no_model");
			}
		} else {
			$out['msg']=Yii::t('site',"err_bad_data");
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionMailing()
	{
		$out=['err'=>true];
		$q=$this->getJson();

		if (is_object($q)&&isset($q->subject)&&isset($q->text)) {
			if (strlen($q->subject)>0&&strlen($q->text)>0) {
				$users=Contacts::find()->select("email");

				if (isset($q->f)) {
					$r=(array)$q->f;
					if (Yii::$app->user->can('merchant')||(Yii::$app->user->can('administrator') && (!isset($r['all']) || $r['all']==false))) {
						$users->andWhere(["=",'owner_id',Yii::$app->user->id]);
					}
					if (isset($r['string']) && strlen($r['string'])>0) $users->andWhere("email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)",[':string'=>"%".$r['string']."%"]);
				}
				$rows=$users->column();
				if (sizeof($rows)>0) {
					$out['find']=sizeof($rows);
					$out['count']=0;
					$out['hash']=uniqid();
					foreach($rows as $row){
						if (strlen($row)>=5) {
							$insert=Yii::$app->db->createCommand()->insert('mailing',[
								'recipient'=>$row,
								'subject'=>$q->subject,
								'body'=>$q->text,
								'creator'=>Yii::$app->user->id,
								'hash'=>$out['hash'],
							])->execute();
							if ($insert) $out['count']++;
						}
					};
					$out['msg']="Przetworzono ".$out['find']." użytkowników - skolejkowano ".$out['count']." wiadomości";
					$out['err']=false;
				} else {
					$out['msg']=Yii::t('users','msg_no_rows');
				}
			} else {
				$out['msg']=Yii::t('site','err_form_empty');
			}
		} else {
			$out['msg']=Yii::t('site','err_form_fill');
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionFreshmail()
	{
		$out=['err'=>true];
		$q=$this->getJson();
		if (is_object($q)&&isset($q->name) && strlen($q->name)>=5) {
				$users=Contacts::find()->select("email");

				if (isset($q->f)) {
					$r=(array)$q->f;
					if (Yii::$app->user->can('merchant')||(Yii::$app->user->can('administrator') && (!isset($r['all']) || $r['all']==false))) {
						$users->andWhere(["=",'owner_id',Yii::$app->user->id]);
					}
					if (isset($r['string']) && strlen($r['string'])>0) $users->andWhere("email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)",[':string'=>"%".$r['string']."%"]);
				}
				$rows=$users->column();
				if (sizeof($rows)>0) {
					$out['find']=sizeof($rows);
					$export=[];
					foreach($rows as $row){
						if (strlen($row)>0) $export[]=['email'=>$row];
					};
					$out['result']=Freshmail::uploadList([
						'list'=>$q->name,
						'desc'=>isset($q->desc)?$q->desc:"",
						'subscribers'=>$export
					]);
					$out['msg']="Przetworzono ".$out['find']." użytkowników";
					$out['err']=false;
				} else {
					$out['msg']=Yii::t('users','msg_no_rows');
				}
		} else {
			$out['msg']=Yii::t('users','msg_no_listhash');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

//end class
}
