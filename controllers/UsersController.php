<?php
namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Categories;
use app\models\Events;
use app\components\Freshmail;

use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

class UsersController extends \app\components\BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['show','save','mailing','message'],
						'allow' => true,
						'roles' => ['merchant'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'index'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('users','panel_title_list'),
				'panelIcon'=>"account_box",
				'map'=>[
					'app\models\Users'=>[
						'id','name','email','status','statusLabel','vip','customersCount','ordersCount','ordersSum','lastDate','lastDays', 'contactsCount',
						'removable'=>function($item){ return (bool)$item->removable; },
						'admin'=>function($item){ return (int)$item->can('administrator'); },
						'created'=>function($item) { return $item::displayDate($item->created); },
					],
				],
				'params'=>[
					'listTitle'=>Yii::t('users','list_all'),
					'paramTitle'=>Yii::t('users','list_status_z'),
					'group'=>Users::findOne(Yii::$app->request->get('group',null)),
					'paramCol'=>Yii::t('users','merchant_customers'),
					'statuses'=>Users::getStatuses(),
					'priorities'=>[],
					'hideVip'=>true,
				],
				'join'=>[
					['inner join','auth_assignment','auth_assignment.user_id='.Users::tableName().'.id'],
					['left outer join','orders_stat2','orders_stat2.user_id='.Users::tableName().'.id']
				],
				'where'=>[
					'!=','auth_assignment.item_name','customer'
				],
				'sort'=>['ilosc','brutto','days'],
				'getParams'=>[
					'status'=>[[],'users.status=:status'],
					'string'=>[[],"email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)"],
				],
			],
			'clients'=>[
				'class'=>"\app\components\IndexAction",
				'panelTitle'=>Yii::t('users','panel_title2_list'),
				'panelIcon'=>"account_box",
				'map'=>[
					'app\models\Users'=>[
						'id','firstname','lastname','name','email','status','statusLabel','merchant','vip','ordersCount','ordersSum','lastDate','lastDays',
						'removable'=>function($item){ return (bool)$item->removable; },
						'admin'=>function($item){ return 0; },
						'created'=>function($item) { return $item::displayDate($item->created); },
					],
				],
				'params'=>[
					'listTitle'=>Yii::t('users',(Yii::$app->request->get('group',false)!==false?'list_group':'list2_all')),
					'paramTitle'=>Yii::t('users','list_status2_z'),
					'paramCol'=>Yii::t('users','model_group'),
					'group'=>Users::findOne(Yii::$app->request->get('group',null)),
					'statuses'=>Users::getStatuses(),
					'priorities'=>Users::getPriorities(),
				],
				'view'=>"index.twig",
				'join'=>[
					['inner join','auth_assignment','auth_assignment.user_id='.Users::tableName().'.id'],
					['left outer join','orders_stat','orders_stat.user_id='.Users::tableName().'.id']
				],
				'where'=>[
					'=','auth_assignment.item_name','customer'
				],
				'sort'=>['lastname','ilosc','brutto','days'],
				'getParams'=>[
					'status'=>[[],'users.status=:status'],
					'string'=>[[],"email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)"],
					'group'=>[[],'users.group=:group'],
					'vip'=>[[],'users.vip=:vip'],
				],
				'beforeFind'=>"clientsPrepare"
			],
			'delete'=>[
				'class'=>'\app\components\DeleteAction',
				'readonly'=>'removable',
			],
			'message'=>[
				'class'=>"\app\components\MessageAction",
				'model'=>"Users"
			],
		];
	}

	public function clientsPrepare(&$c)
	{
		$buyer=Yii::$app->request->get('buyer',null);
		if ($buyer==="0") {
			$c->getParams['buyer']=[[],'orders_stat.ilosc is null',true];
		}
		if ($buyer==1) {
			$c->getParams['buyer']=[[],'orders_stat.ilosc>0',true];
		}
		if ($buyer==2) {
			//$c->getParams['buyer']=[[],'orders_stat.brutto>0 and orders_stat.days>90 and orders_stat.days<=120',true];
			$c->getParams['buyer']=[[],'orders_stat.brutto>0 and orders_stat.days>90',true];
		}
	}

	public function actionFreshmail()
	{
		$out=['err'=>true];
		$q=$this->getJson();
		if (is_object($q)&&isset($q->name) && strlen($q->name)>=5) {
				$users=Users::find()->select("email")
					->join('inner join','auth_assignment','auth_assignment.user_id='.Users::tableName().'.id')
					->where(['=','auth_assignment.item_name','customer']);

				if (!Yii::$app->user->can("administrator")) $users->andWhere(["=",'group',Yii::$app->user->id]);

				if (isset($q->f)) {
					$r=(array)$q->f;
					if (isset($r['status'])) $users->andWhere('users.status=:status',[':status'=>$r['status']]);
					if (isset($r['group'])) $users->andWhere('users.group=:group',[':group'=>$r['group']]);
					if (isset($r['vip'])) $users->andWhere('users.vip=:vip',[':vip'=>$r['vip']]);
					if (isset($r['string']) && strlen($r['string'])>0) $users->andWhere("email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)",[':string'=>"%".$r['string']."%"]);
				}
				$rows=$users->column();
				if (sizeof($rows)>0) {
					$out['find']=sizeof($rows);
					$export=[];
					foreach($rows as $row){
						$export[]=['email'=>$row];
					};
					$out['result']=Freshmail::uploadList([
						'list'=>$q->name,
						'desc'=>isset($q->desc)?$q->desc:"",
						'subscribers'=>$export
					]);
					$out['msg']="Przetworzono ".$out['find']." użytkowników";
					$out['err']=false;
				} else {
					$out['msg']=Yii::t('users','msg_no_rows');
				}
		} else {
			$out['msg']=Yii::t('users','msg_no_listhash');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionMailing()
	{
		$out=['err'=>true];
		$q=$this->getJson();
		//czy jest temat i tresc
		if (is_object($q)&&isset($q->subject)&&isset($q->text)) {
			if (strlen($q->subject)>0&&strlen($q->text)>0) {
				$users=Users::find()->select("email")
					->join('inner join','auth_assignment','auth_assignment.user_id='.Users::tableName().'.id')
					->where(['=','auth_assignment.item_name','customer']);

				if (!Yii::$app->user->can("administrator")) $users->andWhere(["=",'group',Yii::$app->user->id]);

				if (isset($q->f)) {
					$r=(array)$q->f;
					if (isset($r['status'])) $users->andWhere('users.status=:status',[':status'=>$r['status']]);
					if (isset($r['group'])) $users->andWhere('users.group=:group',[':group'=>$r['group']]);
					if (isset($r['vip'])) $users->andWhere('users.vip=:vip',[':vip'=>$r['vip']]);
					if (isset($r['string']) && strlen($r['string'])>0) $users->andWhere("email like :string or phone like :string or (concat(firstname,' ',lastname) like :string or concat(lastname,' ',firstname) like :string or lastname like :string or firstname like :string)",[':string'=>"%".$r['string']."%"]);
				}
				$rows=$users->column();
				if (sizeof($rows)>0) {
					$out['find']=sizeof($rows);
					$out['count']=0;
					$out['hash']=uniqid();
					foreach($rows as $row){
						$insert=Yii::$app->db->createCommand()->insert('mailing',[
							'recipient'=>$row,
							'subject'=>$q->subject,
							'body'=>$q->text,
							'creator'=>Yii::$app->user->id,
							'hash'=>$out['hash'],
						])->execute();
						if ($insert) $out['count']++;
					};
					$out['msg']="Przetworzono ".$out['find']." użytkowników - skolejkowano ".$out['count']." wiadomości";
					$out['err']=false;
				} else {
					$out['msg']=Yii::t('users','msg_no_rows');
				}
			} else {
				$out['msg']=Yii::t('site','err_form_empty');
			}
		} else {
			$out['msg']=Yii::t('site','err_form_fill');
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	public function actionShow($id=null)
	{
		$model=$this->loadModel($id);
		if (Yii::$app->user->can('administrator')||Yii::$app->user->can('manageCustomer',$model->group)) {
			$params=[
				'panelTitle'=>Yii::t('users',$model->can('customer')?'panel_title2_show':'panel_title_show'),
				'model'=>$model,
				'target'=>[
					'type'=>Events::$targetTypeUser,
					'id'=>$model->id,
					'label'=>Events::userTargetLabel($model->name)
				],
				'events'=>Events::forUser($model->id),
				'user'=>ArrayHelper::toArray($model,[
					'app\models\Users'=>['id','email','firstname','lastname','phone','status'=>function($item){ return (string)$item->status;},'role','params','merchant','vip'=>function($item){ return (string)$item->vip;},'group','notes']
				]),
				'roles'=>$this->getRoles(),
				'categories'=>Categories::getCategories(0),
				'merchants'=>Users::getMerchants(),
			];
			return $this->render('show.twig',$params);
		} else {
			throw new ForbiddenHttpException(Yii::t('users','err_not_permit'));
		}
	}

	public function actionSave()
	{
		$out=['saved'=>false];
		$request=(array)$this->getJson();
		if (isset($request['id']) && isset($request['mode']) && in_array($request['mode'],['data','discount','invoice','shipping','notes'])) {
			$model=Users::findOne($request['id']);
			if ($model) {
				if (Yii::$app->user->can('administrator')||Yii::$app->user->can('manageCustomer',$model->group)) {
					$params=is_object($model->params)?$model->params:new \stdClass();
					switch($request['mode']) {
						case 'data':
							if (isset($request['user']->firstname)) { 
								$model->firstname=$request['user']->firstname;
								$out['saved']=true;
							}
							if (isset($request['user']->lastname)) {
								$model->lastname=$request['user']->lastname;
								$out['saved']=true;
							}
							if (isset($request['user']->phone)) {
								$model->phone=$request['user']->phone;
								$out['saved']=true;
							}
							if (isset($request['user']->status)) {
								$model->status=$request['user']->status;
								$out['saved']=true;
							}
							if (isset($request['user']->vip)) {
								$model->vip=$request['user']->vip;
								$out['saved']=true;
							}
							if (isset($request['user']->role) && !$model->can($request['user']->role)) {
								$auth = Yii::$app->authManager;
								$uroles=$auth->getRolesByUser($model->getId());
								$auth->revokeAll($model->getId());
								//$auth->revoke($auth->getRole($model->role),$model->getId());
								$aresult=$auth->assign($auth->getRole($request['user']->role),$model->getId());
								Yii::info([
									'mode'=>"change auth",
									'userid'=>$model->getId(),
									'auth-from'=>$uroles,
									'auth-to'=>$request['user']->role,
									'adminid'=>Yii::$app->user->id,
									'result'=>$aresult]
								,"authassign");
							}
							if (isset($request['user']->group) && $model->group!=$request['user']->group) {
								Yii::info([
									'mode'=>"change group",
									'group-from'=>$model->group,
									'group-to'=>$request['user']->group,
									'userid'=>$model->getId(),
									'adminid'=>Yii::$app->user->id]
								,"authassign");
								$model->group=$request['user']->group;
								$out['saved']=true;
							}
							break;
						case 'shipping':
							if (isset($request['user']->params->shipping)) {
								$params->shipping=$request['user']->params->shipping;
								$out['saved']=true;
							}
							break;
						case 'invoice':
							if (isset($request['user']->params->invoice)) {
								$params->invoice=$request['user']->params->invoice;
								$out['saved']=true;
							}
							if (isset($request['user']->params->payDefer)) {
								$params->payDefer=$request['user']->params->payDefer;
								$out['saved']=true;
							}
							break;
						case 'discount':
							if (isset($request['user']->params->discount)) {
								$params->discount=$request['user']->params->discount;
								$out['saved']=true;
							}
							break;
						case 'notes':
							if (isset($request['user']->notes)) {
								$notes=$request['user']->notes;
								$out['saved']=true;
							}
							break;
					}
					if ($out['saved']==true) {
						if (isset($request['mode']) && $request['mode']=="notes") $model->notes=$notes;
							else $model->params=$params;
						$model->scenario="admin";
						$out['saved']=$model->save();
						$out['errors']=$model->errors;
					}
				} else {
					$out['msg']=Yii::t('users',"err_not_permit");
				}
			} else {
				$out['msg']=Yii::t('users',"err_no_model");
			}
		} else {
			$out['msg']=Yii::t('site',"err_bad_data");
		}
		
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

	/*
	public function actionMigrate()
	{
		return;
		$dmap=[
			"fotoalbum"=>10,
			"albumKlasyczny"=>11,
			"odbitki"=>15,
			"powiekszenia"=>17,
			"fotoobrazy"=>17,
			"fotoksiazeczka"=>18,
			"etuiNaAlbum"=>19,
			"etuiNaCD"=>19,
			"etuiNaPendrive"=>19,
			"pakiety"=>12,
			"rabat_plyty_dvd"=>13,
		];

		$users=Yii::$app->db2->createCommand("select * from user__user where id>1")->queryAll();
		foreach ($users as $u) {
			$user=new Users();
			$user->scenario="migrate";
			$user->email=$u['email'];
			$user->firstname=$u['firstname'];
			$user->lastname=$u['lastname'];
			if (strlen($u['phone'])>=9) $user->phone=$u['phone'];
			$user->lastactive=$u['last_login'];
			$user->created=$u['created_at'];
			$user->previd=$u['id'];
			$user->status=$u['enabled']?1:3;
			$user->password=$u['password'];
			$user->salt=$u['salt'];

			$discount=[];
			foreach ($dmap as $k=>$mid) {
				if ($u[$k] && $mid) $discount[$mid]=$u[$k];
			}

			$addr=Yii::$app->db2->createCommand("select * from user__address where user_id=".$u['id'])->queryAll();

			$shipping=[];
			$invoice=[];
			foreach ($addr as $a) {
				switch ($a['type']) {
					case 'billing':
							$invoice['nip']=trim($a['nip']);
							$invoice['name']=trim($a['firstName']." ".$a['lastName']." ".$a['company']);
							$invoice['street']=trim($a['address']);
							$invoice['postcode']=trim($a['postcode']);
							$invoice['postcity']=trim($a['city']);
							if (strlen($a['phone'])>5) $invoice['phone']=trim($a['phone']);
						break;
					case 'deliver':
							$shipping['name']=trim($a['firstName']." ".$a['lastName']." ".$a['company']);
							$shipping['street']=trim($a['address']);
							$shipping['postcode']=trim($a['postcode']);
							$shipping['postcity']=trim($a['city']);
							if (strlen($a['phone'])>5) $shipping['phone']=trim($a['phone']);
						break;
				}
			}

			$params=[];
			if (sizeof($discount)>0) $params['discount']=$discount;
			if (sizeof($shipping)>0) $params['shipping']=$shipping;
			if (sizeof($invoice)>0) $params['invoice']=$invoice;
			$user->params=$params;

			if ($user->save()) {
			$auth = Yii::$app->authManager;
			Yii::$app->authManager->assign($auth->getRole('customer'),$user->getId());
			} else {
				var_dump($user->attributes,$user->errors);
				break;
			}

			//var_dump($user->attributes);
		}
	}*/

	public function actionSudo($id=null)
	{
		$model=Users::findOne($id);
		if ($model) {
			Yii::$app->user->login($model);;
		}
		return $this->redirect("/");
	}


//enc class
}
