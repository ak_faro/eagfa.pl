<?php
namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use app\models\Products;
use app\models\Categories;


class GalleryController extends \app\components\BaseController
{

	public function actionIndex()
	{
		$products=[];
		foreach(Products::find()->where(['status'=>1])->all() as $product){
			if ($product->galleryCount>0) {
				$products[]=[
					'id'=>$product->id,
					'cat'=>$product->group,
					'foto'=>$product->getFoto(),
					'name'=>$product->name,
					'images'=>$product->prepGallery,
				];
			}
		}
		$params=[
			'categories'=>ArrayHelper::toArray(Categories::getCategories(0),['app\models\Categories'=>[
				'id','name',
				'foto'=>function($item){ return $item->getFoto(); },
				]]),
			'products'=>$products
		];
		return $this->render('index.twig',$params);
	}

	/*
	public function actionCategory($category)
	{
		$model=Categories::findOne(['name'=>$category,'type'=>0]);

		if ($model==null) throw new NotFoundHttpException(Yii::t('categories','err_empty_model'),404);

		$params=[
			'model'=>$model,
			'categories'=>$model::getCategories(0),
			'childs'=>ArrayHelper::toArray($model->childrens,['app\models\Products'=>['id','name','foto'=>function($item){ return $item->getFoto(); },'intro']]),
		];

		return $this->render('category.twig',$params);
	}

	public function actionProduct($category,$product)
	{
		$model=Products::find()
			->joinWith('category')
			->where(['products.name'=>$product,'categories.name'=>$category])
			->one();

		if ($model==null) throw new NotFoundHttpException(Yii::t('products','err_empty_model'),404);

		$params=[
			'model'=>$model,
			'categories'=>Categories::getCategories(0),
		];

		return $this->render('product.twig',$params);
	}
	*/

//end class
}
