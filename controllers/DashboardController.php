<?php
namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Orders;
use app\models\Users;
use app\models\Events;

class DashboardController extends \app\components\BaseController
{

	public $model="Users";

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
					[
						'actions' => ['persons'],
						'allow' => true,
						'roles' => ['merchant'],
					],
					[
						'actions' => [],
						'allow' => true,
						'roles' => ['administrator'],
					],
				],
			],
		];
	}

	public function actions()
	{
		$r=$this->getJson();
		$where=[
			"or",
			["like","email",(isset($r->q)?$r->q:"")],
			["like","phone",(isset($r->q)?$r->q:"")],
			["like","concat(firstname,' ',lastname)",(isset($r->q)?$r->q:"")],
			["like","concat(lastname,' ',firstname)",(isset($r->q)?$r->q:"")]
		];
		if (isset($r->user) && $r->user && (!isset($r->contact)||$r->contact==false)) $where=['and',["=",'type',"user"],$where];
		if (isset($r->contact) && $r->contact && (!isset($r->user)||$r->user==false)) $where=['and',["=",'type',"contact"],$where];

		return [
			'persons'=>[
				'class'=>"\app\components\ListAction",
				'model'=>"\app\models\Persons",
				'map'=>[
					'app\models\Persons'=>['id','firstname','lastname','phone','email','type','parent','created'],
					'app\models\Users'=>['id','name'],
				],
				'limit'=>6,
				'order'=>"lastname asc,firstname asc",
				'where'=>$where
			],
			'save' => [
				'class'=>'\app\components\SaveAction',
				'savedMsg'=>Yii::t('users','msg_saved'),
				'notsavedMsg'=>Yii::t('users','msg_notsaved'),
				'scenario'=>"user",
				'modelId'=>Yii::$app->user->id,
			],
			'savepass'=>[
				'class'=>"\app\components\PassAction",
			],
		];
	}

	public function actionIndex()
	{
		$periods=[1,7,14,30,90,180];
		//Yii::$app->params['pager']['size']
		$params=[
			'panelTitle'=>"Dashboard",
			'panelIcon'=>"dashboard",
			'orders'=>Orders::find()->orderBy('id desc')->limit(15)->all(),
			'ordersCount'=>(new \yii\db\Query())->select(['status','count(*) as amount'])->from(Orders::tableName())->groupBy(['status'])->all(),
			'orderStatus'=>Orders::getStatuses(),
			'usersCount'=>(new \yii\db\Query())->select(['status','count(*) as amount'])->from(Users::tableName())->groupBy(['status'])->all(),
			'userStatus'=>Users::getStatuses(),
			'sumBrutto'=>Orders::sumBrutto($periods),
			'sumAmount'=>Orders::sumCount($periods),
			'regAmount'=>Users::regCount($periods),
			'periods'=>$periods,
			'target'=>['type'=>0,'id'=>0,'label'=>Yii::t('events','label_unspecifed')],
			'events'=>ArrayHelper::toArray(Events::find()->where(['owner_id'=>Yii::$app->user->id])->all(),[
				'app\models\Events'=>['id','title','description','start','end'=>'prepEnd','remind','targetLabel','target_type','target_id'],
			])
		];
		return $this->render('admin.twig',$params);
	}

	public function actionSettings()
	{
		$params=[
			'panelTitle'=>"Ustawienia",
			'panelIcon'=>"settings",
			'user'=>ArrayHelper::toArray(
				Yii::$app->user->identity,
				['app\models\Users'=>['id','firstname','lastname','phone']]
			),
		];
		return $this->render('settings.twig',$params);
	}

//end class
}
