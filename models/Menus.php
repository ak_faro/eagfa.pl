<?php
namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use creocoder\nestedsets\NestedSetsBehavior;

class Menus extends \app\models\BaseModel
{

	public static function tableName()
	{
		return 'menu';
	}

	public function behaviors() {
		return [
			'tree' => [
				'class' => NestedSetsBehavior::className(),
				'treeAttribute'=>'tree'
			]
		];
	}

	public function rules()
	{
		return [
			[['name'], 'required'],
			[['name'],'string','min'=>3],
			[['link','style','class'],'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('menus', 'model_id'),
			'name' => Yii::t('menus', 'model_name'),
			'link' => Yii::t('menus', 'model_link'),
			'style' => Yii::t('menus', 'model_style'),
			'class' => Yii::t('menus', 'model_class'),
		];
	}

	public function getAncestor()
	{
		if ($this->depth>0) {
			$p=$this->parents(1)->one();
			return $p->id;
		} else {
			return 0;
		}
	}

	public function getOrder()
	{
		if ($this->depth>0) {
			$p=$this->parents(1)->one();
			$prev=$this->prev()->one();
			return intval(($prev && $prev->depth==$this->depth)?$prev->id:0);
		} else {
			return 0;
		}
	}

	public function getLinkItems() {
		$tab=[];
		foreach(ArrayHelper::toArray(json_decode($this->link)) as $k=>$v) {
			$tab[]=['key'=>$k,'value'=>$v];
		}
		return $tab;
	}

	//all ancestors path
	public function getPath()
	{
		$out=[];
		if ($this->depth>0) {
			$all=$this->parents()->all();
			foreach ($all as $m) $out[]=$m->name;
		}
		$out[]=$this->name;
		return $out;
	}

	public function getTreeArray($tree=null)
	{
		return static::getTree($tree,false);
	}

	//podane tree albo wszystkie
	public static function getTree($tree=null,$flat=true)
	{
		if ($tree!=null) {
			$roots=static::findOne($tree);
		} else {
			$roots=static::find()->roots()->all();
		}
		$out=[
			['id'=>0,'name'=>Yii::t('menus','main_node')]
		];
		foreach($roots as $root) {
			if ($flat) $out[$root->id]=$root->name;
				else $out[]=[
					'id'=>intval($root->id),
					'name'=>$root->name,
					'left'=>$root->lft,
					'right'=>$root->rgt
				];
			$childs=$root->children()->all();
			foreach ($childs as $c) {
				if ($flat) $out[$c->id]=str_repeat("+",$c->depth).$c->name;
					else $out[]=[
						'id'=>intval($c->id),
						'name'=>str_repeat("+",$c->depth).$c->name,
						'root'=>$root->name,
						'left'=>$c->lft,
						'right'=>$c->rgt
					];
			}
		}
		return $out;
	}

	public function getSiblings($p=null)
	{
		if ($p==null) $p=$this->parents(1)->one();
		if ($p==null) return [];
		$childs=$p->children(1)->all();
		$out=[
			[ 'id'=>0, 'name'=>Yii::t('menus','order_first')]
		];
		foreach ($childs as $c) {
			if ($c->id==$this->id) continue;
			$out[]=['id'=>intval($c->id), 'name'=>$c->name];
		}
		return $out;
	}

	public static function find()
	{
		return new MenusQuery(get_called_class());
	}

	public function getUrl() {
		if (strlen($this->link)>3) {
			$data=json_decode($this->link);
			if ($data!=null) return Url::to(ArrayHelper::toArray($data));
		}
		return $this->link;
	}

} //end class
