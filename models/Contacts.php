<?php
namespace app\models;

use Yii;

class Contacts extends \app\models\BaseModel
{

	public static function tableName()
	{
		return 'contacts';
	}

	public function rules()
	{
		return [
			[['firstname', 'lastname'], 'required'],
			[['notes', 'params'], 'safe'],
			[['firstname', 'lastname', 'phone'], 'string', 'max' => 50],
			[['status'],'integer','min'=>1,'max'=>2],
			[['status'],'default','value'=>1],
			[['email'],'email'],
			['email', 'unique', 'targetAttribute' => 'email'],
			['email', 'unique', 'targetClass'=>"\app\models\Users",'targetAttribute' => 'email'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('contacts', 'model_id'),
			'created' => Yii::t('contacts', 'model_created'),
			'firstname' => Yii::t('contacts', 'model_firstname'),
			'lastname' => Yii::t('contacts', 'model_lastname'),
			'email' => Yii::t('contacts', 'model_email'),
			'phone' => Yii::t('contacts', 'model_phone'),
			'notes' => Yii::t('contacts', 'model_notes'),
			'owner' => Yii::t('contacts', 'model_owner'),
			'status' => Yii::t('contacts', 'model_status'),
		];
	}

	public static function getStatuses()
	{
		return [
			1=>Yii::t('contacts','status_active'),
			2=>Yii::t('contacts','status_blocked'),
		];
	}

	public function getStatusLabel()
	{
		return self::getStatuses()[$this->status];
	}

	public function getOwner()
	{
		return $this->hasOne(Users::className(), ['id' => 'owner_id']);
	}

	public function getName() {
		return ucwords(trim($this->firstname)." ".trim($this->lastname));
	}

	public function afterFind()
	{
		if (is_string($this->params)) $this->params=json_decode($this->params);
		if (is_string($this->notes)&&strlen($this->notes)>0) {
			$this->notes=json_decode($this->notes);
		} elseif ($this->notes===null||$this->notes==="")  {
			$this->notes=[];
		}
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (is_array($this->params)||is_object($this->params)) $this->setAttribute('params',json_encode($this->params));
			if (is_array($this->notes)||is_object($this->notes)) $this->setAttribute('notes',json_encode($this->notes));
			if ($insert) $this->owner_id=Yii::$app->user->id;
			return true;
		} else {
			return false;
		}
	}
//end class
}
