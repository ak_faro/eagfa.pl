<?php

namespace app\models;

use Yii;

class Comments extends \app\models\BaseModel
{

	public static function tableName()
	{
		return 'comments';
	}

	public function rules()
	{
		return [
			[['user', 'user_id','status'], 'safe'],
			[['text','user'], 'required'],
			[['text','user'], 'string','min'=>2],
		];
	}

	public function attributeLabels()
	{
		return [
		'id' => Yii::t('comments', 'model_id'),
		'text' => Yii::t('comments', 'model_text'),
		'user' => Yii::t('comments', 'model_user'),
		'created' => Yii::t('comments', 'model_created'),
		'status' =>  Yii::t('comments', 'model_status'),
		];
	}

	public static function getStatuses()
	{
		return [
			0=>Yii::t('comments', 'status_new'),
			1=>Yii::t('comments', 'status_ok'),
		];
	}

	public function getStatusLabel()
	{
		return self::getStatuses()[$this->status];
	}

	//end class
}
