<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Products extends \app\models\BaseModel
{


	public function init()
	{
		$this->price=0;
		$this->status=0;
		$this->name="";
		$this->foto="";
		$this->params=[];
		$this->images=[];
		$this->amount=[];
		$this->gallery=[];
		$this->intro="";
	}

	public static function tableName()
	{
		return 'products';
	}

	public function rules()
	{
		return [
			[['name', 'price','status','params','foto','group'], 'required'],
			[['status'], 'integer'],
			[['status'],'in','range'=>[0,1]],
			['group','integer','min'=>1],
			['name', 'string','min'=>3],
			['foto', 'string','min'=>5],
			[['price'], 'number','min'=>0],
			[['images','amount','intro','export','gallery'],'safe'],
			[['sort'], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('products', 'model_id'),
			'name' => Yii::t('products', 'model_name'),
			'price' => Yii::t('products', 'model_price'),
			'params' => Yii::t('products', 'model_params'),
			'status' => Yii::t('products', 'model_status'),
			'foto' => Yii::t('products', 'model_foto'),
			'images' => Yii::t('products', 'model_images'),
			'gallery' => Yii::t('products', 'model_gallery'),
			'amount' => Yii::t('products', 'model_amount'),
			'group'=> Yii::t('products', 'model_group'),
			'intro'=> Yii::t('products', 'model_intro'),
			'sort' => Yii::t('products', 'model_sort'),
		];
	}

	public function attributeFields()
	{
		return [
			'name' => 'text',
			'price' => 'text',
			'status' => 'dropdown',
			'group' => 'dropdown',
		];
	}

	public static function getStatuses()
	{
		return [
			0=>Yii::t('products', 'status_0'),
			1=>Yii::t('products', 'status_1'),
		];
	}

	public static function getSchema()
	{
		return [
			'name'=>"",
			'type'=>NULL,
			'group'=>"",
			'matrix'=>"0",
			'cond'=>[],
			'notes'=>"",
			'multiple'=>0,
			'size'=>'10', //rozmiar kolumny w %
			'noauto'=>false,
		];
	}

	public static function getImageSchema()
	{
		return [
			'path'=>"",
			'layer'=>0,
			'cond'=>[],
		];
	}

	public static function getGallerySchema()
	{
		return [
			'path'=>"",
			'name'=>"",
		];
	}

	public static function getOption()
	{
		return [
			'name'=>"",
			'notes'=>"",
			'price'=>"0",
			'data'=>"",
			'default'=>false,
			'cond'=>[],
		];
	}

	public static function getImageCond()
	{
		return [
			'name'=>"",
			'value'=>"",
		];
	}


	public static function getParamTypes()
	{
		return [
			0=>Yii::t('products', 'paramTypes_text'),
			1=>Yii::t('products', 'paramTypes_btn'),
			2=>Yii::t('products', 'paramTypes_ico'),
			3=>Yii::t('products', 'paramTypes_color'),
			4=>Yii::t('products', 'paramTypes_list'),
			5=>Yii::t('products', 'paramTypes_pages'),
			6=>Yii::t('products', 'paramTypes_notes'),
			7=>Yii::t('products', 'paramTypes_num'),
		];
	}

	public static function getParamSizes()
	{
		return [
			'10'=>"10%",
			'15'=>"15%",
			'20'=>"20%",
			'25'=>'25%',
			'33'=>'33%',
			'50'=>'50%',
			'none'=>Yii::t('products', 'autosizing')
		];
	}

	public function afterFind()
	{
		if (is_string($this->params)) $this->params=json_decode($this->params);
		if (is_string($this->images)) $this->images=json_decode($this->images);
		if (is_string($this->gallery)) $this->gallery=json_decode($this->gallery);
		if (is_string($this->amount)) $this->amount=json_decode($this->amount);
		if (is_string($this->export)) $this->export=json_decode($this->export);
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (is_array($this->params)) $this->setAttribute('params',json_encode($this->params));
			if (is_array($this->images)) $this->setAttribute('images',json_encode($this->images));
			if (is_array($this->gallery)) $this->setAttribute('gallery',json_encode($this->gallery));
			if (is_array($this->amount)) $this->setAttribute('amount',json_encode($this->amount));
			if (is_array($this->export)&&sizeof($this->export)>0) $this->setAttribute('export',json_encode($this->export));
				else $this->setAttribute('export',"");
			return true;
		} else {
			return false;
		}
	}

	public function getFoto()
	{
		if (strlen($this->foto)>0) {
			return "/pictures/".$this->foto;
		} else {
			return "/images/nofoto1.png";
		}
	}

	public function getIcon($icon="")
	{
		if (strlen($icon)>0) {
			return "/pictures/".$icon;
		} else {
			return "/images/nofoto0.png";
		}
	}

	public function getStatusLabel()
	{
		return self::getStatuses()[$this->status];
	}

	//setData czy ustawiać data, default ustawienie param/option.default
	//onlyData tylko same 'data'
	public function getPrepParams($setData=true,$default=[],$onlyData=false)
	{
		$step=0;
		$out=[];

		foreach($this->params as $k=>$param){
			if ((!isset($param->matrix)||$param->matrix!=1) and $k>0) $step++;
			$param->step=$step;

			if (isset($default[$param->name]) && $param->type>0 &&$param->type!=7 && isset($param->options)) {
				if (array_search($default[$param->name],ArrayHelper::getColumn($param->options, 'name'))!==false) {
					foreach($param->options as $option) {
						$option->default=($option->name==$default[$param->name]?true:false);
					}
				}
			}

			if ($setData) {
				if ($param->type>0 &&$param->type!=7 && isset($param->options)) {
					foreach($param->options as $option) {
						if (isset($option->default)&&$option->default) {
							$param->data=['name'=>$option->name,'price'=>$option->price];
							break;
						}
					}
				} elseif (in_array($param->type,[0,7]) && isset($param->text->default)) {
					$param->data=['name'=>$param->text->default,'price'=>0];
				}
			}
			if ($onlyData) {
				$out[]=isset($param->data)?json_decode(json_encode($param->data)):new \StdClass();
			} else {
				$out[]=$param;
			}
		}
		return $out;
	}

	//zsumowanie cen z wybranych opcji parametrów
	public function paramSum($data,$basket=null)
	{
		$sum=0;
		if (is_array($data) && sizeof($data)>0) {
			foreach($this->params as $k=>$param){
				$sum+=isset($data[$k])?$this->findPrice($param,$data[$k],$basket):0;
			}
		}
		return $sum;
	}

	//sprawdzenie aktualności cen wybranych opcji
	public function updatePrices($data)
	{
		$out=[];
		if (is_array($data) && sizeof($data)>0) {
			foreach($this->params as $k=>$param){
				if (isset($data[$k])) {
					$out[$k]=$data[$k];
					$out[$k]->price=$this->findPrice($param,$data[$k],$data);
				}
			}
		}
		return $out;
	}

	public function findNotes($param,$name) {
		if (is_array($name)) return $name;
		$out=$name;
		if (isset($param->options) && is_array($param->options)) {
			foreach($param->options as $option) {
				if ($option->name==$name && isset($option->notes) && strlen($option->notes)>0) {
					$out.=" (".$option->notes.")";
					break;
				}
			}
		}
		return $out;
	}

	public function findPrice($param,$data,$basket=null)
	{
		$price=0;
		if (is_object($data)) $data=ArrayHelper::toArray($data);
		if (isset($param->options)) {
			$options=ArrayHelper::map($param->options,'name','price');
			if (isset($options[$data['name']])) $price=$options[$data['name']];
		}

		if ($param->type==7 && isset($param->price)) {
			$pagmin=(isset($param->text)&&isset($param->text->min))?$param->text->min:0;
			$pages=isset($data['name'])?intval($data['name']):0;
			$minv=0;

			if (isset($param->price->cond)&&is_array($param->price->cond)) {
				foreach($param->price->cond as $cond) {
					if (!isset($cond->price)||!isset($cond->start)) continue;

					if (isset($cond->name) && strlen($cond->name)>0) {
						foreach ($this->params as $k=>$p) {
							if ($basket && isset($basket['data'][$k]) && isset($basket['data'][$k]->name)) {
								$pos=$basket['data'][$k]->name;
							}
							if (isset($p->name) && isset($pos) && $cond->name==$p->name && isset($cond->value)) {
								if (is_array($cond->value)) {
									if (array_search($pos,$cond->value)!==false) {
										if ($pages>=$cond->start&&($minv==0||$cond->start>$minv)) {
											$minv=$cond->start;
											$price=($pages-$pagmin)*$cond->price;
										}
									}
								} else {
									if ($cond->value==$pos) {
										if ($pages>=$cond->start&&($minv==0||$cond->start>$minv)) {
											$minv=$cond->start;
											$price=($pages-$pagmin)*$cond->price;
										}
									}
								}
								break;
							}
						}
					} else {
						if ($pages>=$cond->start&&($minv==0||$cond->start>$minv)) {
							$minv=$cond->start;
							$price=($pages-$pagmin)*$cond->price;
						}
					}
				}
			} elseif (isset($param->price->value)) {
				$price=($data['name']-$pagmin)*$param->price->value;
			}
			//$price=($data['name']-(isset($param->text->min)?$param->text->min:0))*$param->text->price;
		}

		if ($param->type==5 && isset($param->multi->price)) {
			$price=sizeof($data['name'])*$param->multi->price;
		}

		if (isset($basket) && isset($data['depend'])) {
			foreach ($this->params as $k=>$p) {
				if ($p->name==$data['depend'] && isset($basket['data'][$k]) && isset($basket['data'][$k]->name)) {
					$price=$price*$basket['data'][$k]->name;
					break;
				}
			}
		}

		return $price;
	}

	public function getDiscount($user=null)
	{
		if ($user==null) {
			if (Yii::$app->user->isGuest) return 0;
				else $discount=Yii::$app->user->identity->discount;
		} else {
			$discount=$user->discount;
		}
		return (isset($this->category->id) && isset($discount[$this->category->id]))?$discount[$this->category->id]:0;
	}

	//zwocenie listy:start,price, z/bez dyskontu
	public function amountPrices($data)
	{
		if (isset($this->amount->cond) && is_array($this->amount->cond) && sizeof($this->amount->cond)>0) {
			$params=array_unique(ArrayHelper::getColumn($this->amount->cond,'name'));
			$out=[];
			foreach($params as $item) {
				foreach ($data as $k=>$p) {
					if (!isset($this->params[$k]) || !isset($this->params[$k]->name) || $this->params[$k]->name!=$item) continue;
					foreach($this->amount->cond as $cond) {
						if ($cond->name==$item&&$cond->value==$p->name) {
							$out[intval($cond->start)]=$cond->price;
						}
					}
					break;
				}
			}
			ksort($out);
			return $out;
		}
		return [];
	}

	//zwrocenie wyliczonej ceny
	public function amountPrice($data,$amount)
	{
		$out=0;
		foreach($this->amountPrices($data) as $start=>$price) {
			if ($start>$amount) continue;
			$out=$price;
		}
		return $out;
	}

	public function getConfiguration($data)
	{
		$out=[];
		foreach($data as $i=>$v){
			if (isset($v->name)&&isset($this->params[$i])&&isset($this->params[$i]->name)) {
				$out[$i]=[
					'name'=>$this->params[$i]->name,
					'value'=>$v->name,
				];
			} else {
				$out[$i]=null;
			}
		}
		return $out;
	}

	public function getCategory()
	{
		return $this->hasOne(Categories::className(), ['id' => 'group']);
	}

	//print data
	public function zalamoPrintValues($i,$amount=true)
	{
		$out=[];
		if (isset($this->export->zalamo)) {
			$z=ArrayHelper::toArray($this->export->zalamo);
			$p=ArrayHelper::map($this->params,'name','options');
			$out=[];
			foreach (['format','paper','fill'] as $n) {
				if (isset($p[$z[$n]])) {
					$opts=ArrayHelper::map($p[$z[$n]],'name','default');
					if (isset($z[$n."s"])) {
						$param=isset($z[$n."s"][$i[$n]])?$z[$n."s"][$i[$n]]:"";
					} else {
						$param=$i[$n];
					}
					if (in_array($param,array_keys($opts))) {
						$out[$z[$n]]=$param;
					} else {
						$out[$z[$n]]=array_search(true,$opts);
					}
				}
			}
			if ($amount) $out["Ilość"]=(isset($i['amount'])&&$i['amount']?$i['amount']:1)." szt.";
		}
		return $out;
	}

	public function getGalleryCount() {
		return is_array($this->gallery)? sizeof($this->gallery): 0;
	}

	public function getPrepGallery()
	{
		$out=[];
		if (is_array($this->gallery)) {
			foreach($this->gallery as $image){
				$out[]=[
					'name'=>$image->name,
					'foto'=>Url::to(['files/crop','file'=>$image->path]),
					'path'=>$image->path
				];
			}
		}
		return $out;
	}

//end class
}