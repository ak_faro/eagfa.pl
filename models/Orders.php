<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use app\models\Users;
use app\components\Emailer;

class Orders extends \app\models\BaseModel
{

	public static function tableName()
	{
		return 'orders';
	}

	public function rules()
	{
		return [
			[['shipping','payment'],'required'],
			[['shipping','invoice'],'integer','min'=>0,'max'=>1],
			[['payment'],'integer','min'=>0,'max'=>4], //min->0 dla online
			['status','integer','min'=>-1,'max'=>3],
			[['items', 'params','user_id','brutto','user_name','user_group'],'safe'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('orders', 'model_id'),
			'items' => Yii::t('orders', 'model_items'),
			'params' => Yii::t('orders', 'model_params'),
			'status' => Yii::t('orders', 'model_status'),
			'user_id' => Yii::t('orders', 'model_user'),
			'created' => Yii::t('orders', 'model_created'),
			'brutto' => Yii::t('orders', 'model_brutto'),
			'invoice' => Yii::t('orders', 'model_invoice'),
		];
	}


	public static function getStatuses()
	{
		return [
			0=>Yii::t('orders', 'status_new'), //nowe
			1=>Yii::t('orders', 'status_work'), //w realizacji
			2=>Yii::t('orders', 'status_ready'), //gotowe
			3=>Yii::t('orders', 'status_closed'),//wysłane/wydane
			-1=>Yii::t('orders', 'status_cancel'),//anulowane
		];
	}

	public static function getStatusesData()
	{
		return [
			-1=>['color'=>"858585",'icon'=>"delete"],
			0=>['color'=>"30b66c",'icon'=>"system_update_alt"],
			1=>['color'=>"df0024",'icon'=>"update"],
			2=>['color'=>"000000",'icon'=>"exit_to_app"],
			3=>['color'=>"307fb6",'icon'=>"flight_takeoff"],
		];
	}

	public static function getShiptypes()
	{
		return [
			0=>"wysyłka",
			1=>"odbiór osobisty",
		];
	}

	public static function getInvoicetypes()
	{
		return [
			0=>"paragon",
			1=>"faktura",
		];
	}

	public static function getPayments()
	{
		return [
			//0=>Yii::t('orders','paytype_0'),
			1=>Yii::t('orders','paytype_1'),
			2=>Yii::t('orders','paytype_2'),
			3=>Yii::t('orders','paytype_3'),
			4=>Yii::t('orders','paytype_4'),
		];
	}

	public function getStatusLabel()
	{
		return self::getStatuses()[$this->status];
	}

	public function getStatusData()
	{
		return self::getStatusesData()[$this->status];
	}

	public static function getLocations()
	{
		return Yii::$app->params['collectLoc'];
	}

	public function getLocation(){
		if (is_string($this->params)) $params=json_decode($this->params);
			else $params=$this->params;
		if ($this->shipping){
			$collect=ArrayHelper::getValue($params,'collect');
			return isset(self::getLocations()[$collect])? self::getLocations()[$collect]:"";
		} else {
			$adr=ArrayHelper::getValue($params,'address');
			if($adr) {
				return Users::shippingFormat($adr);
			} else {
				return "";
			}
		}
	}

	public static function getInvoiceAccounts()
	{
		return Yii::$app->params['invoiceAccouts'];
	}

	public function getPaymentLabel()
	{
		return self::getPayments()[$this->payment];
	}

	public function getShippingLabel()
	{
		return self::getShiptypes()[$this->shipping];
	}

	public function getInvoiceLabel()
	{
		return self::getInvoicetypes()[$this->invoice];
	}

	public function getInvoiceData()
	{
		if (is_string($this->params)) $params=json_decode($this->params);
			else $params=$this->params;
		if ($this->invoice) {
			if (ArrayHelper::keyExists('invoice',$params)) {
				return Users::invoiceFormat($params->invoice);
			} else {
				return "brak danych";
			}
		} else {
			return "";
		}
	}

	public function getShipCost($type=null)
	{
		$cost=0;
		if ($type==null) $type=$this->payment;
		if ($this->shipping==0) {
			switch($type) {
				case 1: $cost=Yii::$app->params['shipping']['instant']; break;
				case 2: $cost=Yii::$app->params['shipping']['delay']; break;
				case 3:
				case 4: $cost=Yii::$app->params['shipping']['delivery']; break;
			}
		}
		return $cost;
	}

	public function getProjects()
	{
		$params=$this->params;
		return isset($params->projects)?ArrayHelper::toArray($params->projects):[];
		//który projekt
	}

	public function getProjectsParsed()
	{
		$out=[];
		$params=$this->params;
		if (isset($params->projects)) {
			foreach (ArrayHelper::toArray($params->projects) as $k=>$proj) {
				foreach ($proj as $i=>$p) {
					if (isset($p['provider'])&&$p['provider']=="zalamo"&&isset($p['mode'])&&$p['mode']=="print") {
						$proj[$i]['data']=$this->zalamoPrintValues($k,$i);
					}
				}
				$out[]=$proj;
			}
		}
		return $out;
	}

	public function afterFind()
	{
		if (is_string($this->params)) $this->params=json_decode($this->params);
		if (is_string($this->items)) $this->items=array_values(json_decode($this->items));
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (!is_string($this->params)) $this->setAttribute('params',json_encode($this->params));
			if (!is_string($this->items)) $this->setAttribute('items',json_encode($this->items));
			if ($this->isAttributeChanged('status') && $this->status==3 ) $this->user->rewardRefer();
			return true;
		} else {
			return false;
		}
	}

	public function afterSave($insert, $changedAttributes) {
		if ($insert) {
			$this->email("user");
			$this->email("admin");

			$params=is_string($this->params)?json_decode($this->params):$this->params;
			if (isset($params->coupons) && is_array($params->coupons)) {
				Coupons::updateAll(['status'=>0],['id'=>$params->coupons]);
			}
		}
	}

	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			$this->deleteFiles();
			return true;
		} else {
			return false;
		}
	}

	public function canDelete()
	{
		return in_array($this->status,[-1]);
	}

	public function deleteFiles()
	{
		if (is_dir($this->updir)) {
			@FileHelper::removeDirectory($this->updir);
			$dir=Yii::getAlias(Yii::$app->params['folders']['upload'])."/".$this->user->ftpname;
			if (sizeof(glob($dir . '/*' , GLOB_ONLYDIR))==0) @FileHelper::removeDirectory($dir);
		}
	}

	public function getTags()
	{
		$params=(is_object($this->params))?$this->params:json_decode($this->params);
		return [
			'[id]'=>$this->id,
			'[total]'=>$this->brutto-(isset($params->shipCost)?$params->shipCost:0),
			'[brutto]'=>$this->brutto,
			'[shipcost]'=>(isset($params->shipCost)?$params->shipCost:0),
			'[username]'=>$this->user->name,
			'[userid]'=>$this->user->id,
			'[userphone]'=>$this->user->phone,
			'[useremail]'=>$this->user->email,
			'[created]'=>($this->created?$this->displayDate($this->created):date(self::$db_save_datetime)),
			'[location]'=>nl2br($this->location),
			'[invoice]'=>nl2br($this->invoiceData),
			'[shiptype]'=>$this->shippingLabel,
			'[payment]'=>$this->paymentLabel,
			'[bill]'=>$this->invoiceLabel,
			'[link]'=>Url::to(['panel/order','id'=>$this->id,'files'=>'true'],true),
			'[userlink]'=>Url::to(['users/show','id'=>$this->user->id],true),
			'[orderlink]'=>Url::to(['orders/show','id'=>$this->id],true),
			'[items]'=>$this->mailRows,
			'[notice]'=>isset($params->notice)?$params->notice:"",
			'[bankaccount]'=>isset(self::getInvoiceAccounts()[$this->invoice])?self::getInvoiceAccounts()[$this->invoice]:"",
		];
	}

	public function email($mode="user",$debug=false)
	{
		$options=[
		];
		
		if ($mode=="user") {
			$options=[
				'email'=>$this->user->email,
				'schema_id'=>22,
				'tags'=>$this->tags
			];
		} else {
			$options=[
				'email'=>Yii::$app->params['adminEmail'],
				'schema_id'=>23,
				'tags'=>$this->tags,
				'attach'=>[
					'schema_id'=>36,
					'name'=>"zamówienie_".$this->id.".pdf",
				]
			];
		}
		
		if ($this->user->id>1) {
			return Emailer::queue($options,$debug);
		}
	}

	public function getMailRows($table=true)
	{
		$out="";
		$items=!is_array($this->items)?json_decode($this->items):$this->items;

		foreach ($items as $item) {
			if ($table) {
				$config="";
				foreach($item->config as $param) {
					$config.=$param->name.": ".(is_array($param->value)?implode(", ",$param->value):$param->value)."<br />\n";
				}
				$out.="<tr><td><div style=\"font-size:17px;font-weight:bold;\">".$item->category." - ".$item->name."</div>".$config."</td>";
				$out.="<td>".$item->amount."</td>";
				$out.="<td>".$item->price." PLN</td>";
				$out.="<td>".$item->discount." %</td>";
				$out.="<td>".((isset($item->price2)&&$item->price2)?$item->price2:$item->price)." PLN</td></tr>";
			} else {
				$out.=Yii::t('products','model_name').": ".$item->category." - ".$item->name."<br />";
				$out.=Yii::t('orders','label_amount').": ".$item->amount."<br />";
				$out.=Yii::t('orders','label_itemprice').": ".$item->price." PLN<br />";
				$out.=Yii::t('orders','label_discount').": ".$item->discount." %<br />";
				$out.=Yii::t('orders','label_price2').": ".($item->price2?$item->price2:$item->price)." PLN<br />";
				$out.="Konfiguracja:<br />";
				foreach($item->config as $param) {
					$out.=$param->name.": ".(is_array($param->value)?implode(", ",$param->value):$param->value)." | ";
				}
				$out.="<br /><br />";
			}
		}
		if (strlen($out)>1&&$table){
			$head="<td>".Yii::t('products','model_name')."</td>";
			$head.="<td>".Yii::t('orders','label_amount')."</td>";
			$head.="<td>".Yii::t('orders','label_itemprice')."</td>";
			$head.="<td>".Yii::t('orders','label_discount')."</td>";
			$head.="<td>".Yii::t('orders','label_price2')."</td>";
			$out="<table border=\"1\" cellpadding=\"5\" align=\"center\" style=\"font-size:14px;width:100%;\"><tr>".$head."</tr>".$out."</table>";
		}
		return $out;
	}

	public function statusInform()
	{
		Emailer::queue([
			'email'=>$this->user->email,
			'schema_id'=>24,
			'tags'=>[
				'[id]'=>$this->id,
				'[status]'=>$this->statusLabel,
				'[created]'=>$this->displayDate($this->created),
			]
		]);
	}

	public function canUpload()
	{
		return in_array($this->status,[0,1]);
	}

	public function getUpdir()
	{
		if ($this->user) {
			return Yii::getAlias(Yii::$app->params['folders']['upload'])."/".$this->user->ftpname."/".$this->id;
		} else {
			return false;
		}
	}

	public function getFiles()
	{
		$out=[];
		$dir= $this->updir;
		if (is_dir($dir)) {
			foreach(new \DirectoryIterator($dir) as $item){
				if($item->isDot()) continue;
				//$f=$this->fileType($item->getExtension());
				$item=[
					'dir'=>$item->isDir()?1:0,
					'name'=>$item->getFileName(),
					'size'=>$item->getSize(),
					//'type'=>$f['type'],
					//'icon'=>$f['icon'],
				];
				$out[]=$item;
			}
		}
		return $out;
	}

	public function projectUrl($project)
	{
		if (isset($project['provider'])) {
			switch($project['provider']){
				case 'zalamo':
					if (isset($project['hash'])&&isset($project['mode'])) {
						if ($project['mode']=="project") return "http://zalamo.com/api/eagfa/files/hash/".$project['hash'];
						if ($project['mode']=="print")   return "http://zalamo.com/api/eagfa/zip/hash/".$project['hash'];
					}
					break;
			}
		}
	}

	public static function printParse($print)
	{
		$items=[];
		$print=ArrayHelper::toArray($print);
		if (isset($print['provider']) && isset($print['data'])) {
			switch($print['provider']){
				case 'zalamo':
					foreach($print['data'] as $foto=>$row) {
						if (isset($row[0])){
							$f=ArrayHelper::toArray($row[0]);
							$tag=$f['format']."_".$f['paper']."_".$f['fill'];
							if (!isset($items[$tag])) $items[$tag]=['amount'=>0,'foto'=>[]];
							$items[$tag]['amount']+=$f['count'];
							$items[$tag]['foto'][]=$foto;
							$items[$tag]['format']=$f['format'];
							$items[$tag]['paper']=$f['paper'];
							$items[$tag]['fill']=$f['fill'];
						}
					}
					break;
			}
		}
		return array_values($items);
	}

	//pos item, pos project in item
	public function zalamoPrintValues($pos,$pos2) {
		$out=[];
		if (isset($this->projects[$pos]) && isset($this->projects[$pos][$pos2]) && isset($this->items[$pos])) {
			$prod=$this->getProduct($this->items[$pos]);
			if ($prod) {
				$print=self::printParse($this->projects[$pos][$pos2]);
				foreach ($print as $i) {
					$out[]=$prod->zalamoPrintValues($i);
				}
			}
		}
		return $out;
	}

	public function getUser()
	{
		return $this->hasOne(Users::className(), ['id' => 'user_id']);
	}

	public static function sumBrutto($period)
	{
		$out=[];
		foreach($period as $p) {
			$out[$p]=(float)(new \yii\db\Query())->from(self::tableName())->where("TIMESTAMPDIFF(DAY,created,now())<=".$p)->sum('brutto');
		}
		return $out;
	}

	public static function sumCount($period)
	{
		$out=[];
		foreach($period as $p) {
			$out[$p]=(int)(new \yii\db\Query())->from(self::tableName())->where("TIMESTAMPDIFF(DAY,created,now())<=".$p)->count();
		}
		return $out;
	}

	public function getProduct($item)
	{
		return isset($item->id)?\app\models\Products::findOne($item->id):null;
	}

//end class
}