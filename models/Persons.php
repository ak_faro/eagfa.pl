<?php
namespace app\models;

use Yii;

class Persons extends \app\models\BaseModel
{

	public static function tableName()
	{
		return 'persons';
	}

	public function getParent()
	{
		if ($this->parent_id>0) return $this->hasOne(Users::className(), ['id' => 'parent_id']);
		return null;
	}

//end class
}
