<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Categories extends \app\models\BaseModel
{

	public static function tableName()
	{
		return 'categories';
	}

	public function rules()
	{
		return [
			[['name','type'], 'required'],
			[['foto'], 'required','when' => function($model) {return $model->type==0; }],
			[['foto','subtitle','intro'], 'string'],
			[['type'], 'integer','min'=>0,'max'=>1],
			[['sort'], 'integer'],
			[['name'], 'string', 'min'=>2,'max' => 250]
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('categories', 'model_id'),
			'name' => Yii::t('categories', 'model_name'),
			'foto' => Yii::t('categories', 'model_foto'),
			'sort' => Yii::t('categories', 'model_sort'),
			'subtitle' => Yii::t('categories', 'model_subtitle'),
			'intro' => Yii::t('categories', 'model_intro'),
		];
	}

	public static function getTypes()
	{
		return [
			0=>"Produkty",
			1=>"Treści",
		];
	}

	public function getTypeLabel()
	{
		return self::getTypes()[$this->type];
	}

	public function getChildrens()
	{
		$out=null;
		switch($this->type){
			case 0: $out=$this->hasMany(Products::className(), ['group' => 'id'])->orderBy('sort asc'); break;
			case 1: $out=$this->hasMany(Articles::className(), ['group' => 'id']); break;
		}
		return $out;
	}

	public function getChilds()
	{
		return $this->getChildrens()->count();
	}

	//type
	public function getSiblings($type=0)
	{
		$models=self::find()->where(['type'=>$type])->orderBy('sort asc')->all();
		//return ArrayHelper::map($models,'id','name');
		$out=[
			[ 'id'=>0, 'name'=>Yii::t('categories','order_first')]
		];
		foreach ($models as $c) {
			if ($c->id==$this->id) continue;
			$out[]=['id'=>intval($c->id), 'name'=>$c->name];
		}
		return $out;
	}

	public function getFoto($mode="")
	{
		if (strlen($this->foto)>0) {
			if ($mode=="style") return "";
			else return "/images/".$this->foto;
		} else {
			if ($mode=="style") return "width:100px;";
			else return "/images/nofoto1.png";
		}
	}

	public static function getList($type=null)
	{
		return static::find()->select(['id','name'])->where(($type!==null?['type'=>$type]:[]))->asArray()->all();
	}

	public static function getCategories($type=null)
	{
		return static::find()->where(($type!==null?['type'=>$type]:[]))->orderBy('sort asc')->all();
	}

	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			return ($this->childs>0)?false:true;
		} else {
			return false;
		}
	}

//end class
}
