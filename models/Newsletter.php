<?php
namespace app\models;

use Yii;
use yii\helpers\Url;
use app\components\Emailer;

class Newsletter extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'newsletter';
	}

	public function rules()
	{
		return [
			[['email'], 'required'],
			[['email'], 'email','message'=>Yii::t('newsletter', 'err_email')],
			[['email'], 'unique'],
			[['status'], 'integer','min'=>0,'max'=>1],
			['hash','safe'],
		];
	}

	public function attributeLabels()
	{
		return [
			'email' => Yii::t('newsletter', 'model_email'),
			'status' => Yii::t('newsletter', 'model_status'),
			'created' => Yii::t('newsletter', 'model_created'),
		];
	}

	public static function getStatuses()
	{
		return [
			0=>Yii::t('newsletter','status_new'),
			1=>Yii::t('newsletter','status_active'),
		];
	}

	public function getStatusLabel()
	{
		return self::getStatuses()[$this->status];
	}

	public function beforeSave($insert) {
		if ($insert) {
			$this->hash=$this->calcHash();
		}
		return parent::beforeSave($insert);
	}

	public function afterSave($insert, $changedAttributes) {
		if ($insert) {
			Emailer::queue([
				'email'=>$this->email,
				'schema_id'=>16,
				'tags'=>[
					'[link]'=>Url::to(['newsletter/confirm','hash'=>$this->hash], true),
				]
			]);
		}
	}

	public function calcHash()
	{
		$hash=hash('md5', mt_rand().time());
		if (self::findOne(['hash'=>$hash])) $hash=$this->calcHash();
		return $hash;
	}


//end class
}
