<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\components\Emailer;


class Users extends \app\models\BaseModel implements \yii\web\IdentityInterface
{

	/* dziedziczone */
	public $accessToken;
	public $errorCode;
	public $authKey;

	public $password0;
	public $passwordOrginal;
	public $password2; //new repeat

	public static function tableName()
	{
		return 'users';
	}

	public function rules()
	{
		return [
			[['email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
			[['email'], 'filter', 'filter' => function($v) { return strtolower($v); }, 'skipOnArray' => true],

			[['email'],'email','on'=>['register']],
			[['email'],'unique','on'=>['register','admin']],
			[['email'],'exist','on'=>["login",'remind']],
			[['email'],'required','on'=>['login','remind','register']],
			[['email', 'password'], 'string', 'max' => 255],

			[['password0'],'required','on'=>['changepass']],
			[['password0'],'isPassValid','on'=>['changepass']],
			//[['password0'],'exist','targetAttribute'=>'password','filter'=>['id'=>$this->id],'on'=>"changepass"],
			[['password'],'required','on'=>['login','resetpass','register']],
			[['password'],'required','on'=>['changepass'],'message'=>Yii::t('users','err_empty_currpass')],
			//[['password'],'exist','filter'=>['email'=>$this->email],'on'=>"login"],
			[['password','password2'],'string','min'=>5,'on'=>['register','resetpass','changepass']],
			[['password2'],'required','on'=>['register','resetpass','changepass']],
			[['password2'],'compare', 'compareAttribute'=>'password','on'=>['register','resetpass','changepass'],'message' =>Yii::t('users','err_diff_pass')],

			[['firstname', 'lastname', 'phone'], 'required','on'=>['register','user','admin']],
			[['firstname'], 'string','min'=>3,'max'=>50],
			[['lastname'], 'string','min'=>2,'max'=>50],

			[['phone'],'string','min'=>9],
			[['phone'],'safe','on'=>['admin']],

			[['params','hash','last_active','created','notes'], 'safe'],
			[['salt','lastactive','created','previd'], 'safe'],
			[['status'], 'integer'],
			[['vip'],'integer','min'=>0,'max'=>1],
			[['group'],'checkGroup'],
			[['refer'],'checkRefer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('users', 'model_id'),
			'email' => Yii::t('users', 'model_email'),
			'password' => Yii::t('users', 'model_password'),
			'password0' => Yii::t('users', 'password0'),
			'password2' => Yii::t('users', 'password2'),
			'firstname' => Yii::t('users', 'model_firstname'),
			'lastname' => Yii::t('users', 'model_lastname'),
			'phone' => Yii::t('users', 'model_phone'),
			'status' => Yii::t('users', 'model_status'),
			'created' => Yii::t('users', 'model_created'),
			'group'=> Yii::t('users', 'model_group'),
			'refer'=> Yii::t('users', 'model_refer'),
			'vip'=> Yii::t('users', 'model_vip'),
		];
	}

	public function scenarios()
	{
		return [
			'login' => ['email', 'password'],
			'register' => ['email', 'password','password2','firstname','lastname','phone','group','refer'],
			'user'=>['firstname','lastname','phone','params','notes'],
			'admin'=>['email','firstname','lastname','phone','status','params','group','notes','vip'],
			'remind'=>['hash'],
			'resetpass'=>['password','password2'],
			'changepass'=>['password0','password','password2'],
			//'migrate'=>['email','password','salt','firstname','lastname','phone','status','params','created','lastactive','previd'],
			'default'=>['lastactive'],
		];
	}

	public static function getStatuses()
	{
		return [
			0=>Yii::t('users','status_new'),
			1=>Yii::t('users','status_active'),
			2=>Yii::t('users','status_blocked'),
		];
	}

	public function getStatusLabel()
	{
		return self::getStatuses()[$this->status];
	}

	public static function getPriorities()
	{
		return [
			0=>Yii::t('users','priority_normal'),
			1=>Yii::t('users','priority_vip'),
		];
	}

	public function getName() {
		return ucwords(trim($this->firstname)." ".trim($this->lastname));
	}

	public static function shippingFormat($data)
	{
		return (isset($data->name)?$data->name."\n":"").(isset($data->street)?$data->street."\n":"").((isset($data->postcode)&&isset($data->postcity))?$data->postcode." ".$data->postcity:"");
	}

	//text-return txt/obj
	public function getShipping($text=false) {
		$params=$this->params;
		if ($text) {
			if ($params && isset($params->shipping)) return self::shippingFormat($params->shipping);
					else return "";
		} else {
			if ($params && isset($params->shipping)) return $params->shipping;
				else return [];
		}
	}

	public function getEmailerData()
	{
		if (isset($this->params->emailer)) return $this->params->emailer;
		return null;
	}

	public static function invoiceFormat($data)
	{
		return (isset($data->nip)?"NIP: ".$data->nip."\n":"").(isset($data->name)?$data->name."\n":"").(isset($data->street)?$data->street."\n":"").((isset($data->postcode)&&isset($data->postcity))?$data->postcode." ". $data->postcity:"");
	}

	public function getInvoice($text=false) {
		$params=$this->params;
		if ($text) {
			if ($params && isset($params->invoice)) return self::invoiceFormat($params->invoice);
				else return "";
		} else {
			if ($params && isset($params->invoice)) return $params->invoice;
				else return [];
		}
	}

	public function getPayDefer() {
		$params=$this->params;
		return isset($params->payDefer)?$params->payDefer:Yii::$app->params['invoice']['defer'];
	}

	public function getDiscount()
	{
		$params=$this->params;
		return isset($params->discount)?ArrayHelper::toArray($params->discount):[];
	}

	public function login()
	{
		if ($this->validate()) {
			$user=$this->findByEmail($this->email);
			if ($user && $user->validatePassword($this->password)) {
				if ($user->status==1) {
					$user->lastactive = new \yii\db\Expression('NOW()');
					$user->update();
					return Yii::$app->user->login($user, 3600*24*30);
				} else {
					$this->addError('status', Yii::t('users','err_status_'.$user->status));
					return false;
				}
			} else {
				$this->addError('password', Yii::t('users','err_bad_pass'));
				return false;
			}
		} else {
			return false;
		}
	}

	public function calcPassword($password,$salt="") {
		$salted = $password.(strlen($salt)>0?"{".$salt."}":"");
		$digest = hash("sha512", $salted, true);
		for ($i = 1; $i < 5000; ++$i) {
			$digest = hash("sha512", $digest.$salted, true);
		}
		return base64_encode($digest);
	}

	public function validatePassword($password,$msg="") {
		if ($this->calcPassword($password,$this->salt)==$this->passwordOrginal){
			return true;
		} else {
			$this->addError('password'.$msg, Yii::t('users','err_bad_pass'.$msg));
			return false;
		}
	}

	public function isPassValid($attr) {
		return $this->validatePassword($this->$attr,substr($attr,-1));
	}

	public static function findByEmail($email)
	{
		return static::findOne(['email'=>strtolower($email)]);
	}

	public function can($role)
	{
		return Yii::$app->authManager->checkAccess($this->id,$role);
	}

	public function getRoles($full=true)
	{
		$auth=Yii::$app->authManager;
		$out=[];
		foreach($auth->getAssignments($this->id) as $r) {
			$role=$auth->getRole($r->roleName);
			if ($full) {
				$out[]=['id'=>$r->roleName,'label'=>$role->description];
			} else {
				$out[]=$role->description;
			}
		}
		return $out;
	}

	public function getRole()
	{
		$roles=$this->getRoles();
		return isset($roles[0])?$roles[0]['id']:"";
	}

	public function getRoleLabel()
	{
		$roles=$this->getRoles();
		return isset($roles[0])?$roles[0]['label']:"?";
	}

	public function getOrders($status=null)
	{
		if ($this->can('merchant')) {
			//return $this->hasMany(Orders::className(), ['user_id' => 'id'])->viaTable('users',['group'=>'id']);
			return $this->hasMany(Orders::className(), ['user_group' => 'id']);
		} else {
			$where=($status!==null)?['status'=>$status]:[];
			return $this->hasMany(Orders::className(), ['user_id' => 'id'])->where($where);
		}
	}

	public function getOrdersCount()
	{
		return $this->getOrders()->count();
	}

	public function getOrdersSum()
	{
		return array_sum(ArrayHelper::getColumn($this->getOrders()->asArray()->all(),function ($element) {
			return floatval($element['brutto']);
		}));
	}

	public function getLastDate() {
		$last=ArrayHelper::getColumn($this->getOrders()->asArray()->all(),function ($element) {
			return \DateTime::createFromFormat(self::$db_save_datetime, $element['created'])->format("U");
		});
		return (is_array($last)&&sizeof($last)>0)? date(self::$display_date,max($last)) : "";
	}

	public function getLastDays() {
		$from=$this->getLastDate();
		if ($from=="") return "";
		$diff=date_diff(new \DateTime("now"),\DateTime::createFromFormat(self::$display_date,$from));
		return $diff->format("%a");
	}

	public function getMerchant() {
		if ($this->can('customer')) {
			return $this->hasOne(self::className(),['id'=>'group']);
		} else {
			return null;
		}
	}

	public function getReferer() {
		return $this->hasOne(self::className(),['id'=>'refer']);
	}

	public function getCustomers()
	{
		if ($this->can('merchant')||$this->can('administrator')) {
			return $this->hasMany(self::className(), ['group' => 'id']);
		} else {
			return null;
		}
	}

	public function getCustomersCount()
	{
		if ($this->can('merchant')||$this->can('administrator')) {
			return $this->getCustomers()->count();
		} else {
			return 0;
		}
	}

	public function getContacts()
	{
		if ($this->can('merchant')||$this->can('administrator')) {
			return $this->hasMany(Contacts::className(), ['owner_id' => 'id']);
		} else {
			return null;
		}
	}

	public function getContactsCount()
	{
		if ($this->can('merchant')||$this->can('administrator')) {
			return $this->getContacts()->count();
		} else {
			return 0;
		}
	}

	public function getFollowers()
	{
		return $this->hasMany(self::className(), ['refer' => 'id']);
	}

	public static function getMerchants($full=false)
	{
		$models=self::find()->innerJoin('auth_assignment','auth_assignment.user_id=users.id')->where('auth_assignment.item_name="merchant"')->all();
		return $full?$models:ArrayHelper::map($models,'id','name');
	}

	public function getCoupons($status=null)
	{
		$where=($status!==null)?['status'=>$status]:[];
		return $this->hasMany(Coupons::className(), ['user_id' => 'id'])->where($where);
	}

	public function calcHash()
	{
		$hash=hash('md5', mt_rand().time());
		if (self::findOne(['hash'=>$hash])) $hash=$this->calcHash();
		return $hash;
	}

	public static function roleByCode($code)
	{
		$model=self::find()->where(['id'=>$code])->one();
		if ($model) {
			if (Yii::$app->authManager->checkAccess($model->id,'merchant')) return [$model->id,"merchant"];
			if (Yii::$app->authManager->checkAccess($model->id,'customer')) return [$model->id,"customer",$model->group];
		}
		return [0,null];
	}

	public function afterFind()
	{
		if (is_string($this->params)) $this->params=json_decode($this->params);
		if (is_string($this->notes)&&strlen($this->notes)>0) {
			$this->notes=json_decode($this->notes);
		} elseif ($this->notes===null||$this->notes==="")  {
			$this->notes=[];
		}
		//if (strlen($this->password)>0)
			$this->passwordOrginal=$this->password;
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (is_array($this->params)||is_object($this->params)) $this->setAttribute('params',json_encode($this->params));
			if (is_array($this->notes)||is_object($this->notes)) $this->setAttribute('notes',json_encode($this->notes));
			if ($insert) {
				$this->salt=hash('md5', mt_rand().time());
				$this->passwordOrginal=$this->password;
				$this->password=$this->calcPassword($this->password,$this->salt);
			}
			return true;
		} else {
			return false;
		}
	}

	public function afterSave($insert, $changedAttributes) {
		if ($insert) {
			Emailer::queue([
				'email'=>$this->email,
				'schema_id'=>25,
				'tags'=>[
					'[login]'=>$this->email,
					'[password]'=>$this->passwordOrginal,
				],
			]);
		}
	}

	//merchants
	public function getInviteHash() {
		return $this->id;
	}

	public function checkGroup($attribute,$param)
	{
		if(!Yii::$app->authManager->checkAccess($this->$attribute,'merchant')) {
			$this->addError($attribute, Yii::t('users','err_bad_group'));
		}
	}

	public function checkRefer($attribute,$param)
	{
		if(!self::find()->where(['id'=>$this->$attribute])->exists()) {
			$this->addError($attribute, Yii::t('users','err_bad_refer'));
		}
	}

	public function getProjects()
	{
		$params=$this->params;
		return isset($params->projects)?$params->projects:[];
	}

	public static function regCount($period)
	{
		$out=[];
		foreach($period as $p) {
			$out[$p]=(int)(new \yii\db\Query())->from(self::tableName())->where("TIMESTAMPDIFF(DAY,created,now())<=".$p)->count();
		}
		return $out;
	}

	public function getFtpname()
	{
		return str_replace("@","at",$this->email);
	}

	//czy dla polecającego wystawiony kupon
	public function getIsRefunded()
	{
		return $this->refer>0 && isset($this->params->refunded);
	}

	public function refundConfirm() {
		if ($this->refer>0) {
			$this->params->refunded=true;
			$this->update(false);
		}
	}

	public function rewardRefer()
	{
		if (Yii::$app->params['coupon']['limit']<=0 || Yii::$app->params['coupon']['reward']<=0) return false;
		if ($this->refer>0 && $this->ordersSum>=Yii::$app->params['coupon']['limit'] && !$this->isRefunded) $this->referer->rewardCoupon($this);
	}

	public function rewardCoupon($follower)
	{
		$coupon=new Coupons();
		$coupon->user_id=$this->id;
		$coupon->amount=Yii::$app->params['coupon']['reward'];
		$coupon->params=[
			'reason'=>Yii::t('app',Yii::$app->params['coupon']['reason'],$follower->name),
			'user'=>['id'=>$follower->id, 'name'=>$follower->name, 'orderSum'=>$follower->ordersSum],
		];
		if ($coupon->save(false)) $follower->refundConfirm();
	}

	
	public function paramsfromOrder($data)
	{
		$save=false;
		$params=$this->params;
		if (is_string($params) && strlen($params)>2) $params=json_decode($params);
		if (!is_object($params)) $params=new \stdClass();

		if (!isset($params->shipping) && isset($data['address']) && is_object($data['address'])) {
			$params->shipping=$data['address'];
			$save=true;
		}
		if (!isset($params->invoice) && isset($data['invoice']) && is_object($data['invoice'])) {
			$params->invoice=$data['invoice'];
			$save=true;
		}
		if ($save) {
			$this->params=$params;
			$this->update(false);
		}
	}
	
/* dziedziczone */

	public function getId()
	{
		return $this->id;
	}

	public function getAuthKey()
	{
		return $this->authKey;
	}

	public function validateAuthKey($authKey)
	{
		return $this->authKey === $authKey;
	}

	public static function findIdentity($id)
	{
		return self::findOne($id);
	}
	
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne(['token' => $token]);
	}

/* ********* */



//end class
}