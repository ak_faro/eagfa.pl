<?php

namespace app\models;

use Yii;

class Articles extends \app\models\BaseModel
{
	public $pages=[];

	public static function tableName()
	{
		return 'articles';
	}

	public static function getPagebreak()
	{
		return "<!-- pagebreak -->";
	}

	public function rules()
	{
		return [
			[['title', 'group'], 'required'],
			[['title', 'intro', 'full','style','image'], 'string'],
			[['group'], 'integer']
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('articles', 'model_id'),
			'title' => Yii::t('articles', 'model_title'),
			'intro' => Yii::t('articles', 'model_intro'),
			'full' => Yii::t('articles', 'model_full'),
			'group' => Yii::t('articles', 'model_group'),
			'style' => Yii::t('articles', 'model_style'),
			'created'=> Yii::t('articles', 'model_created'),
			'image'=> Yii::t('articles', 'model_image'),
		];
	}

	public function getStyleArray()
	{
		return (array)json_decode($this->style);
	}

	public function getDate()
	{
		return $this->displayDate($this->created);
	}

	public function afterFind() {
		if (sizeof($this->pages)==0) {
			$pages=explode(self::getPagebreak(),$this->full);
			if (is_array($pages)&&sizeof($pages)>0) {
				$last=0; $last2=-1;
				foreach($pages as $k=>$page) {
					preg_match("=<h1[^>]*>(.*)</h1>=siU", $page,$out0);
					if (sizeof($out0)<2) {
						preg_match("=<h2[^>]*>(.*)</h2>=siU", $page,$out);
						if (sizeof($out)>1) {
							$last2++;
							$this->pages[$last]['subpages'][$last2]=[
								'index'=>$k,
								'title'=>isset($out[1])?$out[1]:"",
								'body'=>isset($out[0])?str_replace($out[0],"",$page):$page,
							];
							continue;
						} else {
							preg_match("=<h3[^>]*>(.*)</h3>=siU", $page,$out2);
							if (sizeof($out2)>1) {
								$this->pages[$last]['subpages'][$last2]['subpages'][]=[
									'index'=>$k,
									'title'=>isset($out2[1])?$out2[1]:"",
									'body'=>isset($out2[0])?str_replace($out2[0],"",$page):$page,
								];
								continue;
							}
						}
					} else {
						$this->pages[$k]=[
							'index'=>$k,
							'title'=>isset($out0[1])?$out0[1]:"",
							'body'=>isset($out0[0])?str_replace($out0[0],"",$page):$page,
						];
					}
					$last=$k;
				}
 			}
		}
	}

	public function getFoto()
	{
		if (strlen($this->image)>0) {
			return "/images/".$this->image;
		} else {
			return "/images/nofoto1.png";
		}
	}

	public function getCategory()
	{
		return $this->hasOne(Categories::className(), ['id' => 'group']);
	}

	public static function getHtml($id=null)
	{
		$out=self::findOne($id);
		return ($out?$out->full:"");
	}


} //end class
