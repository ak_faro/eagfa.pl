<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Events extends \app\models\BaseModel
{

	public static $targetTypeUser=1;
	public static $targetTypeContact=2;

	public static function tableName()
	{
		return 'events';
	}

	public function rules()
	{
		return [
			[['start', 'title'], 'required'],
			[['start','end'], 'safe'],
			[['title','description', 'params'], 'string'],
			[['owner_id', 'target_type', 'target_id','remind'], 'integer'],
			[['target_type'], 'integer', 'min'=>0,"max"=>2],
			//sprawdzenie czy moze tworzyc/zapisac do danego usera/customera
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('events', 'model_id'),
			'created' => Yii::t('events', 'model_created'),
			'start' => Yii::t('events', 'model_when'),
			'end' => Yii::t('events', 'model_stop'),
			'remind' => Yii::t('events', 'model_remind'),
			'title' => Yii::t('events', 'model_title'),
			'description' => Yii::t('events', 'model_description'),
			'owner_id' => Yii::t('events', 'model_owner'),
		];
	}

	public static function userTargetLabel($name)
	{
		return self::prepTargetLabel(self::$targetTypeUser,$name);
	}

	public static function contactTargetLabel($name)
	{
		return self::prepTargetLabel(self::$targetTypeContact,$name);
	}

	public static function prepTargetLabel($type,$name="")
	{
		switch($type) {
			case self::$targetTypeUser: return $name." (klient)";
			case self::$targetTypeContact: return $name." (kontakt)";
			default: return Yii::t('events','label_unspecifed');
		}
	}

	public function getTargetLabel(){
		return self::prepTargetLabel($this->target_type, ($this->target?$this->target->name:""));
	}

	public function getOwner()
	{
		return $this->hasOne(Users::className(), ['id' => 'owner_id']);
	}

	//targetType 0-unspecified, 1-users,2-contacts
	public function getTarget()
	{
		switch ($this->target_type) {
			case 1: return $this->hasOne(Users::className(), ['id' => 'target_id']);
			case 2: return $this->hasOne(Contacts::className(), ['id' => 'target_id']);
			default: return null;
		}
	}

	//default start+5min
	public function getPrepEnd()
	{
		if ($this->end) return self::pickerDate($this->end);
		return \DateTime::createFromFormat(self::$db_save_datetime, $this->start)->add(new \DateInterval('PT5M'))->format(self::$picker_datetime);
	}

	public function getStartDate()
	{
		return self::pickerDate($this->start);
	}

	public function getRemindDate()
	{
		if ($this->remind>-1) {
			if ($this->remind==0) $out=\DateTime::createFromFormat(self::$db_save_datetime, $this->start);
				else $out=\DateTime::createFromFormat(self::$db_save_datetime, $this->start)->sub(new \DateInterval('PT'.$this->remind.'M'));
			return $out->format(self::$picker_datetime);
		}
		return null;
	}

	public function getTargetLink() {
		switch ($this->target_type) {
			case 1: return Url::to(['users/show','id'=>$this->target_id]);
			case 2: return Url::to(['contacts/show','id'=>$this->target_id]);
			default: return "";
		}
	}

	public static function forUser($user)
	{
		return self::forObject($user,1);
	}

	public static function forContact($user)
	{
		return self::forObject($user,2);
	}

	public static function forObject($user,$type)
	{
		$items=self::find()->where(['owner_id'=>Yii::$app->user->id, 'target_type'=>$type,'target_id'=>$user])->all();
		return ArrayHelper::toArray($items,[
			'app\models\Events'=>['id','title','description','start','end'=>'prepEnd','remind','targetLabel','target_type','target_id'],
		]);
	}

	public function afterFind()
	{
		if (is_string($this->params)) $this->params=json_decode($this->params);
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (!is_string($this->params)) $this->setAttribute('params',json_encode($this->params));
			if ($insert) $this->owner_id=Yii::$app->user->id;
			return true;
		} else {
			return false;
		}
	}

//end class
}
