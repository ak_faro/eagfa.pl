<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class BaseModel extends \yii\db\ActiveRecord
{

	public static $db_read_date="yyyy-MM-dd";
	public static $db_save_date="Y-m-d";
	public static $db_read_datetime="yyyy-MM-dd HH:mm:ss";
	public static $db_save_datetime="Y-m-d H:i:s";
	public static $db_empty_date="0000-00-00 00:00:00";
	public static $db_empty_time="00:00:00";

	public static $display_date="d.m.Y";
	public static $display_datetime="d.m.Y H:i";

	public static $picker_date="Y-m-d";
	public static $picker_datetime="Y-m-d\TH:i:s";


	public static function displayDate($d)
	{
		if ($d==null) return "";
		return \DateTime::createFromFormat(self::$db_save_datetime, $d)->format(self::$display_date);
	}

	public static function pickerDate($d)
	{
		return \DateTime::createFromFormat(self::$db_save_datetime, $d)->format(self::$picker_datetime);
	}

//end
}