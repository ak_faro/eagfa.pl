<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class Coupons extends \app\models\BaseModel
{

	public static function tableName()
	{
		return 'coupons';
	}

	public function rules()
	{
		return [
			[['user_id', 'amount'], 'required'],
			[['user_id', 'status'], 'integer'],
			[['amount'], 'number'],
			[['params'],'safe'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => Yii::t('coupons', 'model_id'),
			'user_id' => Yii::t('coupons', 'model_user'),
			'created' => Yii::t('coupons', 'model_created'),
			'amount' => Yii::t('coupons', 'model_amount'),
			'status' => Yii::t('coupons', 'model_status'),
	  ];
	}

	public static function getStatuses()
	{
		return [
			0=>Yii::t('coupons', 'status_used'),
			1=>Yii::t('coupons', 'status_active'),
		];
	}

	public function getStatusLabel()
	{
		return self::getStatuses()[$this->status];
	}

	public function getUser()
	{
		return $this->hasOne(Users::className(), ['id' => 'user_id']);
	}

	public function getReason()
	{
		if (is_object($this->params)) return isset($this->params->reason)?$this->params->reason:"";
			return isset($this->params['reason'])?$this->params['reason']:"";
	}

	public function afterFind()
	{
		if (is_string($this->params)) $this->params=json_decode($this->params);
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (!is_string($this->params)) $this->setAttribute('params',json_encode($this->params));
			return true;
		} else {
			return false;
		}
	}

	public static function prepDiscount($user,$coupons)
	{
		$items=self::find()->where(['status'=>1,'user_id'=>$user,'id'=>$coupons])->all();
		return [
			array_sum(ArrayHelper::getColumn($items,function ($element) { return floatval($element['amount']);})),
			ArrayHelper::getColumn($items,'id')
		];
	}

//end class
}
