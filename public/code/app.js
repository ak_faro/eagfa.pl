var app=angular.module('eagfa',['ngMaterial','ngAnimate','ngSanitize','ngMessages','angular.filter','ngScrollSpy'])

.directive('numberPicker', function($interval) {
	return {
		restrict: 'E',
		scope: {
			value:'=',
			min:'=',
			max:'=',
			step:'=?',
			pos:'=?', //ilosc suwaków:= 1
			pickdir:'@', //rozmieszczenie przyciskow: column/row
			calldep:'=',
			fontset:'@',
			iconup:'@',
			icondown:'@',
			btnclass:'@',
			orderup:'@',
			orderdown:'@',
			ordernum:"@"
		},
		templateUrl: '/code/number-picker.html',
		link: function ($scope) {

			$scope.pos=$scope.pos?parseInt($scope.pos):1;
			$scope.posDiv=[];
			$scope.pickdir=$scope.pickdir||'column';
			$scope.min=parseFloat($scope.min)||undefined;
			$scope.max=parseFloat($scope.max)||undefined;
			//$scope.value=parseFloat($scope.value)||0;
			$scope.step=$scope.step?parseFloat($scope.step):1;
			$scope.fontset=$scope.fontset||'material-icons';
			$scope.btnclass=$scope.btnclass||'md-icon-button';
			$scope.iconup=$scope.iconup||'expand_less';
			$scope.icondown=$scope.icondown||'expand_more';

			$scope.orderup=$scope.order||1;
			$scope.ordernum=$scope.order||2;
			$scope.orderdown=$scope.order||3;

			$scope.timer=[];

			for (var a=$scope.pos-1;a>0;a--) {
				$scope.posDiv.push(Math.pow(10,a));
			}
			$scope.posDiv.push(1);

			$scope.increaseStart=function(i) {
				if ($scope.increase(i)) $scope.timer[i]=$interval($scope.increase,600,0,true,i);
					else $scope.$emit('picknumber',{'val':$scope.value,'depend':$scope.calldep});
				console.log($scope.timer[i]);
			};

			$scope.increase=function(i){
				if (angular.isDefined($scope.max) && $scope.value>=$scope.max){
					$scope.value=$scope.max;
					$scope.timerStop(i);
					return false;
				} else {
					if (angular.isDefined($scope.max)) {
						if (($scope.value+$scope.step*$scope.posDiv[i])>=$scope.max) {
							$scope.value=$scope.max;
							return false;
						}
					}
					$scope.value+=$scope.step*$scope.posDiv[i];
					return true;
				}
			};

			$scope.decreaseStart=function(i) {
				if ($scope.decrease(i)) $scope.timer[i]=$interval($scope.decrease,600,0,true,i);
					else $scope.$emit('picknumber',{'val':$scope.value,'depend':$scope.calldep});
			};

			$scope.decrease=function(i){
				if (angular.isDefined($scope.min) && $scope.value<=$scope.min){
					$scope.value=$scope.min;
					$scope.timerStop(i);
					return false;
				} else {
					if (angular.isDefined($scope.min)) {
						if (($scope.value-$scope.step*$scope.posDiv[i])<=$scope.min) {
							$scope.value=$scope.min;
							return false;
						}
					}
					$scope.value-=$scope.step*$scope.posDiv[i];
					return true;
				}
			};

			$scope.$watch('value',function(n){
				if (angular.isUndefined(n)||isNaN(n)) {
					$scope.value=angular.isDefined($scope.min)?$scope.min:1;
				} else {
					if (angular.isDefined($scope.min) && n<=$scope.min) $scope.value=$scope.min;
					if (angular.isDefined($scope.max) && $scope.value>=$scope.max) $scope.value=$scope.max;
				}
			});

			$scope.timerStop=function(i) {
				if (angular.isDefined($scope.timer[i])) {
					$interval.cancel($scope.timer[i]);
					$scope.timer[i]=undefined;
					$scope.$emit('picknumber',{'val':$scope.value,'depend':$scope.calldep});
				}
			};

			$scope.getPos=function(i){
				if ($scope.pos>1) {
					if($scope.posDiv[i]>1) {
						return Math.floor($scope.value/$scope.posDiv[i]);
					} else {
						return $scope.value%10;
					}
				} else {
					return $scope.value;
				}
			};

		} //link
	};
})

.directive('condList', ['$filter',function($filter) {
	return {
		restrict: 'E',
		scope: {
			'data':'=',
			'sort':'=',
			'list':'=',
			'msg':'=',
			'price':'=?',
			'logic':'=?',
		},
		templateUrl: '/code/condition-list.html',
		link: function ($scope) {

			$scope.price=$scope.price||false;
			$scope.logic=$scope.logic||false;
			if (!$scope.data) { $scope.data={}; }
			if (!$scope.data.cond) { $scope.data.cond=[]; }

			$scope.condChecked=function(cond,item) {
				return angular.isArray(cond)?cond.indexOf(item)>-1:cond==item;
			};

			$scope.delCond=function(i) {
				if (confirm($scope.msg.delcond)) { $scope.data.cond.splice(i,1); }
			};

			$scope.condToggle=function(cond,item) {
				if (angular.isArray(cond.value)) {
					var i=cond.value.indexOf(item);
					if (i>-1) cond.value.splice(i,1);
						else cond.value.push(item);
				} else {
					if (cond.value.length>0) {
						if (cond.value==item) {
							cond.value=[];
						} else {
							cond.value=[cond.value,item];
						}
					} else {
						cond.value=item;
					}
				}
			};

			$scope.getParamList=function() {
				return $filter('pick')($scope.list,"name.length>0&&['1','2','3','4'].indexOf(type+'')>-1");
			};

			$scope.getParamOptions=function(name) {
				var i=$filter('where')($scope.list,{'name':name});
				if (i.length>0) {
					if (angular.isDefined(i[0].options)) { return i[0].options; }
				}
				return [];
			};

		} //end link
	};
}]);
