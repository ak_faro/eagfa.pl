<?php
namespace app\components;

use Yii;
use yii\base\Action;
use yii\helpers\Url;
use yii\validators\EmailValidator;


class MessageAction extends Action
{

	public $model="Users";

	public function run()
	{
		$out=['status'=>false];
		$r=$this->controller->getJson();
		if (isset($r->email)&&isset($r->subject)&&isset($r->text)) {
			if (strlen($r->email)>=5&&strlen($r->subject)>0&&strlen($r->text)>0) {
				$vemail = new EmailValidator();
				if ($vemail->validate($r->email)) {
					$model="app\\models\\".$this->model;
					$model=$model::findOne(['email'=>$r->email]);
					if ($model) {
						if (Yii::$app->user->can('merchant') &&
							 !($this->model=="Contacts" ? Yii::$app->user->can('manageContact',$model->owner_id) : Yii::$app->user->can('manageCustomer',$model->group))) {
							$out['msg']=Yii::t('users','err_not_permit');
						} else {
							$out['status']=Emailer::queue([
								'email'=>$model->email,
								'sender_id'=>Yii::$app->user->id,
								'subject'=>$r->subject,
								'tags'=>['[message]'=>$r->text],
								'schema_id'=>Yii::$app->params['schema']['private'],
								'reply'=>(Yii::$app->user->can('administrator')?Yii::$app->params['adminEmail']:Yii::$app->user->identity->email),
							]);
						}
					} else {
						$out['msg']=Yii::t('users','err_no_model');
					}
				} else {
					$out['msg']="Niepoprawny e-mail";
				}
			} else {
				$out['msg']=Yii::t('site','err_form_empty');
			}
		} else {
			$out['msg']=Yii::t('site','err_form_fill');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

} //end class
?>