<?php
namespace app\components;

use Yii;
use FreshMail\RestApi;

class Freshmail
{
	public static $rest=null;

	public static function conn()
	{
		if (self::$rest==null) {
		self::$rest = new RestAPI();
		self::$rest->setApiKey( Yii::$app->params['freshmail']['key'] );
		self::$rest->setApiSecret( Yii::$app->params['freshmail']['secret'] );
		}
	}

	public static function getLists()
	{
		self::conn();
		$response = self::$rest->doRequest('subscribers_list/lists');
		if ($response && isset($response['lists'])) return $response['lists'];
			else return [];
	}

	public static function createList($name,$desc="")
	{
		self::conn();
		$response = self::$rest->doRequest('subscribers_list/create',['name'=>$name,'description'=>$desc]);
		if ($response && isset($response['hash'])) return $response['hash'];
			else return false;
	}

	//items {'email':""}
	public static function addMultipleUsers($list,$items)
	{
		self::conn();
		$out=[];
		foreach(array_chunk($items,100) as $chunk){
			$out[] = self::$rest->doRequest('subscriber/addMultiple',[
				'list'=>$list,
				'subscribers'=>$chunk,
				'state'=>1,
				'confirm'=>0
			]);
		}
		return $out;
	}

	public static function uploadList($data)
	{
		$hash=self::createList($data['list'],$data['desc']);
		if ($hash) {
			return self::addMultipleUsers($hash,$data['subscribers']);
		} else {
			return false;
		}
	}

//end class
}