<?php
namespace app\components;

use Yii;
use yii\base\Action;
use yii\helpers\Url;
use yii\validators\EmailValidator;
use app\models\Users;

class InviteAction extends Action
{

	public function run()
	{
		$out=['status'=>false];
		$r=$this->controller->getJson();
		if (isset($r->email)&&isset($r->subject)&&isset($r->text)) {
			if (strlen($r->email)>=5&&strlen($r->subject)>0&&strlen($r->text)>0) {
				$vemail = new EmailValidator();
				if ($vemail->validate($r->email)) {
					if (!Users::find()->where(['email'=>$r->email])->exists()) {
						$out['status']=Emailer::queue([
							'email'=>$r->email,
							'sender_id'=>Yii::$app->user->id,
							'schema_id'=>Yii::$app->params['schema']['invite'],
							'subject'=>$r->subject,
							'tags'=>[
								'[message]'=>$r->text,
								'[link]'=>Url::to(['site/register','code'=>Yii::$app->user->identity->inviteHash], true),
							],
						]);
					} else {
						$out['msg']="Użytkownik o tym adresie e-mail jest już zarejestrowany.";
					}
				} else {
					$out['msg']="Niepoprawny e-mail";
				}
			} else {
				$out['msg']=Yii::t('site','err_form_empty');
			}
		} else {
			$out['msg']=Yii::t('site','err_form_fill');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

} //end class
?>