<?php
namespace app\components;

use Yii;
use \app\models\Articles;
use \app\models\Users;

class Emailer
{
	/*
	email,schema,params
	*/
	public static function queue($msg,$debug=false)
	{
		if (isset($msg['schema'])||isset($msg['schema_id'])) {
			if (isset($msg['schema_id'])) $article=Articles::findOne($msg['schema_id']);
				elseif (isset($msg['schema'])) $article=Articles::find()->where(['title'=>$msg['schema']])->one();
			if ($article) {
				if (!isset($msg['subject'])) $msg['subject']=$article->title;
				if (isset($msg['tags'])) {
					$msg['body']=str_replace(array_keys($msg['tags']),array_values($msg['tags']),$article->full);
					$msg['subject']=str_replace(array_keys($msg['tags']),array_values($msg['tags']),$msg['subject']);
				} else {
					$msg['body']=$article->full;
				}
			}
		}
		if (isset($msg['email']) && isset($msg['subject']) && isset($msg['body'])) {
			try {
				$result=Yii::$app->mailer->compose()
					->setTo($msg['email'])
					->setSubject($msg['subject'])
					->setTextBody(strip_tags($msg['body']))
					->setFrom(Yii::$app->params['nofityEmail']);

				if (isset($msg['sender_id']) && $msg['sender_id']>0) {
					if (isset(Yii::$app->user) && $msg['sender_id']==Yii::$app->user->id) {
						$sender=Yii::$app->user->identity;
					} else {
						$sender=Users::findOne($msg['sender_id']);
					}
					if ($sender->emailerData) {
						$transport=Yii::$app->components['mailer']['transport'];
						$t=(array)$sender->emailerData;
						if (isset($t['username'])) $transport['username']=$t['username'];
						if (isset($t['password'])) $transport['password']=$t['password'];
						if (isset($t['host'])) $transport['host']=$t['host'];
						Yii::$app->mailer->setTransport($transport);
						$result->setFrom([$sender->email=>$sender->name]);
					}
				}

				if (isset($msg['reply'])) $result->setReplyTo($msg['reply']);

				$msg['body']=preg_replace_callback('/<img\s+src="([^"]+)"[^>]+>/i',function($m) use (&$result){
					$base=Yii::getAlias(Yii::$app->params['folders']['pub']);
					if (isset($m[1]) && file_exists($base.$m[1])) return str_replace($m[1],$result->embed($base.$m[1]),$m[0]);
				},$msg['body']);
				$result->setHtmlBody($msg['body']);

				if (isset($msg['attach']) && isset($msg['attach']['schema_id']) && isset($msg['attach']['name'])) {
					$article=Articles::findOne($msg['attach']['schema_id']);
					if ($article) {
						$pdf=Yii::$app->pdf;
						$pdf->content=str_replace(array_keys($msg['tags']),array_values($msg['tags']),$article->full);
						$pdf->destination=$pdf::DEST_STRING;
						$result->attachContent($pdf->render(), [
							'fileName'=>$msg['attach']['name'],
							'contentType'=>"application/pdf",
						]);
					}
				}

				$out=$result->send();
			} catch (Swift_TransportException $e) {
				$out=$e->getMessage();
			}
		} else {
			$out="bad params";
		}
		self::log($msg,$out);
		return $debug?$out:true;
	}

	public static function log($msg,$result)
	{
		Yii::$app->db->createCommand()->insert('emails',[
			'recipient'=>is_array($msg['email'])?implode(", ",$msg['email']):$msg['email'],
			'subject'=>$msg['subject'],
			'body'=>$msg['body'],
			'status'=>$result,
			'sender'=>isset($msg['sender_id'])?$msg['sender_id']:0
		])->execute();
	}


//end class
}