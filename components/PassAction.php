<?php
namespace app\components;

use Yii;
use yii\base\Action;

class PassAction extends Action
{

	public function run()
	{
		$out=['saved'=>false];
		$request=$this->controller->getJson();
		$model=Yii::$app->user->identity;

		$model->password0=isset($request->password0)?$request->password0:"";
		$model->password=isset($request->password)?$request->password:"";
		$model->password2=isset($request->password2)?$request->password2:"";

		$model->scenario="changepass";
		$model->validate();
		if (sizeof($model->errors)){
			$out['errors']=$model->errors;
		} else {
			$model->password=$model->calcPassword($model->password,$model->salt);
			$out['saved']=$model->save(false);
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

} //end class
?>