<?php
namespace app\components;

use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\data\Pagination;

// view: index

// searchBy kolumna do wyszukiwania po id
// where dodatkowe param wyszukiwania
// map filtr do arrayhelper

class IndexAction extends Action
{
	public $panelTitle;
	public $panelIcon;
	public $model=false;
	public $searchBy=null;
	public $where=[];
	public $map=[];
	public $params=[];
	public $order=[];//bezposredni sort
	public $sort=[];//mozliwe kolumny z get
	public $orderInit=false; //true: $order zawsze obecny w sort; false: zastepowany przez sort
	public $view=null;
	public $pageSize=0;
	public $tables=[];
	public $getParams=[]; //[tables],'where sql',true-no key
	public $join=[];
	public $summary=[]; //sumowanie kolumn;
	public $beforeFind=false;

	public function run($id=null,$json=false)
	{
		if ($this->beforeFind) $this->controller->{$this->beforeFind}($this);
		if ($this->pageSize==0) $this->pageSize=Yii::$app->params['pager']['size'];
		$model=$this->model?$this->model:$this->controller->getModel();
		$class=new $model();

		if (array_key_exists('tree',$class->behaviors)) {
			if ($id!=null) {
				$item=$this->controller->loadModel($id);
				$query=$item->children(1);
				$ancestor=$item->parents(1)->one();
			} else {
				$query=$model::find()->roots();
			}
			$count=$query->count();
			if ($this->pageSize>0) {
				$pagination = new Pagination(['totalCount' => $count,'pageSize'=>$this->pageSize]);
				$models = $query->offset($pagination->offset)->limit($pagination->limit)->all();
			} else {
				$models = $query->all();
			}

		} else {
			$where=($id!=null&&$this->searchBy!=null)?[$this->searchBy=>$id]:[];
			if (sizeof($this->where)>0) $where=sizeof($where)>0?array_merge($where,$this->where):$this->where;

			$order=Yii::$app->request->get('order',null);
			if ($order && in_array($order,$this->sort)) {
				$dir=Yii::$app->request->get('dir','asc');
				if (!in_array($dir,['asc','desc'])) $dir="asc";
				if ($this->orderInit) $this->order[$order]=($dir=='asc'?SORT_ASC:SORT_DESC);
					else $this->order=$order." ".$dir;
			}
			$query=$model::find()->where($where)->orderBy($this->order);

			if (sizeof($this->join)>0) {
				foreach($this->join as $j) {
					$query->join($j[0],$j[1],$j[2]);
				}
			}

			if (sizeof($this->getParams)>0) {
				$joinTables=[];
				foreach ($this->getParams as $k=>$p) {
					$a=Yii::$app->request->get($k,null);
					if ($a!==null) {
						$joinTables=array_merge($joinTables,$p[0]);
						if (isset($p[2]) && $p[2]) {
							$query->andWhere($p[1]);
						} else {
							$key=":".$k;
							$query->andWhere($p[1],[$key=>(preg_match("/like/i",$p[1])?"%".$a."%":$a)]);
						}
					}
				}
				if (sizeof($joinTables)>0) {
					$joinTables=array_unique($joinTables);
					foreach($joinTables as $t) {
						$query->innerJoin($t,$this->tables[$t]);
					}
				}
			}

			foreach ($this->summary as $s) {
				$summary[$s]=$query->sum($s);
			}
		}

		//var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

		$count=$query->count();

		if ($this->pageSize>0) {
			$pagination = new Pagination(['totalCount' => $count,'pageSize'=>$this->pageSize]);
			$models = $query->offset($pagination->offset)->limit($pagination->limit)->all();
		} else {
			$models = $query->all();
		}


		if (sizeof($this->map)>0) {
			$models=ArrayHelper::toArray($models,$this->map);
		}
		$params=[
			'item'=>((isset($item)&&$item)?$item:null),
			'models'=>$models,
			'panelTitle'=>$this->panelTitle,//.(isset($item)?": ".$item->name:""),
			'panelIcon'=>$this->panelIcon,
		];

		if ($this->pageSize>0) {
			$params['pagination']=$pagination;
			$params['pager']=[
				'page'=>$pagination->page+1,
				'pageSize'=>$pagination->pageSize,
				'pageCount'=>$pagination->pageCount,
				'totalCount'=>$pagination->totalCount,
			];
		}

		if (isset($summary)) $params['summary']=$summary;

		if (isset($ancestor)) {
			$params['ancestor']=$ancestor;
		}

		if ($json) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$out=[
				'items'=>$models,
			];
			if ($this->pageSize>0) $out['pagination']=$params['pager'];

			if (isset($summary)) $out['summary']=$summary;
			return $out;
		} else {
			return $this->controller->render($this->view?$this->view:'index.twig',array_merge($params,$this->params,['get'=>Yii::$app->request->get()]));
		}
	}

} //end class
?>