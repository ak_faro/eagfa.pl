<?php
namespace app\components;

use Yii;
use yii\base\Action;

/* 
BaseController: 
loadModel(), getJson(), sendJson(), setAlert()
*/

// save ajax post data

class SaveAction extends Action
{
	public $scenario;
	public $savedMsg;
	public $savedmultiMsg;
	public $notsavedMsg;
	public $throwError=true;
	public $flash=false; //czy ustawiać w sesji flash msg
	public $beforeCheck=false;
	public $afterSave=false;
	public $modelId=null;
	public $modelClass=null;
	public $multiSave=false;
	public $onlyInsert=false;

	public function run()
	{
		$out=['saved'=>false];
		$request=$this->controller->getJson();
		$data=$this->controller->prepare($request,'data');
		if (is_array($data) and sizeof($data)>0) {
			$id=$this->modelId;
			if ($this->modelId==null and isset($request->model_id) and $request->model_id>0) $id=$request->model_id;
			if ($this->modelClass) $this->controller->model=$this->modelClass;

			$amount=($this->multiSave && $id==null && isset($request->amount) && $request->amount>1)?$request->amount:1;

			for ($i=0;$i<$amount;$i++) {
				$model=$this->controller->loadModel($id,$this->throwError);
				if ($this->scenario!=null) $model->scenario=$this->scenario;
				if ($this->onlyInsert==false || ($this->onlyInsert && $model->isNewRecord)) {
					if ($model!==null) {
						if ($model->load($data)) {
							if ($this->beforeCheck) $result=$this->controller->{$this->beforeCheck}($request);
							if ($this->beforeCheck===false || $result) {
								if (array_key_exists('tree',$model->behaviors)) {
									$d=\yii\helpers\ArrayHelper::toArray($request->data);
									//nested behavior
									//zapis po edycji root bez zmiany parent przez save
									if (!isset($d['ancestor']) || $d['ancestor']==0) {
										$out['saved']=($model->isRoot()?$model->save():$model->makeRoot()); //multitree
										//appendTo jesli singletree
									} else {
										$mod=$this->controller->getModel();
										if (isset($d['order']) and $d['order']>0) {
											if ($d['order']!=$model->id) {
												$s=$mod::findOne($d['order']);
												if ($s!=null) $out['saved']=$model->insertAfter($s);
											} else {
												$out['saved']=$model->save();
											}
										} 
										if ($out['saved']==false) {
											$p=$mod::findOne($d['ancestor']);
											if ($p!=null) {
												if (isset($d['order']) and $d['order']==0) $out['saved']=$model->prependTo($p);
													else $out['saved']=$model->appendTo($p);
											} else {
												$out['saved']=($model->isRoot()?$model->save():$model->makeRoot());
											}
										}
									}
								} else {
									//normal model
									$out['saved']=$model->save();
								}
							}
						}
						if ($out['saved']) {
							if ($this->afterSave) {
								$result=$this->controller->{$this->afterSave}($request,$model);
								if (isset($result['out'])) $out=array_merge($out,$result['out']);
							}
							$out['model_id']=$model->id;
							if ($this->multiSave && $this->savedmultiMsg && $amount>1) {
								$out['msg']=$this->savedmultiMsg;
							} else {
								$out['msg']=($this->savedMsg!=null?$this->savedMsg:Yii::t('site','msg_saved'));
							}
							if ($this->flash) $this->controller->setAlert2($out['msg'],'success');
							//set ancestor?
						} else {
							$out['msg']=($this->notsavedMsg!=null?$this->notsavedMsg:Yii::t('site','msg_bad_data'));
							if (sizeof($model->errors)>0) $out['errors']=$model->errors;
						}
					} else {
						$out['msg']=Yii::t('site','err_empty_model');
					}
				} else {
					$out['saved']==false;
					$out['msg']=Yii::t('site','err_only_new');
				}
			}
		} else {
			$out['msg']=Yii::t('site','err_no_data');
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}

} //end class
?>