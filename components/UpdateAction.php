<?php
namespace app\components;

use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;

// create/edit model data
// view: update

class UpdateAction extends Action
{
	public $panelTitle;
	public $panelIcon;
	public $buttons;
	public $inject;
	public $allowedParents;
	public $params=[];
	public $map=[];
	//public $mode;
	//public $save;
	public $view="update.twig";

	public function run($id=null,$parentid=null)
	{
		if (is_array($this->allowedParents)) {
			if ($parentid==null||!in_array($parentid,$this->allowedParents)) {
				throw new BadRequestHttpException(Yii::t('site','err_bad_create'));
			}
		}

		if ($this->controller->action->id=='copy' && $id!=null) {
			$copy=$this->controller->loadModel($id);
			$model=$this->controller->loadModel(null);
			$model->attributes=$copy->attributes;
			if (array_key_exists('copyof',$model->attributes)) {
				$model->copyof=$copy->id;
			}
		} else {
			$model=$this->controller->loadModel($id);
		}

		//przypisz parentid do atrybutu modelu
		if ($parentid!=null&&$model->isNewRecord&&$this->inject) {
			$attr=$this->inject;
			$model->$attr=$parentid;
		}

		if ($parentid==null && method_exists($model, 'getAncestor')) {
			$parentid=$model->ancestor;
		}

		$params=[
			//'mode'=>($this->mode?$this->controller->{$this->mode}($model):""),
			//'save'=>($this->save?$this->controller->{$this->save}($model):"save"),
			//model zależny od mode
			'model'=>(sizeof($this->map)>0?ArrayHelper::toArray($model,$this->map):$model),
			'parentid'=>$parentid,
			'panelTitle'=>$this->panelTitle,
			'panelIcon'=>$this->panelIcon,
			//'buttons'=>($this->buttons?$this->buttons:$this->getButtons()),
		];
		return $this->controller->render($this->view,array_merge($params,$this->params));
	}

	public function getButtons()
	{
		return [
			[
				'name'=>Yii::t('site','save'),
				'icon'=>'',
				'color'=>'success',
				'emit'=>'save',
			],[
				'name'=>Yii::t('site','saveclose'),
				'icon'=>'',
				'color'=>'success',
				'emit'=>'saveclose',
			],[
				'name'=>Yii::t('site','cancel'),
				'icon'=>'',
				'color'=>'default',
				'emit'=>'cancel',
			]
		];
	}

} //end class
?>