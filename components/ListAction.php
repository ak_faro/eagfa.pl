<?php
namespace app\components;

use Yii;
use yii\base\Action;
use yii\helpers\ArrayHelper;

class ListAction extends Action
{
	public $where=[];
	public $columns=[];
	public $param=[];
	public $limit=null;
	public $order=[];
	public $map=[];
	public $join=[];
	public $model=false;

	public function run($id=null)
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if (sizeof($this->map)==0 && sizeof($this->columns)==0) return [];

		$model=$this->model?$this->model:$this->controller->getModel();

		$query=$model::find();
		if (sizeof($this->columns)>0) $query->select(array_keys($this->columns));
		$query->where($this->where)->orderBy($this->order)->limit($this->limit);

		//echo($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

		$items=ArrayHelper::toArray($query->all(),(sizeof($this->map)>0?$this->map:[$model=>array_flip($this->columns)]));

		if (sizeof($this->param)>0) {
			foreach($items as $k=>$v) {
				$items[$k]['param']=$this->param;
			}
		}
		return $items;
	}

} //end class
?>