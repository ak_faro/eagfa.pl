<?php
namespace app\components;

use Yii;
use yii\base\Action;

class DeleteAction extends Action
{

	public $readonly="removable";
	public $rule=null;
	public $field=null;

	public function run()
	{
		$out=['delete'=>false];
		$request=(array)$this->controller->getJson();
		$class=$this->controller->getModel();
		if (isset ($request['id'])) {
			$model=$class::findOne($request['id']);
			if ($model) {
				if (!isset($model->{$this->readonly})||(isset($model->{$this->readonly})&&$model->{$this->readonly})) {
					if ($this->rule && $this->field) {
						$del=Yii::$app->user->can($this->rule,$model->{$this->field});
					} elseif($this->rule) {
						$del=Yii::$app->user->can($this->rule);
					} else {
						$del=true;
					}
					if ($del) $out['delete']=$model->delete();
				}
			}
		}
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $out;
	}


//end class
}