<?php
namespace app\components;

use Yii;
use yii\web\View;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

class BaseController extends Controller {

	public $layout=false;
	public $savedMsg;
	public $notsavedMsg;
	public $model;

	public static function csrf()
	{
		return Yii::$app->request->csrfToken;
	}

	public static function user()
	{
		return Yii::$app->user->identity;
	}

	public function sendJson($out)
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		Yii::$app->response->data = $out;
	}

	public function getJson()
	{
		$obj=json_decode(file_get_contents('php://input'));
		return $obj!=null?$obj:new \stdClass();
	}

	public function prepare($request,$var) {
		if (isset($request->$var)) {
			$model=(isset($this->model)?$this->model:ucfirst($this->id));
			if (is_object($request->$var)) {
				$out=[$model=>ArrayHelper::toArray($request->$var)];
			} elseif (is_array($request->$var)){
				$out=[$model=>$request->$var];
			} else {
				$out=null;
			}
			if (isset($out[$model]['id'])) unset($out[$model]['id']);
		} else {
			$out=null;
		}
		return $out;
	}

	/* title, type. text */
	public function setAlert($alert)
	{
		Yii::$app->session->setFlash('alert', $alert);
	}

	public function setAlert2($text,$type,$title=null)
	{
		$this->setAlert(compact('title','type','text'));
	}

	public function getAlert()
	{
		return Yii::$app->session->getFlash('alert');
	}

	public function loadModel($id=null,$throw=true)
	{
		$model=$this->getModel();
		if ($id!==null) {
			$m=$model::findOne($id);
			if ($m!==null) {
				return $m; 
			} else {
				if ($throw) throw new NotFoundHttpException(Yii::t('site','err_empty_model'));
					else return null;
			}
		} else {
			return new $model();
		}
	}

	public function getModel()
	{
		return "app\models\\".($this->model!=null?$this->model:ucfirst($this->id));
	}

	public function getRoles()
	{
		$roles=array_filter(Yii::$app->authManager->roles,function($r) { return !in_array($r->name,['guest','authenticated']); });
		return array_values(array_map(function($r) { return ['id'=>$r->name,'label'=>$r->description]; },$roles));
	}

	public static function showCookie()
	{
		return !Yii::$app->session->get('cookie');
	}
	
	public function getImported($src,$hash)
	{
		$opts=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			)
		);

		$out=[];
		switch($src) {
			case 'zalamo':
			case 'zalamo-proj':
				$data=@file_get_contents("https://zalamo.com/api/eagfa/details/hash/".$hash, false, stream_context_create($opts));
				if ($data) {
					$import=json_decode($data);
					if ($import) {
						$import->hash=$hash;
						$import->mode="project";
						$import->provider="zalamo";
						if (isset($import->thumbs)) unset($import->thumbs);
						$out=$import;
					}
				}
				break;
			case 'zalamo-pic':
				$data=@file_get_contents("https://zalamo.com/api/eagfa/pirntorder/hash/".$hash, false, stream_context_create($opts));
				if ($data) {
					$import=json_decode($data);
					if ($import) {
						$out=new \stdClass();
						$out->data=$import;
						$out->hash=$hash;
						$out->mode="print";
						$out->provider="zalamo";
					}
				}
				break;
		}
		return $out;
	}

} //end class
