<?php
namespace app\components;

use Yii;
use yii\base\BootstrapInterface;

class Settings implements BootstrapInterface {

	public static function tableName()
	{
		return 'settings';
	}

	public function bootstrap($app) {
		return self::read(false);
	}

	public static function read($out=true) {
		$settings= Yii::$app->db->createCommand("SELECT `name`,`table`,`value`,`label`,`readonly` FROM ".self::tableName()." order by `table`,`name`")->queryAll();
		$var=[];
		foreach ($settings as $v) {
			if ($out) {
				if (!$v['readonly']) $var[]=$v;
			} else {
				if (strlen($v['table'])>0) Yii::$app->params[$v['table']][$v['name']] = $v['value'];
					else Yii::$app->params[$v['name']] = $v['value'];
			}
		}
		if ($out) return $var;
	}

	public static function save($name,$value,$table=""){
		Yii::$app->db->createCommand()->update(self::tableName(), ['value' => $value], ['name'=>$name,'table'=>$table])->execute();
	}

} //end class