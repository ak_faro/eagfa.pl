<?
namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class manageRule extends Rule
{

	public $name = 'canManage';

	public function execute($user, $rule, $item)
	{
		return $item==$user;
	}

} //end class