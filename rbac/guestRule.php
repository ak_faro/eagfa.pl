<?
namespace app\rbac;

use Yii;
use yii\rbac\Rule;


class guestRule extends Rule
{

	public $name = 'guest';

	public function execute($user, $item, $params)
	{
		return Yii::$app->user->isGuest;
	}

} //end class