<?
namespace app\rbac;

use Yii;
use yii\rbac\Rule;


class authenticatedRule extends Rule
{

	public $name = 'authenticated';

	public function execute($user, $item, $params)
	{
		return !Yii::$app->user->isGuest;
	}

} //end class
