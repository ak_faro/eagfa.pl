<?
return [
	'baseUrl'=>"/",
	'enablePrettyUrl' => true,
	'showScriptName' => false,
	'rules' => [
		'oferta/<category>/<product>' => 'offer/product',
		'oferta/<category>' => 'offer/category',
		'oferta'=>"offer/index",
		'galeria'=>"gallery/index",
		'artykuł/<id>/<title>'=>'articles/show',
		'artykuł/<id>'=>'articles/show',
		'thumb/<file:(.*)>' => "files/thumb",
		'thumb2/<file:(.*)>' => "files/thumb2",
//		'pic/<file:(.*)>' => "files/pic",
		'import/zalamo/<type>/<hash>'=>"import/zalamo",
		[
			'pattern' => 'panel/import/<action:\w+>/<hash>',
			'route' => 'import/<action>',
			'defaults' => ['type' => "project"],
		],
		'opinie'=>"comments/all",
	],
];
