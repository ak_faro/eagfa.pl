<?php

return [
	'adminEmail' =>['wroclaw@eagfa.pl'],
	'nofityEmail'=>['system@eagfa.pl'=>"EAGFA.PL"],
	'ftphost'=>"eagfa.ddns.net",

	'publicWidth'=>'80',
	'adminWidth'=>'70',

	'angularVer'=>'1.5.5',
	'materialVer'=>'1.1.1',

	'folders'=>[
		'pub'=>"@app/public",
		'img'=>"@app/public/images",
		'ico'=>"@app/public/pictures",
		'upload'=>"@app/upload",
	],

	'zalamo'=>[
		'types'=>['album'=>"fotoalbum",'book'=>"fotoksiążka",'print'=>"odbitki"],
		'margin'=>4,
		'bleed'=>4,
		'sizes'=>[
			//sz,wy
			'15x15'=>[305,152],
			'15x20'=>[305,203],
			'20x15'=>[402,152],
			'20x20'=>[402,201],
			'20x30'=>[402,305],
			'22x30'=>[457,305],
			'25x25'=>[504,252],
			'30x20'=>[610,203],
			'30x30'=>[610,305],
			'40x30'=>[800,305],
			'42x30'=>[840,296], //'42x30'=>[852,305],
			'45x30'=>[900,305],
		],
	],

	'collectLoc'=>[
		0=>"50-301 Wrocław, ul. Jedności Narodowej 109",
	],

	'invoiceAccouts'=>[
		0=>"65 1940 1076 3133 6390 0000 0000", //paragon
		1=>"41 1300 0000 2253 4336 7379 0001", //faktura
	],

	'freshmail'=>[
		'key'=>"fb04c915ad2b8603ba2c2df269ad906d",
		'secret'=>"c5df31ec843dbda8741b3969dbd57232f57c92d7"
	],
];
