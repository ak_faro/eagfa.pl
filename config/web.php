<?php

$params = require(__DIR__ . '/params.php');

$config = [
	'id' => 'basic',
	'basePath' => dirname(__DIR__),
	'homeUrl'=>"/",
	'aliases'=>[
		'@partial'=>"@app/schemas/partials",
		'@layout'=>"@app/schemas/layouts",
		'@function'=>"@app/schemas/functions",
		'@images'=>"@app/public/images",
		'@icons'=>"@app/public/pictures",
	],
	'bootstrap' => [
		'log',
		'app\\components\\Settings'
	],
	'language'=>'pl',
	'sourceLanguage'=>'en',
	'components' => [
		'request' => [
			'cookieValidationKey' => 'fffgd455rtrw32sd4Yfds%4ET23fsdfY4234asfNmu',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'app\models\Users',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer' => require(__DIR__ . '/mailer.php'),
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['info'],
					'categories' => ['authassign'],
					'logFile'=>"@runtime/logs/authassign.log",
				]
			],
		],
		'db' => require(__DIR__ . '/db.php'),
		'view' => require(__DIR__ . '/view.php'),
		'urlManager' => require(__DIR__ . '/routes.php'),
		'i18n' => [
			'translations' => [
				'*'=>[
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages'
				]
			]
		],
		'image' =>[
			'class' => 'yii\image\ImageDriver',
			'driver' => 'GD',  //GD or Imagick
		],
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
			'defaultRoles'=> ['authenticated', 'guest'],
		],
		'pdf' => [
			'class' => 'kartik\mpdf\Pdf',
		]
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
	];

	$config['bootstrap'][] = 'gii';

	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		'generators' => [
			'model'   => [
				'class'     => 'yii\gii\generators\model\Generator',
				'templates' => ['default' => '@app/templates']
			],
			'controller'   => [
				'class'     => 'yii\gii\generators\controller\Generator',
				'templates' => ['default' => '@app/templates']
			]
		]
	];
}

return $config;
