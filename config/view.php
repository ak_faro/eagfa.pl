<?php
return [
	'renderers' => [
		'twig' => [
			'class' => 'yii\twig\ViewRenderer',
			'cachePath' => '@runtime/Twig/cache',
			'lexerOptions' => [
				'tag_comment'   => array('{#', '#}'),
				'tag_block'     => array('{%', '%}'),
				'tag_variable'  => array('{&', '&}'),
			],
			'options' => [
				'auto_reload' => true,
			],
			'uses' => [
				'yii\bootstrap'
			],
			'globals'=>[
				//'app'=>'Yii::$app',
				'url' => '\yii\helpers\Url',
			],
			'functions' => [
				't'=>'Yii::t',
				'nl2br'=>'\nl2br',
				'csrf'=>'\app\components\BaseController::csrf',
				'cookie'=>'\app\components\BaseController::showCookie',
				'alias'=>'Yii::getAlias',
				'menu'=>'\app\widgets\menuWidget::widget',
				'strip'=>'\app\widgets\articleWidget::widget',
				'article'=>'\app\models\Articles::getHtml',
				'articleList'=>'\app\widgets\articlesWidget::widget',
				'slider'=>'\app\widgets\sliderWidget::widget',
				'toArray'=>'yii\helpers\ArrayHelper::toArray',
			],
			'filters' => [
				'toJson' => 'json_encode',
			],
		]
	],
];